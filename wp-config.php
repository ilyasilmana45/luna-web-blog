<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

$connectstr_dbhost = 'localhost';
$connectstr_dbname = 'clodeo-web-blog-prod';
$connectstr_dbusername = 'root';
$connectstr_dbpassword = '';

define('WP_HOME', 'http://localhost/luna-web-blog/');
define('WP_SITEURL', 'http://localhost/luna-web-blog/');

if (isset($_SERVER['environment'])) {
	switch ($_SERVER['environment']) {
		case "development":
			$connectstr_dbhost = 'vm1.clodeo.com';
			$connectstr_dbname = 'clodeo-web-blog-dev';
			$connectstr_dbusername = 'clodeowebblogdevu';
			$connectstr_dbpassword = 'Cl0de0WebBlogDev';

			define('WP_HOME', 'https://clodeo-web-blog-dev.azurewebsites.net');
			define('WP_SITEURL', 'https://clodeo-web-blog-dev.azurewebsites.net');
		break;
		case "production":
			$connectstr_dbhost = 'vm1.clodeo.com';
			$connectstr_dbname = 'clodeo-web-blog-prod';
			$connectstr_dbusername = 'clodeowebblogprodu';
			$connectstr_dbpassword = 'Cl0de0WebBlogProd';

			define('WP_HOME', 'https://clodeo.com/blog');
			define('WP_SITEURL', 'https://clodeo.com/blog');
		break;
	}
}

if (isset($_SERVER['DB_HOST'])) {
	$connectstr_dbhost = $_SERVER['DB_HOST'];
	$connectstr_dbname = $_SERVER['DB_NAME'];
	$connectstr_dbusername = $_SERVER['DB_USERNAME'];
	$connectstr_dbpassword = $_SERVER['DB_PASSWORD'];
}

// foreach ($_SERVER as $key => $value) {
//     if (strpos($key, "MYSQLCONNSTR_") !== 0) {
//         continue;
//     }
    
//     $connectstr_dbhost = preg_replace("/^.*Data Source=(.+?);.*$/", "\\1", $value);
//     $connectstr_dbname = preg_replace("/^.*Database=(.+?);.*$/", "\\1", $value);
//     $connectstr_dbusername = preg_replace("/^.*User Id=(.+?);.*$/", "\\1", $value);
//     $connectstr_dbpassword = preg_replace("/^.*Password=(.+?)$/", "\\1", $value);
// }

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $connectstr_dbname);

/** MySQL database username */
define('DB_USER', $connectstr_dbusername);

/** MySQL database password */
define('DB_PASSWORD', $connectstr_dbpassword);

/** MySQL hostname */
define('DB_HOST', $connectstr_dbhost);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7ic2oUVv?Z&+T8Y:m.3ddYNUE(Hux0eAI^e#2uw5Pd)>2/fLj<E2pC,uPV0U9L(C');
define('SECURE_AUTH_KEY',  'oRmN$v^-73pANoc/oU?AZ%)QmY0HV)<kk<uU1Jw%tIlh,=B$_vV4+Fe{iXgVe^)W');
define('LOGGED_IN_KEY',    'm%?]){1j>eY1^rQw`W~v>N%~cu<>s%#^6AV[cyEA7eK7-2FBx#a{Ijc 8TLd)st0');
define('NONCE_KEY',        '~UXzvqt{K+%@NDnI4X(iFiUpiwfxnLK#TM2h(DaX^lHYn-bv%VU^BCDDky1bI|[6');
define('AUTH_SALT',        '%L&]o!340h${uC03AX!T?L;}@{%u&i$CFHsk8td0c4jon-}D>D|fIi)P%hZyxEu)');
define('SECURE_AUTH_SALT', 'h#1%>S%yqK<ierdHUDaRo&]Gp#Lr%90X$3q<cTn587_-f+v0(1GF)k)U=Y7.xg$8');
define('LOGGED_IN_SALT',   'U5;l3K~8JvGJvK)`CE#LF,N1}Wt)Jo-mka1KzgMwFbH{HAD!FN^#:Ivxmo1~8hN@');
define('NONCE_SALT',       'H:|[G7G:Xyf1Xdo: 4I01({m|kWM><>{hBUOBP@1@04?<okLWJ4?*oZGb<:l<Zqw');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('WP_CACHE', false);
define( 'WP_MEMORY_LIMIT', '256M' );
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
