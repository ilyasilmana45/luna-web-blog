<?php

/* The template for displaying all single portoflio posts */

$fastwp_pasadena_meta = fastwp_pasadena_meta::get( get_the_ID() );

$fastwp_pasadena_use_intro = false;

if( !empty( $fastwp_pasadena_meta['use_hero_section'] ) && !empty( $fastwp_pasadena_meta['hero_image']['url'] ) ) {
    $fastwp_pasadena_use_intro = true;
    fastwp_pasadena_add_body_class( array( 'with-intro' ) );
}

get_header();
the_post();

do_action('fastwp_pasadena_before_page_content');

if( $fastwp_pasadena_use_intro ) { ?>

<div class="container clearfix full-width intro">
  <div class="clearfix with-bg height-500px">
    <div class="fixed">
      <figure class="background-image1 parallax parallax-banner" style="background-image: url('<?php echo esc_url( $fastwp_pasadena_meta['hero_image']['url'] ); ?>');"></figure>
    </div>
    <div class="full-height-wrapper">
      <div class="parent">
        <div class="bottom">
          <div class="container">
            <div class="col--5">
              <div class="banner-textblock">
                <h1 class="header"><?php echo esc_html( $fastwp_pasadena_meta['hero_title']  ); ?></h1>
                <p class="large"><?php echo esc_html( $fastwp_pasadena_meta['hero_text']  ); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="overlay darker"></div>
    </div>
  </div>
</div>

<?php } ?>

<div id="content">
    <div id="container" class="container clearfix">
        <?php FastWP_pasadena_UI::displayPageGrid(); ?>
    </div>

    <?php echo fastwp_pasadena_single_pagination(); ?>
</div>

<?php

do_action('fastwp_pasadena_after_page_content');

get_footer();