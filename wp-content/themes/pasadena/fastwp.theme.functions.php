<?php

define('CHILD_THEME_URL', get_stylesheet_directory_uri());

global
	$fastwp_pasadena_custom_posts_with_id,
	$fastwp_pasadena_visual_composer_blocks,
	$fastwp_pasadena_shortcodes,
	$fastwp_pasadena_data;
	
if(!isset($fastwp_pasadena_custom_js)){
	global $fastwp_pasadena_custom_js;
	$fastwp_pasadena_custom_js = array();
}


global $fastwp_pasadena_load_scripts;
$fastwp_pasadena_load_scripts = array(
    'bootstrap.min',
    'input.fields',
    'jquery.ba-bbq.min',
    'jquery.fancybox.pack',
    'jquery.fitvids',
    'jquery.flexslider-min',
    'jquery.form',
    'jquery.isotope.load',
    'jquery.isotope2.min',
    'jquery.touchSwipe.min',
    'jquery-easing-1.3',
    'main',
    'modernizr',
    'packery-mode.pkgd.min',
    'parallax',
    'preloader'
);

global $fastwp_pasadena_load_styles;
$fastwp_pasadena_load_styles = array(
    'wp',
    'admin',
    'reset',
	'bootstrap.min',
	'contact',
    'font-awesome.min',
	'jquery.fancybox',
    'styles',
    'responsive',
	'flexslider2'
);

if( isset( $fastwp_pasadena_load_styles_child ) ) {
	$fastwp_pasadena_load_styles = array_merge( $fastwp_pasadena_load_styles, $fastwp_pasadena_load_styles_child );
}

if( isset( $fastwp_pasadena_load_scripts_child ) ){
	$fastwp_pasadena_load_scripts = array_merge( $fastwp_pasadena_load_scripts, $fastwp_pasadena_load_scripts_child );
}

$fastwp_pasadena_custom_posts = null;

add_action('init', 'fwp_theme_init_custom_post_types', 1);
function fwp_theme_init_custom_post_types(){
	global $fastwp_pasadena_custom_posts, $fastwp_pasadena_custom_taxonomies;

    $customPortfolioSlug = ( isset( $fastwp_pasadena_data['fastwp_portfolio_slug'] ) ) ? $fastwp_pasadena_data['fastwp_portfolio_slug'] : 'project';

    $fastwp_pasadena_custom_posts = array(
    	'fwp_portfolio' => array(
    		'name' 				 => esc_html__( 'Portfolio', 'pasadena' ),
    		'single' 			 => esc_html__( 'Portfolio Item', 'pasadena' ),
    		'multiple' 			 => esc_html__( 'Portfolio Items', 'pasadena' ),
    		'settings'	=> array(
    			'public'             => true,
    			'publicly_queryable' => true,
    			'show_ui'            => true,
    			'show_in_menu'       => true,
    			'query_var'          => true,
    			'rewrite'            => array( 'slug' => $customPortfolioSlug ),
    			'capability_type'    => 'post',
    			'has_archive'        => true,
    			'hierarchical'       => false,
    			'menu_position'      => null,
    			'supports'           => array( 'title', 'author', 'editor', 'excerpt', 'thumbnail')
    		)
    	)
    );

    $fastwp_pasadena_custom_taxonomies = array(
    	'portfolio-category' => array(
    		'post_type' => 'fwp_portfolio',
    		'settings' => array(
                'label' => esc_html__( 'Portfolio Categories', 'pasadena' ),
    		    'hierarchical'        => true,
    		    //'labels'              => $labels,
    		    'show_ui'             => true,
    		    'show_admin_column'   => true,
    		    'query_var'           => true,
    			'show_in_nav_menus'	  => true,
    		    'rewrite'             => array( 'slug' => 'portfolio-category' )
    		 )
    	 )
    	);

    do_action('fwp_custom_tax_set');

}

global $fastwp_pasadena_required_plugins; 
$fastwp_pasadena_required_plugins = array(
		array(
            'name'			    => esc_html__( 'FastWP Pasadena Theme extension', 'pasadena' ), // The plugin name
            'slug'			    => 'fastwp-pasadena-theme-extension', // The plugin slug (typically the folder name)
            'source'			=> get_template_directory_uri() . '/fastwp/plugins/fastwp-pasadena-theme-extension.zip', // The plugin source
            'required'			=> true, // If false, the plugin is only 'recommended' instead of required
            'version'			=> '1.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'	=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'		=> '', // If set, overrides default API URL and points to an external URL
        ),
		array(
            'name'			    => esc_html__( 'FastWP Redux extension for Pasadena Theme', 'pasadena' ), // The plugin name
            'slug'			    => 'fastwp-pasadena-redux-extension', // The plugin slug (typically the folder name)
            'source'			=> get_template_directory_uri() . '/fastwp/plugins/fastwp-pasadena-redux-extension.zip', // The plugin source
            'required'			=> true, // If false, the plugin is only 'recommended' instead of required
            'version'			=> '1.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'	=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'		=> '', // If set, overrides default API URL and points to an external URL
        ),
        array(
            'name'              => esc_html__( 'WPBakery Visual Composer', 'pasadena' ), // The plugin name
            'slug'              => 'js_composer', // The plugin slug (typically the folder name)
            'source'            => get_template_directory_uri() . '/fastwp/plugins/js_composer.zip', // The plugin source
            'required'          => true, // If false, the plugin is only 'recommended' instead of required
            'version'           => '5.4.2', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'  => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'      => '', // If set, overrides default API URL and points to an external URL
        ),
        array(
            'name'      		=> esc_html__( 'Contact Form 7', 'pasadena' ),
            'slug'      		=> 'contact-form-7',
            'required'  		=> true,
            'force_activation'  => false,
        )

    );

$fastwp_pasadena_custom_posts_with_id    = array( 'fwp_portfolio' => array( 'thumb' ) );
$fastwp_pasadena_themeUrl 	            = defined( 'CHILD_THEME_URL' ) ? CHILD_THEME_URL : get_template_directory_uri();

require_once get_template_directory() . '/extend/extend.actions.php';
require_once get_template_directory() . '/extend/extend.filters.php';
require_once get_template_directory() . '/extend/extend.visual-composer.php';
require_once get_template_directory() . '/extend/extend.shortcodes.php';
require_once get_template_directory() . '/extend/extend.blog.php';