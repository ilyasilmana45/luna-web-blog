<?php

/**
The template for displaying all single posts
*/

$fastwp_pasadena_options = fastwp_pasadena_get_option( array( 'fwp_blog_date_meta', 'fwp_blog_author_bio', 'fwp_blog_sidebar' ) );

$fastwp_pasadena_show_sidebar = true;
if( !empty( $fastwp_pasadena_options['fwp_blog_sidebar'] ) ) {
    $fastwp_pasadena_show_sidebar = false;
}

get_header();

// future use function
do_action( 'fastwp_pasadena_before_page_content' );

// Start the loop.
while ( have_posts() ) :
the_post();

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div id="content">

<?php if( is_active_sidebar( 'sidebar-1' ) ) { ?>

<div class="post-main-content">

<div class="post-main">

<?php } ?>

<div id="container" class="container clearfix">

<?php $fastwp_pasadena_pt = get_post_format();
if( empty( $fastwp_pasadena_pt ) && has_post_thumbnail() ) {
    $fastwp_pasadena_pt = 'image';
}
if( in_array( $fastwp_pasadena_pt, array( 'image', 'video', 'gallery', 'audio' ) ) ) {
    echo '<div class="element clearfix col--5 with-bg home auto">';
    get_template_part( '/extend-helpers/content-' . $fastwp_pasadena_pt );
    echo '</div>';
} ?>

<div class="element clearfix col--5 post-content home auto">
  <div class="greyed">
    <h2><?php the_title(); ?></h2>
    <?php the_content();
    the_tags( '<p>' . esc_html__( 'Tags: ', 'pasadena' ), ', ', '</p>' );
    wp_link_pages(); ?>
  </div>
</div>

<div class="element clearfix col--2-2 home">
  <div class="greyed">
    <?php if( empty( $fastwp_pasadena_options['fwp_blog_author_bio'] ) ) { ?>
    <p class="small"><?php echo esc_html__( 'Author', 'pasadena' ); ?></p>
    <h4><?php echo sprintf( '%s %s', get_the_author_meta( 'user_firstname' ), get_the_author_meta( 'user_lastname' ) ); ?></h4>
    <?php }
    if( empty( $fastwp_pasadena_options['fwp_blog_date_meta'] ) ) { ?>
    <p class="small extra-padding-top"><?php echo esc_html__( 'Published', 'pasadena' ); ?></p>
    <h4><?php the_date(); ?></h4>
    <?php }
    if( ( $fastwp_pasadena_post_cats = fastwp_pasadena_get_category_list_ab() ) && !empty( $fastwp_pasadena_post_cats ) ) { ?>
    <p class="small extra-padding-top"><?php echo esc_html__( 'Categories', 'pasadena' ); ?></p>
    <?php echo fastwp_pasadena_get_category_list_ab();
    }
    if( empty( $fastwp_pasadena_options['fwp_blog_author_bio'] ) ) {
        echo fastwp_pasadena_author_social_networks();
    } ?>
  </div>
</div>

<?php if ( comments_open() ) { ?>

<div class="element clearfix col--3-2 home hover-without-link">
  <div class="greyed">
    <?php fastwp_pasadena_comment_form(); ?>
  </div>
</div>

<?php

}

comments_template();

endwhile;

?>

</div>

<?php echo fastwp_pasadena_single_pagination(); ?>

</div>

<?php if( is_active_sidebar( 'sidebar-1' ) ) { ?>

<div class="widgets-zone">
    <?php get_sidebar(); ?>
</div>

<?php } ?>

</div>

</div>

</div>

<?php

do_action('fastwp_pasadena_after_page_content');

get_footer();