<?php

if( is_page() ) {
    $currentTemplate = FastWP::getPageTemplate();
	if($currentTemplate === 'template-one-page.php')
    fastwp_pasadena_settings::set_var( 'do_skip_menu', true );
}

$fastwp_pasadena_options = fastwp_pasadena_get_option( array( 'show_header', 'show_socials' ) );

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php

Fastwp_pasadena_UI_showPreloader();

if( !fastwp_pasadena_settings::get_var('do_skip_menu') ) {
    Fastwp_Pasadena_UI_displayMenu( true );
}

do_action('fastwp_pasadena_after_menu');

echo '<div id="wrap">';
echo '<div class="content-wrapper">';
echo '<header id="header" class="clearfix">';
    FastWP_pasadena_UI::HeaderLogo();
    FastWP_pasadena_UI::MenuIconMarkup();
    FastWP_pasadena_UI::SocialIconsHeader();
echo '</header>';

?>

