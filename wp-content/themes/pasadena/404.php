<?php get_header(); ?>

<div id="content">
<div id="container" class="container clearfix">

<?php

$fastwp_pasadena_options = fastwp_pasadena_get_option( array( 'e404_menu', 'e404_text', 'e404_page_id', 'e404_override' ) );

$fastwp_pasadena_options['e404_text'] = ( !empty( $fastwp_pasadena_options['e404_text'] ) ) ? $fastwp_pasadena_options['e404_text'] : 'Ooops...The page you are looking for could not be found! Please, go back to our home page and start over or contact us if the problem persists.';

if( isset( $fastwp_pasadena_options['e404_override'] ) && (boolean) $fastwp_pasadena_options['e404_override'] && $fastwp_pasadena_options['e404_page_id'] != '' ) {

	$page = get_page( $fastwp_pasadena_options['e404_page_id'] );

	echo ( isset( $page->post_content ) ? apply_filters( 'the_content', $page->post_content ) : '' );

} else { ?>

<div class="element clearfix col--5 home text-center auto">
    <div class="greyed">
        <h1 class="header"><?php esc_html_e( 'ERROR 404', 'pasadena' ); ?></h1>
        <p class="large"><?php echo( esc_html( $fastwp_pasadena_options['e404_text'] ) );?></p>
        <div class="break"></div>
        <div class="bottom">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
            <h5><?php esc_html_e( 'Back to Home Page', 'pasadena' ); ?></h5>
            <span class="arrow-right dark"></span>
            </a>
        </div>
    </div>
</div>

<?php } ?>

</div>
</div>

<?php

get_footer();
