<?php


add_filter( 'get_search_form', array( 'FastwpPasadenaExtendFilters', 'search_form' ), 10, 2 );
add_filter( 'the_password_form', array( 'FastwpPasadenaExtendFilters', 'password_protected_form' ), 10, 2 );
add_filter( 'excerpt_length', array( 'FastwpPasadenaExtendFilters', 'excerpt_length' ), 10 );

class FastwpPasadenaExtendFilters {

    public static function excerpt_length() {
        return 25;
    }

    public static function search_form() {
        return '<div class="field-wrapper" style="margin-top:30px">
        <form role="search" method="get" class="form-part" action="' . esc_url( home_url( '/' ) ) . '">
            <span class="icon-search3"></span>
            <input name="s" class="form-search form-element" value="" placeholder="' . esc_html__( 'type &amp; hit enter...', 'pasadena' ) . '" type="text">
        </form>
        </div>';
    }

    public static function password_protected_form() {
        return '<div class="field-wrapper">
        <form role="search" method="post" class="form-part" action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '">
    		<input name="post_password" class="form-search form-element" placeholder="' . esc_html__( 'type password &amp; hit enter...', 'pasadena' ) . '" type="text">
    		<span class="border"></span>
        </form>
        </div>';
    }

}


?>