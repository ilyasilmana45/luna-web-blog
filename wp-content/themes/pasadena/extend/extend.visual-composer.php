<?php

if( !function_exists( 'fastwp_pasadena_get_PostIdAndTitle' ) ) {
	function fastwp_pasadena_get_PostIdAndTitle($post_type = 'page', $value_first = false){
		global $wpdb;
		$sql = $wpdb->prepare("SELECT post_title, ID from $wpdb->posts WHERE `post_type` = '%s' AND `post_status`='publish' ORDER BY `post_title` ASC;",
			$post_type);
		$result = $wpdb->get_results( $sql, ARRAY_A );
		$output = array();
		$output['None'] = '';
		if(isset($result) && is_array($result)){
			foreach($result as $item){
				if($value_first == true){
					$key = $item['post_title'];
					$value=$item['ID'];
				}else {
					$key = $item['ID'];
					$value=$item['post_title'];
				}
				$output[$key] = $value;
			}
		}
		return $output;
	}
}

if( !function_exists( 'fastwp_pasadena_get_categories' ) ) {
    function fastwp_pasadena_get_categories( $isPortfolioFilter = false, $key = 'portfolio-category' ){
		$terms = get_terms( $key, array( 'hide_empty' => 0 ) );
		$categories = array();
		if( $isPortfolioFilter == true ) {
			$categories['None / Show All'] = '*';
			foreach( $terms as $k => $v ) {
				if(isset($v->name) && isset($v->slug))
					$categories[$v->name] = '.' . $v->slug;
			}
		} else {
			foreach( $terms as $k => $v ){
				if( isset( $v->name ) && isset( $v->term_id ) )
					$categories[$v->name] = $v->term_id;
			}
		}
		return $categories;
	}
}

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_fwp_gem_menutabs_container extends WPBakeryShortCodesContainer { }
}

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_fwp_gem_menutabs_item extends WPBakeryShortCode { }
}
