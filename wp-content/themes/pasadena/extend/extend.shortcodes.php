<?php

$shortcode_files = glob( FASTWP_PASADENA_FWP_EXTEND_URI . 'shortcodes/fastwp_*.php' );

foreach( $shortcode_files as $shortcode_file ) {
    if( file_exists( $shortcode_file ) ) {
        include_once $shortcode_file;
        $class_name = str_replace( '.php', '', basename( $shortcode_file ) );
        if( class_exists( $class_name ) )
        new $class_name;
    }   
}