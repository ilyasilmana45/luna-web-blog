<?php

class fastwp_pasadena_team_member {

	function __construct() {
		global $fastwp_pasadena_shortcodes;

		$fastwp_pasadena_shortcodes['fastwp_pasadena_team_member'] = array( 'class' => __CLASS__, 'function' => 'shortcode' );
		self::vc_block();
	}

	public static function shortcode( $atts, $content ) {
        global $post, $fastwp_pasadena_item_content, $fastwp_pasadena_auto_add_home;

        extract( shortcode_atts( array(
            'position'      => '',
            'name'          => '',
            'image'         => '',
            'url'           => '',
            'social_icons'  => '',
            'on_home'       => false,
            'pos_on_home'   => 99,
            'on_pages'      => '',
            'size'          => '',
            'extra_class'	=> ''
        ), $atts ) );

        $class = array();

        $class[] = ( in_array( $size, array( 'col--2-1', 'col--2-2' ) ) ? $size : 'col--1-1' );

        if( !empty( $extra_class ) ) {
            $class[] = esc_attr( $extra_class );
        }

        if( (boolean) $on_home || (boolean) $fastwp_pasadena_auto_add_home ) {
            $class[] = 'home';
        }

        if( isset( $post->ID ) ) {
            $class[] = FastWP::getMenuSectionId( $post->ID );
        }

         $other_pages = fastwp_pasadena_utils::param_group_vc( $on_pages );
         if( !empty( $other_pages ) ) {
             foreach( $other_pages as $other_page ) {
                 if( isset( $other_page['on_page'] ) && $other_page['on_page'] != $post->ID )
                 $class[] = FastWP::getMenuSectionId( $other_page['on_page'] );
             }
         }

        $image = wp_get_attachment_image_src( $image, 'full' );
        $image_url = $social_icons_markup = '';

        if( !empty( $image ) ) {
            $class[] = 'with-bg';
            $image_url = $image[0];
        }

        $social_icons =  vc_param_group_parse_atts( $social_icons );

        if( !empty( $social_icons ) ) {
            $social_icons_markup .= '<ul class="social-list clearfix">';
            foreach( $social_icons as $social_icon ) {
                if( !isset( $social_icon['link'] ) ) {
                    continue;
                }
                $url = fastwp_pasadena_utils::url_from_vc( $social_icon['link'] );
                $social_icons_markup .= '<li><a href="' . esc_url( $url['url'] ) . '" target="' . esc_attr( $url['target'] ) . '"><i class="' . esc_attr( $social_icon['icon'] ) . '"></i></a></li>';
            }
            $social_icons_markup .= '</ul>';
        }



        $markup = '<div class="element %s clearfix display-on-hover">
          <div class="borders">
            <img src="%s" alt="">
            <div class="overlay"></div>
            <div class="parent">
              <div class="bottom">
                %s
                <p class="small">%s</p>
                <h4>%s</h4>
              </div>
            </div>
          </div>
        </div>';

        $fastwp_pasadena_item_content[] = array( 'position' => (int) $pos_on_home, 'markup' => sprintf( $markup, implode( ' ', $class ), $image_url, $social_icons_markup, esc_html( $position ), esc_html( $name ) ) );
    }

	public static function vc_block(){
		global $fastwp_pasadena_visual_composer_blocks;

		$fastwp_pasadena_visual_composer_blocks[] = array(
			"name"      => esc_html__( 'Team Member', 'pasadena' ),
			"base"      => 'fastwp_pasadena_team_member',
			"icon" 		=> get_template_directory_uri() . "/assets/img/rw_vc_icon.jpg",
			"category"  => esc_html__( 'Pasadena Elements', 'pasadena' ),
			"params"    => array(
				array(
					"save_always" 	=> true,
					"type" 			=> 'textfield',
					"holder" 		=> false,
					"heading" 		=> esc_html__( 'Position', 'pasadena' ),
					"param_name" 	=> 'position',
					"admin_label"	=> true
				),
				array(
					"save_always" 	=> true,
					"type" 			=> 'textfield',
					"holder" 		=> false,
					"heading" 		=> esc_html__( 'Name', 'pasadena' ),
					"param_name" 	=> 'name',
					"admin_label"	=> true
				),
				array(
					"save_always" 	=> true,
					"type" 			=> 'attach_image',
					"holder" 		=> false,
					"heading" 		=> esc_html__( 'Image', 'pasadena' ),
					"param_name" 	=> 'image',
					"admin_label"	=> true
				),
                array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Social Icons', 'pasadena' ),
                    'type'          => 'param_group',
                    'param_name'    => 'social_icons',
                    'params'        => array(
                                            array(
                                                'param_name' 	=> 'icon',
                                                'heading'		=> esc_html__( 'Icons', 'pasadena' ),
                                                'type' 			=> 'iconpicker',
                                                'settings'      => array(
                                                		                 'emptyIcon'=> false,
                                                                         'type'     => 'fontawesome',
                                                		                 'iconsPerPage' => 200,
                                                		                ),
                                                'description' 	=> esc_html__( 'Select Icons', 'pasadena' ),
                                            ),
                                            array(
                                                'param_name' => 'link',
                                                'type' => 'vc_link',
                                                'heading' => esc_html__( 'Link', 'pasadena' ),
                                            )
                                        )
                ),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Show On', 'pasadena' ),
					"type" 			=> 'checkbox',
					"heading" 		=> esc_html__( 'Show on Home/Index', 'pasadena' ),
					"param_name" 	=> 'on_home',
                    "default"       => 0,
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Check to show this element on home/index page', 'pasadena' )
				),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Show On', 'pasadena' ),
					"type" 			=> 'textfield',
					"heading" 		=> esc_html__( 'Position of Element', 'pasadena' ),
					"param_name" 	=> 'pos_on_home',
					"default" 		=> '99',
                    "value"         => '99',
					"admin_label"	=> true,
                	"description"	=> esc_html__( 'Default: 99', 'pasadena' )
				),
                array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Show On', 'pasadena' ),
                    'type'          => 'param_group',
                    'param_name'    => 'on_pages',
                    'params'        => array(
                                            array(
                                                'param_name' => 'on_page',
                                                'type' => 'dropdown',
                                                'value' => fastwp_pasadena_get_PostIdAndTitle( 'page', true ),
                                                'heading' => esc_html__( 'Display the item on this page', 'pasadena' ),
                                            )
                                        )
                ),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Size', 'pasadena' ),
					"type" 			=> 'dropdown',
					"heading" 		=> esc_html__( 'Size', 'pasadena' ),
					"param_name" 	=> 'size',
					"default" 		=> 'col--1-1',
                    "value"         => array( esc_html__( '1 Column', 'pasadena' ) => 'col--1-1', esc_html__( '2 Columns Large', 'pasadena' ) => 'col--2-1', esc_html__( '2 Columns Tall and Large', 'pasadena' ) => 'col--2-2' ),
					"admin_label"	=> true
				),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Extra', 'pasadena' ),
					"type" 			=> 'textfield',
					"heading" 		=> esc_html__( 'Extra Class', 'pasadena' ),
					"param_name" 	=> 'extra_class',
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Use this options to add an extra class to element, you can use this for custom css', 'pasadena' )
				)

 			)
    );
	}

}

?>