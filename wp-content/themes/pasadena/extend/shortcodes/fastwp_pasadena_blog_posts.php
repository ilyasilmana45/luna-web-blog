<?php

class fastwp_pasadena_blog_posts {

	function __construct() {
		global $fastwp_pasadena_shortcodes;

		$fastwp_pasadena_shortcodes['fastwp_pasadena_blog_posts'] = array( 'class' => __CLASS__, 'function' => 'shortcode' );
		self::vc_block();
	}

	public static function shortcode( $atts, $content ){
        global $post, $fastwp_pasadena_data, $fastwp_pasadena_item_content, $fastwp_pasadena_auto_add_home;

        extract( shortcode_atts( array(
			'include'	    => '',
			'exclude'	    => '',
            'include_cats'  => '',
            'exclude_cats'  => '',
            'limit'         => '-1',
            'on_home'       => false,
            'on_pages'      => '',
            'size'          => 'col--1-2',
            'show_hero'     => true,
            'extra_class'	=> ''
        ), $atts ) );

		$args = array(
			'numberposts' 		=> $limit,
			'posts_per_page' 	=> '-1',
			'post_status' 		=> 'publish',
			'post_type'			=> 'post',
			'include'			=> $include,
			'exclude'			=> $exclude,
			'suppress_filters'	=> '0',
			);

		if( $include != '' ) {
			$args['include'] = $include;
            $args['orderby'] = 'post__in';
		}

		if( $exclude != '' ) {
			$args['exclude'] = $exclude;
		}

        if( !empty( $exclude_cats ) && empty( $include_cats ) ) {
        $args['tax_query'] = array(
          array(
            'taxonomy' => 'category',
            'field'    => 'ids',
            'terms'    => $exclude_cats,
            'operator' => 'NOT IN'
          )
        );
        }

        if( !empty( $include_cats )  ) {
        $args['tax_query'] = array(
          array(
            'taxonomy' => 'category',
            'field'    => 'ids',
            'terms'    => $include_cats
          )
        );
        }

        $items = get_posts( $args );

		if( !is_array( $items ) || ( is_array( $items ) && count( $items ) == 0 ) ) {
			return '';
		}

        $items_markup   = '';

        $default_size   = in_array( $size, array( 'col--2-1', 'col--2-2' ) ) ? $size : 'col--1-2';

        $class = array();

        if( !empty( $extra_class ) ) {
            $class[] = esc_attr( $extra_class );
        }

        if( (boolean) $on_home || (boolean) $fastwp_pasadena_auto_add_home ) {
            $class[] = 'home';
        }

        if( isset( $post->ID ) ) {
            $class[] = FastWP::getMenuSectionId( $post->ID );
        }

        $other_pages = fastwp_pasadena_utils::param_group_vc( $on_pages );
        if( !empty( $other_pages ) ) {
            foreach( $other_pages as $other_page ) {
                if( isset( $other_page['on_page'] ) && $other_page['on_page'] != $post->ID )
                $class[] = FastWP::getMenuSectionId( $other_page['on_page'] );
            }
        }

        if( empty( $fastwp_pasadena_data['fwp_hide_blog_title'] ) && (boolean) $show_hero ) {

        $hero_bg = '';

        if( !empty( $fastwp_pasadena_data['fwp_blog_bg']['url'] ) ) {
            $hero_bg = $fastwp_pasadena_data['fwp_blog_bg']['url'];
        }

        $fastwp_pasadena_item_content[] = array( 'position' => 1, 'markup' => '<div class="element clearfix' . ( !empty( $class ) ? ' ' . implode( ' ', $class ) : '' ) . ' col--2-2 with-bg">
            <div class="borders">
                ' . ( !empty( $hero_bg ) ? '<img src="' . $hero_bg . '" alt="" />' : '' ) . '
                <div class="overlay"></div>
                <div class="parent">
                    <div class="bottom">
                        <h2>' . ( !empty( $fastwp_pasadena_data['fwp_blog_title'] ) ? esc_html( $fastwp_pasadena_data['fwp_blog_title'] ) : 'Blog' ) . '</h2>
                    </div>
                </div>
            </div>
        </div>' );

        }

		foreach( $items as $item ) {

        $meta               = fastwp_pasadena_meta::get( $item->ID );
		// $image 	            = wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), 'full' );
        $iclass             = array();
        $iclass[]           = !empty( $meta['col_size'] ) && in_array( $meta['col_size'], array( 'col--1-2', 'col--2-1', 'col--2-2' ) ) ? $meta['col_size'] : $default_size;
        $index_pos          = isset( $meta['pos_on_home'] ) ? (int) $meta['pos_on_home'] : 99;

        if( isset( $meta['item_show_on'] ) && $meta['item_show_on'] && is_array( $meta['item_show_on'] ) ) {
            foreach( $meta['item_show_on'] as $pid => $pval ) {
                if( $pval == 1 )
                if( is_numeric( $pid ) ) {
                    $iclass[] = FastWP::getMenuSectionId( $pid );
                } else {
                    $iclass[] = esc_html( $pid );
                }
            }
        }

        $markup = '<div class="element %s clearfix">
            <a href="%s" title="">
            <div class="greyed">
                <p class="small">%s</p>
                <h4>%s</h4>
                <p>%s</p>
                <p class="details social-header">%s<br>
                  %s
                </p>
                <span class="arrow-right dark"></span>
            </div>
            </a>
            </div>';

        // Instead "Article" you may use fastwp_pasadena_get_category_list( true, ' / ', $item->ID ) and display categories
        $fastwp_pasadena_item_content[] = array( 'position' => (int) $index_pos, 'markup' => sprintf( $markup, implode( ' ', array_merge( $iclass, $class ) ), get_the_permalink( $item->ID ), esc_html__( 'Article', 'pasadena' ), $item->post_title, get_the_excerpt( $item->ID ), esc_html__( 'By ', 'pasadena' ) . sprintf( '<span class="serif">%s</span>', get_the_author_meta( 'user_nicename', $item->post_author ) ), ( empty( $fastwp_pasadena_data['fwp_blog_date_meta'] ) ? get_the_date( '', $item->ID ) : '' ) ) );

        }
	}

	public static function vc_block(){
		global $fastwp_pasadena_visual_composer_blocks;

		$fastwp_pasadena_visual_composer_blocks[] = array(
			"name"      => esc_html__( 'Blog Posts', 'pasadena' ),
			"base"      => 'fastwp_pasadena_blog_posts',
			"icon" 		=> get_template_directory_uri() . "/assets/img/rw_vc_icon.jpg",
			"category"  => esc_html__( 'Pasadena Elements', 'pasadena' ),
			"params"    => array(
				array(
					"save_always" 	=> true,
					"type" 			=> 'textfield',
					"holder" 		=> false,
					"heading" 		=> esc_html__( 'Include Posts', 'pasadena' ),
					"param_name" 	=> 'include',
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Put service IDs to include only these', 'pasadena' )
				),
				array(
					"save_always" 	=> true,
					"type" 			=> 'textfield',
					"holder" 		=> false,
					"heading" 		=> esc_html__( 'Exclude Posts', 'pasadena' ),
					"param_name" 	=> 'exclude',
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Put service IDs to exclude only these', 'pasadena' )
				),
				array(
					"save_always" 	=> true,
					"type" 			=> 'textfield',
					"holder" 		=> false,
					"heading" 		=> esc_html__( 'Include Categories', 'pasadena' ),
					"param_name" 	=> 'include_cats',
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Put category IDs to include only these', 'pasadena' )
				),
				array(
					"save_always" 	=> true,
					"type" 			=> 'textfield',
					"holder" 		=> false,
					"heading" 		=> esc_html__( 'Exclude Categories', 'pasadena'),
					"param_name" 	=> 'exclude_cats',
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Put category IDs to exclude only these', 'pasadena' )
				),
				array(
					"save_always" 	=> true,
					"type" 			=> 'dropdown',
					"heading" 		=> esc_html__( 'Show Blog Title', 'pasadena' ),
					"param_name" 	=> 'show_hero',
					"default" 		=> 1,
                    "value"         => array( esc_html__( 'Yes', 'pasadena' ) => 1, esc_html__( 'No', 'pasadena' ) => 0 ),
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Use options from Theme Options', 'pasadena' )
				),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Show On', 'pasadena' ),
					"type" 			=> 'checkbox',
					"heading" 		=> esc_html__( 'Show on Home/Index', 'pasadena' ),
					"param_name" 	=> 'on_home',
                    "default"       => 0,
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Check to show this element on home/index page', 'pasadena' )
				),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Show On', 'pasadena' ),
					"type" 			=> 'textfield',
					"heading" 		=> esc_html__( 'Position of Element', 'pasadena' ),
					"param_name" 	=> 'pos_on_home',
					"default" 		=> '99',
                    "value"         => '99',
					"admin_label"	=> true,
                	"description"	=> esc_html__( 'Default: 99', 'pasadena' )
				),
                array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Show On', 'pasadena' ),
                    'type'          => 'param_group',
                    'param_name'    => 'on_pages',
                    'params'        => array(
                                            array(
                                                'param_name' => 'on_page',
                                                'type' => 'dropdown',
                                                'value' => fastwp_pasadena_get_PostIdAndTitle( 'page', true ),
                                                'heading' => esc_html__( 'Display the item on this page', 'pasadena' ),
                                            )
                                        )
                ),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Size', 'pasadena' ),
					"type" 			=> 'dropdown',
					"heading" 		=> esc_html__( 'Size', 'pasadena' ),
					"param_name" 	=> 'size',
					"default" 		=> 'col--1-2',
                    "value"         => array( esc_html__( '1 Column Tall', 'pasadena' ) => 'col--1-2', esc_html__( '2 Columns Large', 'pasadena' ) => 'col--2-1', esc_html__( '2 Columns Tall and Large', 'pasadena' ) => 'col--2-2' ),
					"admin_label"	=> true
				),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Extra', 'pasadena' ),
					"type" 			=> 'textfield',
					"heading" 		=> esc_html__( 'Extra Class', 'pasadena' ),
					"param_name" 	=> 'extra_class',
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Use this options to add an extra class to element, you can use this for custom css', 'pasadena' )
				)

 			)
    );
	}

}

?>