<?php

class fastwp_pasadena_slider {

	function __construct() {
		global $fastwp_pasadena_shortcodes;

		$fastwp_pasadena_shortcodes['fastwp_pasadena_slider'] = array( 'class' => __CLASS__, 'function' => 'shortcode' );
		self::vc_block();
	}

	public static function shortcode( $atts, $content ) {
        global $post, $fastwp_pasadena_item_content, $fastwp_pasadena_auto_add_home;

        extract( shortcode_atts( array(
            'images'        => '',
            'on_home'       => false,
            'pos_on_home'   => 99,
            'on_pages'      => '',
            'size'          => '',
            'extra_class'	=> ''
        ), $atts ) );

         $class = array();

         $class[] = ( in_array( $size, array( 'col--2-2', 'col--3-2' ) ) ? $size : 'col--5' );

         if( !empty( $extra_class ) ) {
             $class[] = esc_attr( $extra_class );
         }

        if( (boolean) $on_home || (boolean) $fastwp_pasadena_auto_add_home ) {
            $class[] = 'home';
        }

         if( isset( $post->ID ) ) {
             $class[] = FastWP::getMenuSectionId( $post->ID );
         }

         $other_pages = fastwp_pasadena_utils::param_group_vc( $on_pages );
         if( !empty( $other_pages ) ) {
             foreach( $other_pages as $other_page ) {
                 if( isset( $other_page['on_page'] ) && $other_page['on_page'] != $post->ID )
                 $class[] = FastWP::getMenuSectionId( $other_page['on_page'] );
             }
         }

        $images_markup = '';

        $list = explode( ',', $images );

        if( count( $list ) > 0 ) {
            foreach( $list as $item ) {
        		$image  = wp_get_attachment_image_src( $item, 'full' );
                $images_markup .= sprintf( '<li><img src="%s" alt="" /></li>', $image[0] );
            }
        }

        $markup = '<div class="element %s clearfix with-bg">
          <div class="borders">
            <div class="flexslider">
              <ul class="slides">
                %s
              </ul>
            </div>
          </div>
        </div>';

        $fastwp_pasadena_item_content[] = array( 'position' => (int) $pos_on_home, 'markup' => sprintf( $markup, implode( ' ', $class ), $images_markup ) );
    }

	public static function vc_block() {
		global $fastwp_pasadena_visual_composer_blocks;

		$fastwp_pasadena_visual_composer_blocks[] = array(
			"name"      => esc_html__( 'Slider', 'pasadena' ),
			"base"      => 'fastwp_pasadena_slider',
			"icon" 		=> get_template_directory_uri() . "/assets/img/rw_vc_icon.jpg",
			"category"  => esc_html__( 'Pasadena Elements', 'pasadena' ),
			"params"    => array(
				array(
					"save_always" 	=> true,
					"type" 			=> 'attach_images',
					"holder" 		=> false,
					"heading" 		=> esc_html__( 'Images', 'pasadena'),
					"param_name" 	=> 'images',
					"admin_label"	=> true
				),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Show On', 'pasadena' ),
					"type" 			=> 'checkbox',
					"heading" 		=> esc_html__( 'Show on Home/Index', 'pasadena' ),
					"param_name" 	=> 'on_home',
                    "default"       => 0,
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Check to show this element on home/index page', 'pasadena' )
				),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Show On', 'pasadena' ),
					"type" 			=> 'textfield',
					"heading" 		=> esc_html__( 'Position of Element', 'pasadena' ),
					"param_name" 	=> 'pos_on_home',
					"default" 		=> '99',
                    "value"         => '99',
					"admin_label"	=> true,
                	"description"	=> esc_html__( 'Default: 99', 'pasadena' )
				),
                array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Show On', 'pasadena' ),
                    'type'          => 'param_group',
                    'param_name'    => 'on_pages',
                    'params'        => array(
                                            array(
                                                'param_name' => 'on_page',
                                                'type' => 'dropdown',
                                                'value' => fastwp_pasadena_get_PostIdAndTitle( 'page', true ),
                                                'heading' => esc_html__( 'Display the item on this page', 'pasadena' ),
                                            )
                                        )
                ),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Size', 'pasadena' ),
					"type" 			=> 'dropdown',
					"heading" 		=> esc_html__( 'Size', 'pasadena' ),
					"param_name" 	=> 'size',
					"default" 		=> 'col--5',
                    "value"         => array( esc_html__( '2 Columns Tall and Large', 'pasadena' ) => 'col--2-2', esc_html__( '3 Columns Tall and Large', 'pasadena' ) => 'col--3-2', esc_html__( 'Full Width', 'pasadena' ) => 'col--5' ),
					"admin_label"	=> true
				),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Extra', 'pasadena' ),
					"type" 			=> 'textfield',
					"heading" 		=> esc_html__( 'Extra Class', 'pasadena' ),
					"param_name" 	=> 'extra_class',
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Use this options to add an extra class to element, you can use this for custom css', 'pasadena' )
				)

 			)
    );
	}

}

?>