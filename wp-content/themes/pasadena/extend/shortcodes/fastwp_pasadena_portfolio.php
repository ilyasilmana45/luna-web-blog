<?php

class fastwp_pasadena_portfolio {

	function __construct() {
		global $fastwp_pasadena_shortcodes;

		$fastwp_pasadena_shortcodes['fastwp_pasadena_portfolio'] = array( 'class' => __CLASS__, 'function' => 'shortcode' );
		self::vc_block();
	}

	public static function shortcode( $atts, $content ) {
        global $post, $fastwp_pasadena_item_content, $fastwp_pasadena_auto_add_home;

        extract( shortcode_atts( array(
			'include'	    => '',
			'exclude'	    => '',
            'include_cats'  => '',
            'exclude_cats'  => '',
            'limit'         => '-1',
            'on_home'       => false,
            'on_pages'      => '',
            'size'          => 'col1-3',
            'extra_class'	=> ''
        ), $atts ) );

		$args = array(
			'numberposts' 		=> $limit,
			'posts_per_page' 	=> '-1',
			'post_status' 		=> 'publish',
			'post_type'			=> 'fwp_portfolio',
			'include'			=> $include,
			'exclude'			=> $exclude,
			'suppress_filters'	=> '0',
			);

		if( $include != '' ) {
			$args['include'] = $include;
            $args['orderby'] = 'post__in';
		}

		if( $exclude != '' ) {
			$args['exclude'] = $exclude;
		}

        if( !empty( $exclude_cats ) && empty( $include_cats ) ) {
        $args['tax_query'] = array(
          array(
            'taxonomy' => 'portfolio-category',
            'field'    => 'ids',
            'terms'    => $exclude_cats,
            'operator' => 'NOT IN'
          )
        );
        }

        if( !empty( $include_cats )  ) {
        $args['tax_query'] = array(
          array(
            'taxonomy' => 'portfolio-category',
            'field'    => 'ids',
            'terms'    => $include_cats
          )
        );
        }

        $items = get_posts( $args );

		if( !is_array( $items ) || ( is_array( $items ) && count( $items ) == 0 ) ) {
		    return;
		}

        $items_markup   = array();
        $class = $iclass = array();

        if( !empty( $extra_class ) ) {
            $class[] = esc_attr( $extra_class );
        }

        if( (boolean) $on_home || (boolean) $fastwp_pasadena_auto_add_home ) {
            $class[] = 'home';
        }

        if( isset( $post->ID ) ) {
            $class[] = FastWP::getMenuSectionId( $post->ID );
        }

        $other_pages = fastwp_pasadena_utils::param_group_vc( $on_pages );
        if( !empty( $other_pages ) ) {
            foreach( $other_pages as $other_page ) {
                if( isset( $other_page['on_page'] ) && $other_page['on_page'] != $post->ID )
                $class[] = FastWP::getMenuSectionId( $other_page['on_page'] );
            }
        }

		foreach( $items as $item ) {

        $iclass = array();

        $meta               = fastwp_pasadena_meta::get( $item->ID );
		$image 	            = wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), 'full' );
        $index_pos          = isset( $meta['pos_on_home'] ) ? (int) $meta['pos_on_home'] : 99;

        $iclass[] = $item->post_title;
        if( isset( $meta['item_show_on'] ) && $meta['item_show_on'] && is_array( $meta['item_show_on'] ) ) {
            foreach( $meta['item_show_on'] as $pid => $pval ) {
                if( $pval == 1 )
                if( is_numeric( $pid ) ) {
                    $iclass[] = FastWP::getMenuSectionId( $pid );
                } else {
                    $iclass[] = esc_html( $pid );
                }
            }
        }

        if( !isset( $meta['col_size'] ) ) {
            $meta['col_size'] = 1;
        }

        switch( (int) $meta['col_size'] ) {

            case 2:
                $fastwp_pasadena_item_content[] = array( 'position' => $index_pos, 'markup' => sprintf( '<div class="element clearfix col--2-1 with-bg %s">
                                    <a href="%s" title="">
                                        <div class="borders">
                                        <img src="%s" alt="">
                                        <div class="overlay"></div>
                                        <div class="parent">
                                          <div class="bottom">
                                            <h3>%s</h3>
                                            <span class="arrow-right"></span>
                                          </div>
                                        </div>
                                        </div>
                                    </a>
                                </div>', implode( ' ', array_merge( $class, $iclass ) ), get_the_permalink( $item->ID ), ( isset( $image[0] ) ? esc_url( $image[0] ) : '' ), $item->post_title ) );
            break;

            case 3:
                $fastwp_pasadena_item_content[] = array( 'position' => $index_pos, 'markup' => sprintf( '<div class="element clearfix col--2-2 with-bg %s">
                                    <a href="%s" title="">
                                        <div class="borders">
                                        <img src="%s" alt="">
                                        <div class="overlay"></div>
                                        <div class="parent">
                                          <div class="bottom">
                                            <h3>%s</h3>
                                            <span class="arrow-right"></span>
                                          </div>
                                        </div>
                                        </div>
                                    </a> </div>', implode( ' ', array_merge( $class, $iclass ) ), get_the_permalink( $item->ID ), ( isset( $image[0] ) ? esc_url( $image[0] ) : '' ), $item->post_title ) );
            break;

            /*
            case 4:
                $categories         = array();

    		    $item_terms         = wp_get_object_terms( $item->ID, 'portfolio-category' );
        		foreach( $item_terms as $item_cat ) {
                    $categories[]   = $item_cat->name;
        		}

                $fastwp_pasadena_item_content[] = array( 'position' => $index_pos, 'markup' => sprintf( '<div class="element clearfix col--1-1 %s">
                                    <a href="%s" title="">
                                        <div class="greyed">
                                        <p class="small">%s</p>
                                        <h4>%s</h4>
                                        <h6 class="social-header"><i class="fa fa-behance"></i> 86<span class="padding">k</span></h6>
                                        <span class="arrow-right dark"></span> </div>
                                    </a> </div>', implode( ' ', array_merge( $class, $iclass ) ), get_the_permalink( $item->ID ), implode( ' / ', $categories ), $item->post_title ) );
            break;
            */

            default:
                $fastwp_pasadena_item_content[] = array( 'position' => $index_pos, 'markup' => sprintf( '<div class="element clearfix col--1-1 with-bg %s">
                                    <a href="%s" title="">
                                      <div class="borders">
                                      <img src="%s" alt="">
                                        <div class="overlay"></div>
                                        <div class="parent">
                                          <div class="top">
                                            <h4>%s</h4>
                                            <span class="arrow-right"></span>
                                          </div>
                                        </div>
                                      </div>
                                    </a>
                                </div>', implode( ' ', array_merge( $class, $iclass ) ), get_the_permalink( $item->ID ), ( isset( $image[0] ) ? esc_url( $image[0] ) : '' ), $item->post_title ) );
            break;

        }

        }
	}

	public static function vc_block(){
		global $fastwp_pasadena_visual_composer_blocks;

		$fastwp_pasadena_visual_composer_blocks[] = array(
			"name"      => esc_html__( 'Portfolio', 'pasadena' ),
			"base"      => 'fastwp_pasadena_portfolio',
			"icon" 		=> get_template_directory_uri() . "/assets/img/rw_vc_icon.jpg",
			"category"  => esc_html__( 'Pasadena Elements', 'pasadena' ),
			"params"    => array(
				array(
					"save_always" 	=> true,
					"type" 			=> 'textfield',
					"holder" 		=> false,
					"heading" 		=> esc_html__( 'Include Portfolio Items', 'pasadena' ),
					"param_name" 	=> 'include',
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Put service IDs to include only these', 'pasadena' )
				),
				array(
					"save_always" 	=> true,
					"type" 			=> 'textfield',
					"holder" 		=> false,
					"heading" 		=> esc_html__( 'Exclude Portfolio Items', 'pasadena' ),
					"param_name" 	=> 'exclude',
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Put service IDs to exclude only these', 'pasadena' )
				),
				array(
					"save_always" 	=> true,
					"type" 			=> 'textfield',
					"holder" 		=> false,
					"heading" 		=> esc_html__( 'Include Portfolio Categories', 'pasadena' ),
					"param_name" 	=> 'include_cats',
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Put category IDs to include only these', 'pasadena' )
				),
				array(
					"save_always" 	=> true,
					"type" 			=> 'textfield',
					"holder" 		=> false,
					"heading" 		=> esc_html__( 'Exclude Portfolio Categories', 'pasadena'),
					"param_name" 	=> 'exclude_cats',
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Put category IDs to exclude only these', 'pasadena' )
				),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Show On', 'pasadena' ),
					"type" 			=> 'checkbox',
					"heading" 		=> esc_html__( 'Show on Home/Index', 'pasadena' ),
					"param_name" 	=> 'on_home',
                    "default"       => 0,
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Check to show this element on home/index page', 'pasadena' )
				),
                array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Show On', 'pasadena' ),
                    'type'          => 'param_group',
                    'param_name'    => 'on_pages',
                    'params'        => array(
                                            array(
                                                'param_name' => 'on_page',
                                                'type' => 'dropdown',
                                                'value' => fastwp_pasadena_get_PostIdAndTitle( 'page', true ),
                                                'heading' => esc_html__( 'Display the item on this page', 'pasadena' ),
                                            )
                                        )
                ),
				array(
					"save_always" 	=> true,
					"group"			=> esc_html__( 'Extra', 'pasadena' ),
					"type" 			=> 'textfield',
					"heading" 		=> esc_html__( 'Extra Class', 'pasadena' ),
					"param_name" 	=> 'extra_class',
					"admin_label"	=> true,
					"description"	=> esc_html__( 'Use this options to add an extra class to element, you can use this for custom css', 'pasadena' )
				)

 			)
    );
	}

}

?>