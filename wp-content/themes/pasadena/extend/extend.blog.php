<?php

if (!function_exists('fastwp_pasadena_is_blog')) {

	function fastwp_pasadena_is_blog() {
		if (!is_front_page() && is_home())
			return TRUE;
	}

}

if (!function_exists("fastwp_pasadena_sidebar_position")) {
	function fastwp_pasadena_sidebar_position() {
		global $fastwp_pasadena_data;
		if (isset($_GET['sidebar_position'])) {
				return $_GET['sidebar_position'];
		}
		return (isset( $fastwp_pasadena_data['fwp_sidebar_pos']))?  $fastwp_pasadena_data['fwp_sidebar_pos'] : "right";
	}
}


if ( !function_exists( 'fastwp_pasadena_posts' ) ) {
	function fastwp_pasadena_posts( $layout = 'blog-list' ) {

    if ( have_posts() ) :
        $i = 1;
    	while ( have_posts() ) : the_post();
    	do_action('fastwp_pasadena_before_post_content');
    		get_template_part('extend-helpers/' . $layout);
        do_action('fastwp_pasadena_after_post_content');
        $i++;
        endwhile;
    	else:
    		get_template_part('extend-helpers/content', 'none');
    	endif;

	}
}

if ( !function_exists( 'fastwp_pasadena_video_post' ) ) :
function fastwp_pasadena_video_post( $url, $html = '' ) {
    if( !empty( $html ) ) {
        return $html;
    }
	if( substr_count( $url, 'vimeo.com' ) == 1 ){
        $videoURL = str_replace( 'vimeo.com/','player.vimeo.com/video/', $url );
	} else if( substr_count($url, 'youtu.be') == 1 ||substr_count($url, 'youtube.com') == 1){
        $videoURL = str_replace( '/watch?v=','/embed/', $url );
	} else $videoURL = '';

    if( !empty( $videoURL ) ) {
        echo '<iframe src="' . esc_url( $videoURL ) . '" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" frameborder="0" height="100%" width="100%"></iframe>';
    }
	return;
}
endif;

if( !function_exists( "fastwp_gallery_post" ) ) :

	function fastwp_gallery_post() {
		global $post;
		if(!isset($post->ID)) return;

		$details 	= fastwp_pasadena_meta::get( $post->ID );

		$photos = (isset($details['gallery']))? $details['gallery'] : array();
		$gallery = '';
		$posturl='#';
		if(!is_single()){
			$posturl = get_permalink($post->ID);
		}

		if(count($photos) > 0){
			foreach($photos as $photo){
				if(trim($photo) == '') continue;
				$gallery .= '<a href="'. $posturl .'">
                               <img class="img-responsive" src="' . esc_url( trim( $photo) ) . ' " alt="image">
                            </a>';
			}
		}
		if($gallery!='')
			return sprintf('<div id="owl-blog-single" class="owl-carousel">%s</div>', $gallery);
		
	   return false;
	
	}
endif;


if( !function_exists( 'fastwp_pasadena_comment_form' ) ):
	function fastwp_pasadena_comment_form() {

		global $post;
		if( !isset( $post->ID ) ) return;

        global $current_user;

		//$commenter      = wp_get_current_commenter();
		$req            = get_option( 'require_name_email' );
		$aria_req       = ( $req ? " aria-required='true'" : '' );

		$args = array(
		  'id_form'             => 'contactform',
          'class_form'          => 'form-part',
		  'id_submit'           => 'submit',
		  'class_submit'        => 'send-btn',
		  'title_reply'         => '',
		  'title_reply_to'      => esc_html__( 'Leave a Reply to %s', 'pasadena' ),
		  'cancel_reply_link'   => esc_html__( 'Cancel Reply', 'pasadena' ),
		  'label_submit'        => esc_html__( 'Post Comment', 'pasadena' ),

		  'comment_field'   => '<textarea id="comments" name="comment" cols="45" rows="8" class="comment-field" aria-required="true" title="' . esc_html__( 'Tell us what you think!', 'pasadena' ) . '"></textarea>',

		  'must_log_in'     => '<p class="mleft must-log-in">' . esc_html__( 'You must be logged in to post a comment.', 'pasadena' ) . '</p>',

		  'logged_in_as'    => '<p class="mleft logged-in-as">' .
		    sprintf(
		    esc_html__( 'Logged in as %s.', 'pasadena' ), $current_user->display_name ) . ' <a href="' . wp_logout_url( apply_filters( 'the_permalink', get_permalink() ) ) . '" title="' . esc_html__( 'Log out of this account', 'pasadena' ) . '">' . esc_html__( 'Log out?', 'pasadena' ) . '</a>' . '</p>',

		    // 'comment_notes_before' => '<p class="mleft comment-notes"> ' .  esc_html__( 'Your email address will not be published.', 'pasadena' ) . '</p>',
            'comment_notes_before' => '',

		    'fields' => apply_filters( 'comment_form_default_fields', array(
    		    'author' => '<input id="name" class="name-field" name="author" type="text" value="' . esc_html( $current_user->display_name ) . '" size="30"' . $aria_req . '  title="' . esc_html__( 'Name', 'pasadena' ) . '" />',
                'email'  => '<input id="email" class="email-field" name="email" type="text" value="' . esc_html(  $current_user->user_email ) . '" size="30"' . $aria_req . ' title="' . esc_html__( 'Email', 'pasadena' ) . '" />',
                'url'    => '<input id="website" name="url" class="website-field"  type="text" value="' . esc_html( $current_user->user_url ) . '" size="30" title="' .  esc_html__( 'Website', 'pasadena' ) . '" />'
		    )

		  )
		);

        return comment_form( $args, $post->ID );
	}
endif;

if( !function_exists( 'fastwp_pasadena_comment_form_submit_button' ) ) {
    function fastwp_pasadena_comment_form_submit_button( $defaults ) {

    $button = '<div class="clear"></div>

        <div class="holder">
        <input class="send-btn" value="Submit" id="submit" name="Submit" type="submit">
        <h6 class="social-header"><i class="fa fa-commenting"></i> ' . esc_html__( 'Comment', 'pasadena' ) . '</h6>
        <span class="arrow-right dark"></span></div>';

        $defaults['submit_button'] = $button;

        return $defaults;
    }
    add_filter( 'comment_form_defaults', 'fastwp_pasadena_comment_form_submit_button' );
}

if ( ! function_exists( 'fastwp_pasadena_comment' ) ) {

    function fastwp_pasadena_comment( $comment, $args, $depth ) {
        global $post;
        if( !isset( $post->ID ) ) return;
        $GLOBALS['comment'] = $comment;

        switch ( $comment->comment_type ) :

        case 'pingback':
        case 'trackback': ?>

        <li class="clearfix col--5 home auto">
          <div class="greyed">
            <p><?php esc_html_e( 'Pingback:', 'pasadena' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( 'Edit', 'pasadena' ), ' / <i class="edit-link">', '</i>' ); ?></p>
          </div>
        </li>

        <?php break;

        default: ?>

        <li id="comment-<?php echo get_comment_ID(); ?>" class="clearfix col--5 home auto">
          <div class="greyed">
            <p class="small">Comment <?php edit_comment_link( esc_html__( 'Edit', 'pasadena' ), ' / ', '' ); ?></p>
            <p style="float:right;">
            <?php echo comment_reply_link( array_merge( $args, array( 'reply_text' => '<span class="fa fa-reply"></span> ' . esc_html__( 'Reply', 'pasadena' ), 'before'=> '', 'after' => '', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>  </p>
            <h6><?php echo esc_html__( 'by ', 'pasadena' ) . ' ' . sprintf( '<span class="serif">%s</span>', get_comment_author() ); ?></h6>
            <p> <?php if( $comment->comment_approved == '0' ): ?>
            <?php esc_html_e( 'Your comment is awaiting moderation.', 'pasadena' ); ?>
            <?php else: ?>
            <?php comment_text(); ?>
            <?php endif; ?> </p>
            <p class="details"><?php echo get_comment_date('F j, Y'); ?></p>
          </div>
        </li>

        <?php break;
        endswitch; // end comment_type check
    }

}

// Move textarea
if( !function_exists( 'fastwp_pasadena_move_comtextarea' ) ) {
    function fastwp_pasadena_move_comtextarea( $fields ) {
        $comment_field = $fields['comment'];
        unset( $fields['comment'] );
        $fields['comment'] = $comment_field;
        return $fields;
    }

    add_filter( 'comment_form_fields', 'fastwp_pasadena_move_comtextarea' );
}

if( !function_exists( 'fastwp_pasadena_get_category_list' ) ) {
    function fastwp_pasadena_get_category_list( $strip_tags = false, $separator = ' / ', $post_id = '' ) {
        $categories_list = get_the_category_list( $separator, '', $post_id );
        if( $strip_tags == true ) {
            return strip_tags( $categories_list );
        } else {
            return $categories_list;
        }

    }
}

if( !function_exists( 'fastwp_pasadena_get_category_list_ab' ) ) {
    function fastwp_pasadena_get_category_list_ab( $post_id = '' ) {
        $categories_markup = '';
        $categories_list = get_the_category_list( '\/', '', $post_id );
        $categories_list = explode( '\/', $categories_list );
        if( !empty( $categories_list ) ) {
            foreach( $categories_list as $category ) {
                $categories_markup .= '<h4>' . $category . '</h4>';
            }
        }
        return $categories_markup;
    }
}

if ( !function_exists( 'fastwp_pasadena_single_pagination' ) ) {
    function fastwp_pasadena_single_pagination() {

        global $post;

        $prev_post = get_previous_post();
        $next_post = get_next_post();

        if( empty( $prev_post ) && empty( $next_post ) ) {
            return;
        }

        $markup = '<div class="container clearfix" id="project-pagination">
            <div class="col-lg-12">
                <div class="borderline"></div>
            </div>

            <div class="col-sm-4">';
                if( $prev_post ) {
                $markup .= '<a href="' . get_the_permalink( $prev_post->ID ) . '" title="">
                    <div class="prev-project">
                        <h6>' . strip_tags( $prev_post->post_title ) . '</h6>
                    </div>
                </a>';
                }
            $markup .= '</div>

            <div class="col-sm-4 centered">
                <a href="' . home_url( '/' ) . '" title="">
                    <h6><i class="fa fa-th"></i> ' . esc_html__( 'Overview', 'pasadena' ) . '</h6>
                </a>
            </div>

            <div class="col-sm-4">';
                if( $next_post ) {
                $markup .= '<a href="' . get_the_permalink( $next_post->ID ) . '" title="" class="alignright navalign">
                    <div class="next-project">
                        <h6>' . strip_tags( $next_post->post_title )  . '</h6>
                    </div>
                </a>';
                }
            $markup .= '</div>

            <div class="clear"></div>

            <div class="col-lg-12">
                <div class="borderline"></div>
            </div>
        </div>';

        return $markup;

    }
}

if ( !function_exists( 'fastwp_pasadena_author_social_networks' ) ) {
    function fastwp_pasadena_author_social_networks( $user_id = false ) {
        $markup = '';

        $profile_sn = get_the_author_meta( 'social_networks', $user_id );
                                          
        if( !empty( $profile_sn['icon'] ) ) {
            $markup .= '<div class="parent">
            <div class="bottom">
            <ul class="social-list clearfix">';
            foreach( $profile_sn['icon'] as $k => $icon ) {
                if( !empty( $icon ) ) {
                    $markup .= sprintf( '<li><a href="%s"> <i class="%s"></i></a></li>', esc_url( $profile_sn['url'][$k] ), esc_html( $icon ) );
                }
            }
            $markup .= '</div>
            </div>
            </ul>';
        }

        return $markup;
    }
}

/* Blog Pagination */

if ( ! function_exists( 'fastwp_pasadena_blog_pagination' ) ) {

    function fastwp_pasadena_blog_pagination( $args = array() ) {

        global $paged, $wp_query;

        $prev_post = get_previous_posts_page_link();
        $next_post = get_next_posts_page_link();
        $pp = get_next_posts_link();
        $np = get_previous_posts_link();

        $markup = '';

        if( $wp_query->max_num_pages > 0 ) {

            $markup = '<div class="container clearfix" id="project-pagination">
                <div class="col-lg-12">
                    <div class="borderline"></div>
                </div>

                <div class="col-sm-6">';
                    if( (int) $paged > 1 ) {
                    $markup .= '<a href="' . $prev_post . '" title="">
                        <div class="prev-project">
                            <h6>' . esc_html__( 'Newest Posts', 'pasadena' ). '</h6>
                        </div>
                    </a>';
                    }
                $markup .= '</div>



                <div class="col-sm-6">';
                    if( $wp_query->max_num_pages > (int) $paged ) {
                    $markup .= '<a href="' . $next_post . '" title="" class="alignright navalign">
                        <div class="next-project">
                            <h6>' . esc_html__( 'Older Posts', 'pasadena' ) . '</h6>
                        </div>
                    </a>';
                    }
                $markup .= '</div>

                <div class="clear"></div>

                <div class="col-lg-12">
                    <div class="borderline"></div>
                </div>
            </div>';

        }

        return $markup;

    }

}

if(!function_exists('fastwp_pasadena_comment_close_tag')):
	function fastwp_pasadena_comment_close_tag(){
	// </div></div>
}
endif;

if(!function_exists('fastwp_pasadena_widgets_init')):
function fastwp_pasadena_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Main Widget Area', 'pasadena' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'pasadena' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'fastwp_pasadena_widgets_init' );
endif;
