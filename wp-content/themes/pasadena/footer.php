</div>

<?php

do_action('fastwp_pasadena_before_footer');

$fastwp_pasadena_options = fastwp_pasadena_get_option( array( 'footer_text_left', 'footer_text_right' ) );

?>

<footer id="footer">
    <div class="container clearfix">
      <p class="alignleft"><?php echo ( !empty( $fastwp_pasadena_options['footer_text_left'] ) ? fastwp_pasadena_utils::fwp_escape( $fastwp_pasadena_options['footer_text_left'] ) : '' ); ?></p>
      <p class="alignright"><?php echo ( !empty( $fastwp_pasadena_options['footer_text_right'] ) ? fastwp_pasadena_utils::fwp_escape( $fastwp_pasadena_options['footer_text_right'] ) : '' ); ?></p>
    </div>
  </footer>

</div>

<?php wp_footer(); ?>

</body>

</html>