<?php

/**
The template for displaying posts in the Audio post format
*/

if( function_exists( 'redux_post_meta' ) ) {

$fastwp_pasadena_audio_post_url = redux_post_meta( 'fastwp_pasadena_data', $post->ID, 'post-audio-url' );

if( !empty( $fastwp_pasadena_audio_post_url ) ) { ?>
<iframe width="100%" height="166" scrolling="no" frameborder="no" src="//w.soundcloud.com/player/?url=<?php echo esc_attr( urlencode( $fastwp_pasadena_audio_post_url ), array( 'http', 'https' ) ); ?>&color=ff6600&auto_play=false&show_artwork=false"></iframe>
<?php }
}

?>