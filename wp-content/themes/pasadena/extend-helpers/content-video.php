<?php

/**
The template for displaying posts in the Video post format
*/

if( function_exists( 'redux_post_meta' ) ) {

global $post;

$fastwp_pasadena_video_post_url = redux_post_meta( 'fastwp_pasadena_data', $post->ID, 'post-video-url' );
$fastwp_pasadena_video_post_html = redux_post_meta( 'fastwp_pasadena_data', $post->ID, 'post-video-html' );

if( !empty( $fastwp_pasadena_video_post_url ) || !empty( $fastwp_pasadena_video_post_html ) ) {

?>

<div class="widescreen">
<?php echo fastwp_pasadena_video_post( $fastwp_pasadena_video_post_url, $fastwp_pasadena_video_post_html ); ?>
</div>

<?php }

}

?>