<?php

/**
The template for displaying posts in the Video post format
*/

?>
<div class="post-not-found">
    <?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

    <div class="element clearfix col--2-2 home">
        <div class="greyed">
        <?php printf( esc_html__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'pasadena' ), esc_url( admin_url( 'post-new.php' ) ) ); ?>
        </div>
    </div>

    <?php elseif ( is_search() ) : ?>

    <div class="element clearfix col--2-2 home">
        <div class="greyed">
            <?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'pasadena' ); ?>
            <?php get_search_form(); ?>
        </div>
    </div>

    <?php else : ?>

    <div class="element clearfix col--2-2 home">
        <div class="greyed">
            <?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'pasadena' ); ?>
            <?php get_search_form(); ?>
        </div>
    </div>

    <?php endif; ?>
</div>