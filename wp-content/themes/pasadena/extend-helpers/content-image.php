<?php if ( has_post_thumbnail() && !post_password_required() && !is_attachment() ) { ?>

<div class="borders image-post-type">
    <?php the_post_thumbnail( 'full' ); ?>
</div>

<?php } ?>