<?php

/**
The template for displaying posts in the Gallery post format
*/

if( function_exists( 'redux_post_meta' ) ) {

$fastwp_pasadena_gallery_post_url = redux_post_meta( 'fastwp_pasadena_data', $post->ID, 'post-gallery' );

if( !empty( $fastwp_pasadena_gallery_post_url ) ) {

?>

<div class="flexslider">
    <ul class="slides">
        <?php
            foreach( $fastwp_pasadena_gallery_post_url as $v ) {
                echo '<li><img src="' . esc_url( $v['image'] ) . '" alt="" /></li>';
            }
        ?>
    </ul>
</div>

<?php }

} ?>