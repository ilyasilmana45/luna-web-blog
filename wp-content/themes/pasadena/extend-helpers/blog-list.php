<?php

/**
The template for displaying posts in the Video post format
*/

$fastwp_pasadena_options = fastwp_pasadena_get_option( array( 'fwp_blog_date_meta' ) );

$fastwp_pasadena_is_sticky = is_sticky() && !is_search() && (int) $paged < 2 ? true : false;

?>

<div class="element clearfix col--1-2 home blog<?php echo ( $fastwp_pasadena_is_sticky ? ' post-is-sticky' : '' ); ?>">
    <a href="<?php the_permalink(); ?>" title="">
        <div class="greyed">
            <p class="small"><?php // echo fastwp_pasadena_get_category_list( true, ' / ' );
            echo esc_html__( 'Article', 'pasadena' ); ?></p>
            <h4><?php the_title(); ?></h4>
            <p><?php the_excerpt(); ?></p>
            <p class="details social-header"><?php echo esc_html__( 'By ', 'pasadena' ) . sprintf( '<span class="serif">%s</span>', get_the_author_meta( 'nickname', get_the_author_meta( 'ID' ) ) ); ?> <br>
                <?php echo ( empty( $fastwp_pasadena_options['fwp_blog_date_meta'] ) ? get_the_date() : '' ); ?>
            </p>
            <span class="arrow-right dark"></span>
        </div>
    </a>
</div>