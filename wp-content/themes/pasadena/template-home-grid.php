<?php

/* Template Name: Home Grid */

get_header();
the_post();

do_action('fastwp_pasadena_before_page_content');

?>

<div id="content">
    <div id="container" class="container clearfix">
        <?php FastWP_pasadena_UI::displayMultipage(); ?>
    </div>
</div>

<?php

do_action('fastwp_pasadena_after_page_content');
get_footer();