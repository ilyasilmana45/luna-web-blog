<?php

/* Template Name: Normal Page */

get_header();
the_post();

do_action('fastwp_pasadena_before_page_content');

?>

<section id="content">
    <div class="container clearfix">
		<?php $temp_content = get_the_content();
        $temp_content = apply_filters( 'the_content', $temp_content );
    	echo apply_filters( 'fastwp_pasadena_filter_section_content', $temp_content, $post ); ?>
	</div>
</section>

<?php

do_action('fastwp_pasadena_after_page_content');
                                  
get_footer();