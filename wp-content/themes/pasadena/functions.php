<?php

define( 'FASTWP_PASADENA_CORE_VERSION', '1.0.0' );
define( 'FASTWP_PASADENA_FWP_CORE_URI', sprintf( '%s/fastwp/', get_template_directory() ) );
define( 'FASTWP_PASADENA_FWP_EXTEND_URI', sprintf( '%s/extend/', get_template_directory() ) );
defined( 'FASTWP_PASADENA_MAIN_THEME_URL' ) or define( 'FASTWP_PASADENA_MAIN_THEME_URL', get_template_directory_uri() );
defined( 'FASTWP_PASADENA_CHILD_THEME_URL' ) or define( 'FASTWP_PASADENA_CHILD_THEME_URL', get_stylesheet_directory_uri() );
defined( 'FASTWP_PASADENA_SHORTCODE_CLASSNAME' ) or define( 'FASTWP_PASADENA_SHORTCODE_CLASSNAME', 'fastwp_pasadena_theme_shortcodes' );

define( 'FASTWP_PASADENA_LIB_URI', FASTWP_PASADENA_FWP_CORE_URI . 'lib/' );
define( 'FASTWP_PASADENA_LIB_URL', FASTWP_PASADENA_MAIN_THEME_URL . '/fastwp/lib/' );
defined( 'FASTWP_PASADENA_MENU_CHILD_CLASS' ) or define( 'FASTWP_PASADENA_MENU_CHILD_CLASS', '' );
defined( 'FASTWP_PASADENA_AUTOLOAD_SCRIPTS' ) or define( 'FASTWP_PASADENA_AUTOLOAD_SCRIPTS', true );
defined( 'FASTWP_PASADENA_AUTOLOAD_STYLES' ) or define( 'FASTWP_PASADENA_AUTOLOAD_STYLES', true );

require_once get_template_directory() . '/fastwp.theme.functions.php';

// Disable VC Front End Editor
if( function_exists( 'vc_disable_frontend' ) ){
	vc_disable_frontend();
}

if( !isset( $fastwp_pasadena_custom_js ) ){
	global $fastwp_pasadena_custom_js;
	$fastwp_pasadena_custom_js = array();
}

if( !isset( $fastwp_pasadena_raw_js ) ){
	global $fastwp_pasadena_raw_js;
	$fastwp_pasadena_raw_js = '';
}

require_once FASTWP_PASADENA_FWP_CORE_URI . 'fastwp.core.php';
require_once FASTWP_PASADENA_FWP_CORE_URI . 'fastwp.core.functions.php';
require_once FASTWP_PASADENA_FWP_CORE_URI . 'fastwp.core.actions.php';
require_once FASTWP_PASADENA_FWP_CORE_URI . 'fastwp.user.interface.php';
require_once FASTWP_PASADENA_FWP_CORE_URI . 'fastwp.abstract.php';
require_once FASTWP_PASADENA_FWP_CORE_URI . 'fastwp.settings.php';
require_once FASTWP_PASADENA_FWP_CORE_URI . 'fastwp.meta.php';
require_once FASTWP_PASADENA_FWP_CORE_URI . 'fastwp.utils.php';

if( is_admin() ) {
	require_once FASTWP_PASADENA_LIB_URI . 'php/plugin.activator.php';
	require_once FASTWP_PASADENA_FWP_CORE_URI . 'fastwp.admin.menu.php';
}

if( !function_exists( 'action_explorer' ) ) {
	add_action( 'fastwp_pasadena_shortcode_output', 'action_explorer' );
	function action_explorer( $a ){
		fastwp_debug::add_row("Called '$a'");
	}
}

// Set up the content width value based on the theme's design and stylesheet.
if( ! isset( $content_width ) )
	$content_width = 980;

if( is_singular() ) wp_enqueue_script( 'comment-reply' );

if( function_exists( 'vc_set_default_editor_post_types' ) ) {
	$list = array( 'page', 'fwp_portfolio' );
	vc_set_default_editor_post_types( $list );
}