jQuery( document ).ready(function( $ ) {

$( document ).on( 'click', '.delete-uexr.button', function(e) {
    e.preventDefault();
    var tr = $(this).parents( 'tr' );
    tr.remove();
});

$( document ).on( 'click', '.add-uexr.button', function(e) {
    e.preventDefault();
    var parent_tr = $(this).parents( 'tr' );
    var new_tr = parent_tr.nextAll( 'tr' );
    parent_tr.before( '<tr>' + new_tr.html() + '</tr>' );
});

});