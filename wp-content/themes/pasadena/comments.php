<?php

/*
 * The template for displaying Comments
 * The area of the page that contains comments and the comment form.

 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */

if ( post_password_required() || ( !have_comments() && !comments_open() ) ) return;

do_action( 'fastwp-pasadena-output-comment-number-markup' );

if ( have_comments() ) { ?>

<?php if ( !comments_open() ) { ?>
<div class="element clearfix col--5 home auto">
  <div class="greyed">
    <p class="no-comments"><?php echo esc_html__( 'Comments are closed.', 'pasadena' ); ?></p>
  </div>
</div>
<?php } ?>

<ul class="element clearfix col--5 no-padding-left home auto">
<?php wp_list_comments( array( 'callback' => 'fastwp_pasadena_comment', 'end-callback' => 'fastwp_pasadena_comment_close_tag', 'style' => '' ) ); ?>
</ul>

<?php

$fastwp_pasadena_prev_comments = get_previous_comments_link( '<span class="icon-left-open-mini left"></span> ' . esc_html__( 'Previous Comments', 'pasadena' ) );
$fastwp_pasadena_next_comments = get_next_comments_link( esc_html__( 'Next Comments', 'pasadena' ) . ' <span class="icon-right-open-mini right"></span>' );

if( !empty( $fastwp_pasadena_prev_comments ) || !empty( $fastwp_pasadena_next_comments ) ) {

?>

<div class="element clearfix col--5 home auto">
  <div class="greyed">
    <?php
        echo '<div class="col-sm-6"><div class="prev-project"><h6>' . ( !empty( $fastwp_pasadena_prev_comments ) ? str_replace( '<a', '<a class="pagination-previous"', $fastwp_pasadena_prev_comments ) : '' ) . '</h6></div></div>';
        echo '<div class="col-sm-6"><div class="next-project"><h6>' . ( !empty( $fastwp_pasadena_next_comments ) ? str_replace( '<a', '<a class="pagination-next"', $fastwp_pasadena_next_comments ) : '' ) . '</h6></div></div>';
    ?>
  </div>
</div>

<?php }

}
