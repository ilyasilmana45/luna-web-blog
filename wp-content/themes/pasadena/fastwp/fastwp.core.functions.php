<?php

class FastWP_Functions {
	/**
		Register theme scripts and load them depending on global settings.
	**/
    static function register_scripts(){
		global $fastwp_pasadena_load_scripts;
        if( is_single() ) {
            $script_key = array_search( 'jquery.sliphover.min', $fastwp_pasadena_load_scripts );
            unset( $fastwp_pasadena_load_scripts[$script_key] );
        }
		$fwp_themeUrl 	= (defined('FASTWP_PASADENA_MAIN_THEME_URL'))? FASTWP_PASADENA_MAIN_THEME_URL : get_template_directory_uri();
		$deps 			= 'jquery';
		$in_footer		= true;
		foreach($fastwp_pasadena_load_scripts as $script){
			$handle = sanitize_title($script);
			$src 	= sprintf('%s/assets/js/%s.js', $fwp_themeUrl, $script);
			if( substr_count( $script, '//' ) ){ 
				$src = $script;
			}
			wp_register_script( $handle, $src, $deps, '1.0.0', $in_footer );
			if(FASTWP_PASADENA_AUTOLOAD_SCRIPTS == true || $handle == 'main2' || $handle == 'jquery.isotope2.min'){
				wp_enqueue_script($handle);
			}
		}
	}
	/**
		Register theme styles and load them depending on global settings.
	**/
	static function register_styles(){
		global $fastwp_pasadena_load_styles;
		$fwp_themeUrl 	= (defined('FASTWP_PASADENA_MAIN_THEME_URL'))? FASTWP_PASADENA_MAIN_THEME_URL : get_template_directory_uri();
		$in_footer	= true;

        /* Include required Google foonts */

        $fonts = array();
        $fonts[] = 'Lato:400,600';
        $subsets = '';

        $fastwp_pasadena_load_styles[] = add_query_arg( array(
            'family' => urlencode( implode( '|', $fonts ) ),
            'subset' => urlencode( $subsets ),
        ), 'https://fonts.googleapis.com/css' );

        $fonts = array();
        $fonts[] = 'Droid Serif:400,700';
        $subsets = '';

        $fastwp_pasadena_load_styles[] = add_query_arg( array(
            'family' => urlencode( implode( '|', $fonts ) ),
            'subset' => urlencode( $subsets ),
        ), 'https://fonts.googleapis.com/css' );

		foreach($fastwp_pasadena_load_styles as $style){
			$handle = sanitize_title($style);
			$src 	= sprintf('%s/assets/css/%s.css', $fwp_themeUrl, $style);
			if( substr_count( $style,'//' ) ) {
				$src = $style;
			}
			wp_register_style( $handle, $src);
			if(FASTWP_PASADENA_AUTOLOAD_STYLES == true ){
				wp_enqueue_style($handle);
			}
		}
	}

	/** 

	**/
	static function add_custom_column(){
		global $fastwp_pasadena_custom_posts_with_id;
		foreach( $fastwp_pasadena_custom_posts_with_id as $custom_post => $fields ){
			add_filter( 'manage_edit-' . $custom_post . '_columns', array('FastWP_Pasadena_Actions', 'set_custom_edit_post_columns') );
            if( is_array( $fields ) && in_array( 'thumb', $fields ) ) {
			    add_filter( 'manage_edit-' . $custom_post . '_columns', array('FastWP_Pasadena_Actions', 'set_custom_preview_post_columns') );
            }
			add_action( 'manage_' . $custom_post . '_posts_custom_column' , array('FastWP_Pasadena_Actions', 'custom_ID_column'), 10, 2 );
		}
	}

}

add_action('init', array('FastWP_Functions', 'add_custom_column'));
add_action('wp_enqueue_scripts', array('FastWP_Functions', 'register_styles'));
add_action('wp_enqueue_scripts', array('FastWP_Functions', 'register_scripts'));