<?php

/**
  ReduxFramework Sample Config File
  For full documentation, please visit: https://docs.reduxframework.com
 * */

if (!class_exists('FastWP_Redux_Framework_config')) {

    class FastWP_Redux_Framework_config {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

            $this->initSettings();

        }

        public function initSettings() {

            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();


            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            // If Redux is running as a plugin, this will remove the demo notice and links
            //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

            // Function to test the compiler hook and demo CSS output.
            // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
            //add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 2);

            // Change the arguments after they've been declared, but before the panel is created
            //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );

            // Change the default value of a field after it's been set, but before it's been useds
            //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );

            // Dynamically add a section. Can be also used to modify sections/fields
            // add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        /**

          This is a test function that will let you see when the compiler hook occurs.
          It only runs if a field   set with compiler=>true is changed.

         * */
        function compiler_action($options, $css) {

        }

        /**

          Custom function for filtering the sections array. Good for child themes to override or add to the sections.
          Simply include this function in the child themes functions.php file.

          NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
          so you must use get_template_directory_uri() if you want to use any of the built in icons

         * */
        function dynamic_section($sections) {
            //$sections = array();
            /*$sections[] = array(
                'title' => esc_html__('Section via hook', 'pasadena'),
                'desc' => esc_html__('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'pasadena'),
                'icon' => 'el-icon-paper-clip',
                'fields' => array()
            );*/

            return $sections;
        }

        /**

          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        function change_arguments($args) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        function change_defaults($defaults) {
         //   $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        public function setSections() {

            $this->sections[] = array(
                'icon'      => 'el-icon-wrench',
                'title'     => esc_html__('General', 'pasadena'),
                'fields'    => array(

                    array(
                        'id'        => 'opt-info-field',
                        'type'      => 'info',
                        'title'  => esc_html__('Welcome to Gem Options Panel.', 'pasadena'),
                        'desc'      => esc_html__('It is meant to make your life easier by offering you options which will customize your website. Here you can set all general options that affects entire website.', 'pasadena')
                    ),
                    array(
                        'id'        => 'info_bubble_1_text',
                        'type'      => 'editor',
                        'required'  => array('use_info_bubble_1', '=', '1'),
                        'title'     => esc_html__('Info bubble #1 content', 'pasadena'),
                        'subtitle'  => esc_html__('Place here your info bubble content.', 'pasadena')
                    ),
                    array(
                        'id'        => 'fastwp_logo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Custom Logo', 'pasadena'),
                        'subtitle'  => esc_html__('Upload an image that will represent your website`s logo.', 'pasadena')  ,
                        'default'   => ''
                    ),
                    array(
                        'id'        => 'custom_js',
                        'type'      => 'ace_editor',
                        'title'     => esc_html__('Custom JS', 'pasadena'),
                        'subtitle'  => esc_html__('Paste your JavaScript code here. Use this field to quickly add JS code snippets.', 'pasadena'),
                        'mode'      => 'javascript',
                        'theme'     => 'chrome',
                        'default'   => ""
                    )
                )
            );

            $this->sections[] = array(
                'icon'      => 'el el-fast-forward',
                'title'     => esc_html__('Preloader Options', 'pasadena'),
                'fields'    => array(
                    array(
                        'id'        => 'show_preloader',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show preloader', 'pasadena'),
                        'subtitle'  => esc_html__('Enable/Disable preloader.', 'pasadena'),
                        'default'   => 1,
                        'on'        => 'Enabled',
                        'off'       => 'Disabled'
                    ),
                    array(
                        'id'        => 'preloader_text',
                        'type'      => 'text',
                        'url'       => true,
                        'title'     => esc_html__('Preloader Text', 'pasadena'),
                        'default'   => 'loading',
                        'required'  => array('show_preloader', '=', '1')
                    ),
                    array(
                        'id' => 'preloader_text_style',
                        'type' => 'typography',
                        'title' => esc_html__('Preloader Text Style', 'pasadena'),
                        'output'    => array('#preloader p'),
                        'google' => true,
                        'default' => array(
                            'font-family' => '',
                            'font-size' => '',
                            'line-height' => '',
                            'font-weight' => '',
                            'color' => '',
                        ),
                        'required'  => array('show_preloader', '=', '1')
                    ),
                    array(
                        'id'        => 'preloader_background',
                        'type'      => 'background',
                        'url'       => true,
                        'title'     => esc_html__( 'Preloader Background', 'pasadena' ),
                        'output'    => array('#preloader'),
                        'background-color'      => true,
                        'background-image'       => false,
                        'background-size'       => false,
                        'background-attachment' => false,
                        'background-repeat'     => false,
                        'background-position'   => false,
                        'required'  => array( 'show_preloader', '=', '1' )
                    )
                )

            );

            $this->sections[] = array(
                'icon'      => 'el-icon-lines',
                'title'     => esc_html__('Menu', 'pasadena'),
                'fields'    => array(
                    array(
                        'id'        => 'menu_social_media',
                        'type'      => 'multi_text',
                        'title'     => esc_html__('Menu Social Media', 'pasadena'),
                        'subtitle'  => esc_html__('Set any social media networks you want', 'pasadena'),
                        'desc'      => esc_html__('Format is FontAwesome | URL. Example - fa fa-facebook | https://www.facebook.com/ ', 'pasadena')
                    ),
                    array(
                        'id'        => 'menu_links_color',
                        'type'      => 'link_color',
                        'title'     => esc_html__('Menu items color', 'pasadena'),
                        'subtitle'  => esc_html__('Select menu items colors for default, hover and active state', 'pasadena'),
                        'google'    => true,
                        'text-align'=> false,
                        'subsets'   => false,
                        'color'     => false,
                        'font-weight'=> false,
                        'font-style'=> false,
                        'font-size' => true,
                        'line-height'=> false,
                        'all_styles'=> true,
                        'units'     => 'px',
                        'output'    => array('#main-nav ul li a'),
                        'default'   => array(
                            'regular'  => '',
                            'hover'    => '',
                            'active'   => '',
                            'visited'  => ''
                        )
                    )
                ),
            );

            // Typograply
            $this->sections[] = array(
                'icon' => 'el-icon-font',
                'title' => esc_html__('Typography Options', 'pasadena'),
                'fields' => array(
                    array(
                        'id' => 'body_font',
                        'type' => 'typography',
                        'title' => esc_html__('Body Font Setting', 'pasadena'),
                        'subtitle' => esc_html__('Specify the body font properties.', 'pasadena'),
                        'google' => true,
                        'default' => array(
                            'font-family' => '',
                            'font-size' => '',
                            'line-height' => '',
                            'font-weight' => '',
                            'color' => '',
                        ),
                    ),
                    array(
                        'id' => 'blog_font',
                        'type' => 'typography',
                        'title' => esc_html__('Blog Font Setting', 'pasadena'),
                        'subtitle' => esc_html__('Specify the body font blog properties.', 'pasadena'),
                        'google' => true,
                        'default' => array(
                            'font-family' => '',
                            'font-size' => '',
                            'line-height' => '',
                            'font-weight' => '',
                            'color' => '',
                        ),
                    ),
                    array(
                        'id' => 'h1_font',
                        'type' => 'typography',
                        'title' => esc_html__('Heading 1(H1) Font Setting', 'pasadena'),
                        'subtitle' => esc_html__('Specify the H1 tag font properties.', 'pasadena'),
                        'units' => 'em',
                        'google' => true,
                        'default' => array(
                            'font-family' => '',
                            'font-weight' => '',
                            'color' => '',
                        )
                    ),
                    array(
                        'id' => 'h2_font',
                        'type' => 'typography',
                        'title' => esc_html__('Heading 2(H2) Font Setting', 'pasadena'),
                        'subtitle' => esc_html__('Specify the H2 tag font properties.', 'pasadena'),
                        'google' => true,
                        'default' => array(
                            'font-family' => '',
                            'font-weight' => '',
                            'color' => '',
                        )
                    ),
                    array(
                        'id' => 'h3_font',
                        'type' => 'typography',
                        'title' => esc_html__('Heading 3(H3) Font Setting', 'pasadena'),
                        'subtitle' => esc_html__('Specify the H3 tag font properties.', 'pasadena'),
                        'google' => true,
                        'default' => array(
                            'font-family' => '',
                            'font-weight' => '',
                            'color' => '',
                        )
                    ),
                    array(
                        'id' => 'h4_font',
                        'type' => 'typography',
                        'title' => esc_html__('Heading 4(H4) Font Setting', 'pasadena'),
                        'subtitle' => esc_html__('Specify the H4 tag font properties.', 'pasadena'),
                        'google' => true,
                        'default' => array(
                            'font-family' => '',
                            'font-weight' => '',
                            'color' => '',
                        )
                    ),
                    array(
                        'id' => 'h5_font',
                        'type' => 'typography',
                        'title' => esc_html__('Heading 5(H5) Font Setting', 'pasadena'),
                        'subtitle' => esc_html__('Specify the H5 tag font properties.', 'pasadena'),
                        'google' => true,
                        'default' => array(
                            'font-family' => '',
                            'font-weight' => '',
                            'color' => '',
                        )
                    ),
                    array(
                        'id' => 'h6_font',
                        'type' => 'typography',
                        'title' => esc_html__('Heading 6(H6) Font Setting', 'pasadena'),
                        'subtitle' => esc_html__('Specify the H6 tag font properties.', 'pasadena'),
                        'google' => true,
                        'default' => array(
                            'font-family' => '',
                            'font-weight' => '',
                            'color' => '',
                        )
                    )
                )
            );

            $this->sections[] = array(
                'icon'      => 'el-icon-th-list',
                'title'     => esc_html__('Blog', 'pasadena'),
                'fields'    => array(
                    array(
                        'id'        => 'fwp_hide_blog_title',
                        'type'      => 'switch',
                        'title'     => esc_html__('Hide Blog Title', 'pasadena'),
                        'subtitle'  => esc_html__('Shows or hides the blog title.', 'pasadena'),
                        'on'        => 'Yes',
                        'off'       => 'No',
                        'default'   => false,
                    ),
                    array(
                        'id'        => 'fwp_blog_title',
                        'type'      => 'text',
                        'title'     => esc_html__( 'Blog Title', 'pasadena' ),
                        'subtitle'  => esc_html__( 'Blog title', 'pasadena' ),
                        'required'  => array( 'fwp_hide_blog_title', '=', '0' ),
                    ),
                    array(
                        'id'        => 'fwp_blog_bg',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__( 'Blog Title Background', 'pasadena' ),
                        'subtitle'  => esc_html__( 'Blog title background', 'pasadena' ),
                        'required'  => array( 'fwp_hide_blog_title', '=', '0' ),
                    ),
                    array(
                        'id'        => 'fwp_blog_date_meta',
                        'type'      => 'switch',
                        'title'     => esc_html__( 'Hide Date Info', 'pasadena' ),
                        'subtitle'  => esc_html__( 'Shows or hides the date information on single.', 'pasadena' ),
                        'on'        => 'Yes',
                        'off'       => 'No',
                        'default'   => false,
                    ),
                    array(
                        'id'        => 'fwp_blog_author_bio',
                        'type'      => 'switch',
                        'title'     => esc_html__( 'Hide Author Info', 'pasadena' ),
                        'subtitle'  => esc_html__( 'Shows or hides the author information on single.', 'pasadena' ),
                        'on'        => 'Yes',
                        'off'       => 'No',
                        'default'   => false,
                    ),
                    /*
                    array(
                        'id'        => 'fwp_blog_sidebar',
                        'type'      => 'switch',
                        'title'     => esc_html__( 'Hide Single Sidebar', 'pasadena' ),
                        'subtitle'  => esc_html__( 'Shows or hides the sidebar on single.', 'pasadena' ),
                        'on'        => 'Yes',
                        'off'       => 'No',
                        'default'   => false,
                    ),
                    */
                )
            );

            $this->sections[] = array(
                'icon'      => 'el-icon-folder',
                'title'     => esc_html__( 'Portfolio', 'pasadena' ),
                'fields'    => array(
                    array(
                        'id'        => 'fastwp_portfolio_slug',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom Slug', 'pasadena'),
                        'desc'      => esc_html__('Use this option in order to change the default portfolio slug (default fwp_portfolio), you need to refresh your permalink page after updating this field.','pasadena'),
                        'default'   => 'fwp_portfolio'
                    ),
                )
            );

            $this->sections[] = array(
                'icon'      => 'el-icon-inbox',
                'title'     => esc_html__('Footer', 'pasadena'),
                'fields'    => array(
                    array(
                        'id'        => 'footer_text_left',
                        'type'      => 'editor',
                        'title'     => esc_html__('Footer copyright (left)', 'pasadena'),
                        'subtitle'  => esc_html__('Place here your copyright info.', 'pasadena'),
                        'default'   => ''
                    ),
                    array(
                        'id'        => 'footer_text_right',
                        'type'      => 'editor',
                        'title'     => esc_html__('Footer text (right)', 'pasadena'),
                        'subtitle'  => esc_html__('Place here your text info.', 'pasadena'),
                        'default'   => ''
                    )
                )
            );

            $this->sections[] = array(
                'icon'      => 'el-icon-inbox',
                'title'     => esc_html__('404 Page', 'pasadena'),
                'fields'    => array(
                    array(
                        'id'        => 'e404_text',
                        'type'      => 'editor',
                        'title'     => esc_html__('Error page content', 'pasadena'),
                        'subtitle'  => esc_html__('Place here your text for 404 page.', 'pasadena'),
                        'default'   => 'Ooops...The page you are looking for could not be found! Please, go back to our home page and start over or contact us if the problem persists.'
                    ),
                    array(
                        'id'        => 'e404_override',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show this page content as 404', 'pasadena'),
                        'subtitle'  => esc_html__('Override default 404 page with page content.', 'pasadena'),
                        'default'   => 0,
                        'on'        => 'Enabled',
                        'off'       => 'Disabled'
                    ),
                    array(
                        'id'        => 'e404_page_id',
                        'type'      => 'select',
                        'title'     => esc_html__('Override 404', 'pasadena'),
                        'subtitle'  => esc_html__('Override default 404 page with this page content.', 'pasadena'),
                        'options'   => FastWP_Pasadena_getPostIdAndTitle(),
                    ),

                ),
            );

        }

        /**

          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'fastwp_pasadena_data',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'submenu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => esc_html__('Theme Options', 'pasadena'),
                'page_title'        => esc_html__('Theme Options', 'pasadena'),

                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => 'AIzaSyBPVwg6CaFLmKlxYjQu0bJGpxDN1p04S-Q', // Must be defined to add google fonts to the typography module

                'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => true,                    // Enable basic customizer support

                // OPTIONAL -> Give you extra features
                'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => '',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => '_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *

                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'show_import_export'    => true, // REMOVE
                'system_info'           => false, // REMOVE

                // HINTS
                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
                        ),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                )
            );


            // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
            $this->args['share_icons'][] = array(
                'url'   => 'http://twitter.com/fastwp',
                'title' => 'Follow FastWP on Twitter',
                'icon'  => 'el-icon-twitter'
            );
            $this->args['share_icons'][] = array(
                'url'   => 'http://themeforest.net/user/fastwp/portfolio',
                'title' => 'Fastwp Official Page',
                'icon'  => 'el-icon-link'
            );
            $this->args['share_icons'][] = array(
                'url'   => 'mailto:themes@fastwp.net',
                'title' => 'Send an email to fastwp',
                'icon'  => 'el-icon-envelope'
            );
        }

    }

    global $reduxConfig;
    $reduxConfig = new FastWP_Redux_Framework_config();
}

/**
Import custom settings after demo content import
**/
if ( !function_exists( 'wbc_demo_load' ) ) {
    function wbc_demo_load( $demo_active_import , $demo_directory_path ) {
        reset( $demo_active_import );
        $current_key = key( $demo_active_import );

        /* Menu import */
        $wbc_menu_array = array(
            'demo-gridpage' => 'Menu'
        );

        if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && (array_key_exists($demo_active_import[$current_key]['directory'], $wbc_menu_array) || in_array( $demo_active_import[$current_key]['directory'], $wbc_menu_array )) ) {
            $top_menu = get_term_by( 'name', $wbc_menu_array[$demo_active_import[$current_key]['directory']], 'nav_menu' );

			if ( isset( $top_menu->term_id ) ) {
                set_theme_mod( 'nav_menu_locations', array(
                        'primary' => $top_menu->term_id
                    )
                );
            }
        }

        /* Homepage select */
        $wbc_home_pages = array(
            'demo-gridpage' => 'Front Page'
        );
        if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_home_pages ) ) {
            $page = get_page_by_title( $wbc_home_pages[$demo_active_import[$current_key]['directory']] );
            if ( isset( $page->ID ) ) {
                update_option( 'page_on_front', $page->ID );
                update_option( 'show_on_front', 'page' );
            }
        }

        /* Blog page select
        $wbc_home_pages = array(
            'demo-gridpage' => 'Blog',
        );
        if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_home_pages ) ) {
            $page = get_page_by_title( $wbc_home_pages[$demo_active_import[$current_key]['directory']] );
            if ( isset( $page->ID ) ) {
                update_option( 'page_for_posts', $page->ID );
            }
        }
        */

    }
    add_action( 'wbc_importer_after_content_import', 'wbc_demo_load', 10, 2 );
}

/**
Set demo content path
**/

if ( !function_exists( 'wbc_change_demo_directory_path' ) ) {
    function wbc_change_demo_directory_path( $demo_directory_path ) {
        $demo_directory_path = get_template_directory().'/demo-data/';
        return $demo_directory_path;
    }

    add_filter('wbc_importer_dir_path', 'wbc_change_demo_directory_path' );
}



if ( !function_exists( 'wbc_filter_title' ) ) {
    function wbc_filter_title( $title ) {
        return ucfirst( trim( str_replace( "-", " ", str_replace('demo','',$title )) ) );
    }
    add_filter( 'wbc_importer_directory_title', 'wbc_filter_title', 10 );
}