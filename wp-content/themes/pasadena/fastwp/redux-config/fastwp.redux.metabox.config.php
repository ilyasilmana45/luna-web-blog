<?php

// You may replace $redux_opt_name with a string if you wish. If you do so, change loader.php
// as well as all the instances below.

if( !function_exists( 'fastwp_pasadena_redux_get_PostIdAndTitle' ) ) {
	function fastwp_pasadena_redux_get_PostIdAndTitle($post_type = 'page', $value_first = false){
		global $wpdb;
		$sql = $wpdb->prepare("SELECT post_title, ID from $wpdb->posts WHERE `post_type` = '%s' AND `post_status`='publish' ORDER BY `post_title` ASC;",
			$post_type);
		$result = $wpdb->get_results( $sql, ARRAY_A );
		$output = array();
        $output['home'] = 'Home';
		if(isset($result) && is_array($result)){
			foreach($result as $item){
				if($value_first == true){
					$key = $item['post_title'];
					$value=$item['ID'];
				}else {
					$key = $item['ID'];
					$value=$item['post_title'];
				}
				$output[$key] = $value;
			}
		}
		return $output;
	}
}

if ( !function_exists( "fastwp_pasadena_add_metaboxes" ) ){
    function fastwp_pasadena_add_metaboxes( $metaboxes ) {

        $blog_posts = $portfolio_posts = $metaboxes = array();

        $portfolio_posts[] = array(
            'icon'          => 'el-icon-screen',
            'icon_class'    => 'icon_large',
            'title'         => esc_html( 'General', 'pasadena' ),
            'fields'        => array(
                array(
                    'id'        => 'col_size',
                    'type'      => 'select',
                    'title'     => esc_html( 'Item Size', 'pasadena' ),
                    'desc'      => esc_html( 'If default, it will use the one set in shortcode', 'pasadena' ),
                    'options'   => array(
                                        '1' => esc_html__( '1 Column With Background', 'pasadena' ),
                                        '2' => esc_html__( '2 Columns Large With Background', 'pasadena' ),
                                        '3' => esc_html__( '2 Columns Tall & Large With Background', 'pasadena' ),
                                        '4' => esc_html__( '1 Column Without Background', 'pasadena' )
                                        )
                ),
                array(
                    'id'        => 'item_show_on',
                    'type'      => 'checkbox',
                    'title'     => esc_html( 'Show Items On', 'pasadena' ),
                    'desc'      => esc_html( 'If default, it will use the one set in shortcode', 'pasadena' ),
                    'options'   => fastwp_pasadena_redux_get_PostIdAndTitle(),
                ),
        		array(
        			'id'        => 'pos_on_home',
        			'type'      => 'text',
        			'title'     => esc_html__( 'Position of Element', 'pasadena' ),
        			'subtitle'  => esc_html__( 'Default: 99', 'pasadena' ),
        			'default'   => '99',
        		),
            )
        );

        $portfolio_posts[] = array(
            'icon'          => 'el-icon-bookmark',
            'icon_class'    => 'icon_large',
            'title'         => esc_html( 'Hero Section', 'pasadena' ),
            'fields'        => array(
                array(
                    'id'        => 'use_hero_section',
                    'type'      => 'select',
                    'title'     => esc_html( 'Use Hero Section', 'pasadena' ),
                    'options'   => array(
                                        0 => esc_html__( 'No', 'pasadena' ),
                                        1 => esc_html__( 'Yes', 'pasadena' ),
                                        )
                ),
                array(
                     'id'       => 'hero_image',
                     'type'     => 'media',
                     'title'    => esc_html__( 'Hero Background', 'pasadena' ),
                     'subtitle' => esc_html__( 'Image background used in hero section', 'pasadena' ),
                     'required' => array( 'use_hero_section', 'equals', 1 )
                ),
        		array(
        			'id'        => 'hero_title',
        			'type'      => 'text',
        			'title'     => esc_html__( 'Hero Title', 'pasadena' ),
                    'required'  => array( 'use_hero_section', 'equals', 1 )
        		),
        		array(
        			'id'        => 'hero_text',
        			'type'      => 'textarea',
        			'title'     => esc_html__( 'Hero Title', 'pasadena' ),
                    'required'  => array( 'use_hero_section', 'equals', 1 )
        		),
            )
        );

        $blog_posts[] = array(
        	'icon' => 'el-icon-screen',
        	'fields' => array(
                array(
                    'id'        => 'item_show_on',
                    'type'      => 'checkbox',
                    'title'     => esc_html( 'Show Items On', 'pasadena' ),
                    'desc'      => esc_html( 'If default, it will use the one set in shortcode', 'pasadena' ),
                    'options'   => fastwp_pasadena_redux_get_PostIdAndTitle(),
                    'default'   => ''
                ),
        		array(
        			'id'        => 'pos_on_home',
        			'type'      => 'text',
        			'title'     => esc_html__( 'Position of Element', 'pasadena' ),
        			'subtitle'  => esc_html__( 'Default: 99', 'pasadena' ),
        			'default'   => '99',
        		),
            )
        );

        // Video Post
        $boxSectionsVideoPost = array();
        $boxSectionsVideoPost[] = array(
        	'icon' => 'el-icon-screen',
        	'fields' => array(
        		array(
        			'id'        => 'post-video-url',
        			'type'      => 'text',
        			'title'     => esc_html__( 'Video URL', 'pasadena' ),
        			'subtitle'  => esc_html__( 'YouTube or Vimeo video URL', 'pasadena' ),
        			'default'   => '',
        		),
        		array(
        			'id'         => 'post-video-html',
        			'type'       => 'textarea',
        			'title'      => esc_html__( 'Embedded video', 'pasadena' ),
        			'subtitle'   => esc_html__( 'Use this option when the video does not come from YouTube or Vimeo', 'pasadena' ),
        			'default'    => '',
        		),
        	)
        );

        // Gagllery Post
        $boxSectionsGalleryPost = array();
        $boxSectionsGalleryPost[] = array(
        	'icon' => 'el-icon-screen',
        	'fields' => array(
        		array(
        			'id'        => 'post-gallery',
        			'type'      => 'slides',
        			'title'     => esc_html__( 'Gallery Slider', 'pasadena' ),
        			'subtitle'  => esc_html__( 'Upload images or add from media library.', 'pasadena' ),
        			'placeholder'   => array(
        				'title'         => esc_html__( 'Title', 'pasadena' ),
        			),
        			'show' => array(
        				'title'         => true,
        				'description'   => false,
        				'url'           => false,
        			)
        		),
        	)
        );

        // Audio Post
        $boxSectionsAudioPost = array();
        $boxSectionsAudioPost[] = array(
        	'icon' => 'el-icon-screen',
        	'fields' => array(
        		array(
        			'id'        => 'post-audio-url',
        			'type'      => 'text',
        			'title'     => esc_html__( 'Audio URL', 'pasadena' ),
        			'subtitle'  => esc_html__( 'Audio file URL in format: mp3, ogg, wav.', 'pasadena' ),
        			'default'   => '',
        		),
        	)
        );

        // Declare metaboxes

        $metaboxes[] = array(
            'id'            => 'sidebar',
            'title'         => esc_html( 'Pasadena Options', 'pasadena' ),
            'post_types'    => array( 'fwp_portfolio' ),
            'position'      => 'normal', // normal, advanced, side
            'priority'      => 'high', // high, core, default, low - Priorities of placement
            'sections'      => $portfolio_posts,
        );
        $metaboxes[] = array(
		    'id'            => 'post-options',
            'title'         => esc_html__( 'Pasadena Options', 'pasadena' ),
            'post_types'    => array( 'post' ),
            'position'      => 'normal', // normal, advanced, side
            'priority'      => 'high', // high, core, default, low - Priorities of placement
            'sections'      => $blog_posts
        );
        $metaboxes[] = array(
		    'id'            => 'video-post-options',
            'title'         => esc_html__( 'Pasadena Options', 'pasadena' ),
            'post_types'    => array( 'post' ),
            'position'      => 'normal', // normal, advanced, side
            'priority'      => 'high', // high, core, default, low - Priorities of placement
            'sections'      => $boxSectionsVideoPost,
            'post_format'   => array( 'video' ),
        );
        $metaboxes[] = array(
		    'id'            => 'gallery-post-options',
            'title'         => esc_html__( 'Pasadena Options', 'pasadena' ),
            'post_types'    => array( 'post' ),
            'position'      => 'normal', // normal, advanced, side
            'priority'      => 'high', // high, core, default, low - Priorities of placement
            'sections'      => $boxSectionsGalleryPost,
            'post_format'   => array( 'gallery' ),
        );
        $metaboxes[] = array(
            'id'            => 'audio-post-options',
            'title'         => esc_html__( 'Pasadena Options', 'pasadena' ),
            'post_types'    => array( 'post' ),
            'post_format'   => array('audio'),
            'position'      => 'normal', // normal, advanced, side
            'priority'      => 'high', // high, core, default, low - Priorities of placement
            'sections'      => $boxSectionsAudioPost,
        );

        return $metaboxes;
    }

    add_action( 'redux/metaboxes/fastwp_pasadena_data/boxes', 'fastwp_pasadena_add_metaboxes' );

}