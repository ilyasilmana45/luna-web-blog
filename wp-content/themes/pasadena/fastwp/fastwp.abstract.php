<?php

if(!function_exists('Fastwp_Pasadena_UI_getMenu')){
	function Fastwp_Pasadena_UI_getMenu($id = '', $class = 'menu', $echo = true ){
		return FastWP_pasadena_UI::getMenu($id, $class, $echo);
	}
}
if(!function_exists('Fastwp_Pasadena_UI_displayMenu')){
	function Fastwp_Pasadena_UI_displayMenu( $echo = true ){
		return FastWP_pasadena_UI::displayMenu( $echo );
	}
}
if(!function_exists('Fastwp_Pasadena_UI_getPreloaderMarkup')){
	function Fastwp_Pasadena_UI_getPreloaderMarkup(){
		return FastWP_pasadena_UI::getPreloaderMarkup();
	}
}
if(!function_exists('Fastwp_Pasadena_UI_getNavMenuItems')){
	function Fastwp_Pasadena_UI_getNavMenuItems($showHome = '0'){
		return FastWP_pasadena_UI::getNavMenuItems($showHome);
	}
}
if(!function_exists('Fastwp_pasadena_UI_showPreloader')){
	function Fastwp_pasadena_UI_showPreloader($forceVisible = false){
		return FastWP_pasadena_UI::showPreloader($forceVisible);
	}
}
if(!function_exists('Fastwp_Pasadena_UI_getPreloaderStatusForCurrentPage')){
	function Fastwp_Pasadena_UI_getPreloaderStatusForCurrentPage(){
		return FastWP_pasadena_UI::getPreloaderStatusForCurrentPage();
	}
}
if(!function_exists('Fastwp_pasadena_UI_alert')){
	function Fastwp_pasadena_UI_alert($type, $message){
		return FastWP_pasadena_UI::alert($type, $message);
	}
}
if( !function_exists( 'fastwp_pasadena_escape' ) ) {
	function fastwp_pasadena_escape( $html ){
		return fastwp_pasadena_utils::fwp_escape( $html );
	}
}
if( !function_exists( 'fastwp_pasadena_add_body_class' ) ) {
    function fastwp_pasadena_add_body_class( $class = array() ) {
        global $fastwp_pasadena_body_classes;
        $fastwp_pasadena_body_classes = array_merge( (array) $fastwp_pasadena_body_classes, $class );
    }
}