<?php

add_action('init', 						array('FastWP_Pasadena_Actions', 'init'), 10);
add_action('wp_enqueue_scripts',		array('FastWP_Pasadena_Actions', 'enqueue_scripts'));
add_action('admin_enqueue_scripts',		array('FastWP_Pasadena_Actions', 'enqueue_scripts'));
add_action('tgmpa_register', 			array('FastWP_Pasadena_Actions', 'check_required_plugins') );
add_action('attach_footer_info_page', 	array('FastWP_Pasadena_Actions', 'attach_footer_info_page'));
add_action('vc_before_init', 			array('FastWP_Pasadena_Actions', 'set_vc_as_theme'));
add_action('after_setup_theme', 		array('FastWP_Pasadena_Actions', 'fastwp_setup'), 11);
add_action('_fastwp_no_sections_defined',array('FastWP_Pasadena_Actions', 'no_section_defined'));
add_action('fastwp_pasadena_enqueue_script', array('FastWP_Pasadena_Actions', 'script_loader'),1);
add_action('wp_footer',					array('FastWP_Pasadena_Actions', 'footer'),1);
add_action('wp_footer', 				array('FastWP_Pasadena_UI', 		'add_custom_css' ), 1000 );
add_action('wp_update_nav_menu_item',   array('FastWP_Pasadena_Actions', 'fastwp_navigation_update'),1,3 );
add_filter('body_class', 				array( 'FastWP_Pasadena_Actions', 'body_class' ) );
add_action( 'show_user_profile',        array('FastWP_Pasadena_Actions','fastwp_add_user_prof_fields' ) );
add_action( 'edit_user_profile',        array('FastWP_Pasadena_Actions','fastwp_add_user_prof_fields' ) );
add_action( 'personal_options_update',  array('FastWP_Pasadena_Actions','fastwp_save_user_prof_fields' ) );
add_action( 'edit_user_profile_update', array('FastWP_Pasadena_Actions','fastwp_save_user_prof_fields' ) );

class FastWP_Pasadena_Actions {
	static function init(){
		self::init_visual_composer_blocks();
	}

	static function body_class( $classes ) {
	    global $fastwp_pasadena_body_classes;
		// $classes[] = 'class here';
		return array_merge( $classes, (array) $fastwp_pasadena_body_classes );
	}

	static function footer(){
		global $fastwp_pasadena_custom_js, $fastwp_pasadena_data;
		wp_localize_script('custom', 'fastwp', $fastwp_pasadena_custom_js );
		if(isset($fastwp_pasadena_data['custom_js']) && !empty($fastwp_pasadena_data['custom_js'])){
		wp_add_inline_script('main', $fastwp_pasadena_data['custom_js']);
		}
	}


	static function  fastwp_setup(){
	    load_theme_textdomain	('fastwp', get_template_directory() . '/locale');
		add_theme_support		('post-formats', array('quote', 'video', 'image', 'audio', 'gallery', 'aside'));
		add_image_size			('fastwp-pasadena-portfolio-thumb', 370, 241, true ); // Portfolio 370x241 image (cropped if larger)
		$args = array();

		// This theme styles the visual editor with editor-style.css to match the theme style.
		add_editor_style();

		// Adds RSS feed links to <head> for posts and comments.
		add_theme_support( 'automatic-feed-links' );

		// This theme uses wp_nav_menu() in one location.
		add_theme_support('menus');
		register_nav_menus( array(
			'primary' => esc_html__('Main navigation menu', 'pasadena')
		) );

		add_theme_support( 'title-tag' );

		// This theme uses a custom image size for featured images, displayed on "standard" posts.
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop
		
	}

	static function enqueue_scripts(){
		global $wp_plugins;
		if( !is_admin() ) {
			$admin_bar_uri = get_stylesheet_directory_uri() . '/assets/css/admin-bar.css';
			if(is_admin_bar_showing() && file_exists($admin_bar_uri)){
				wp_enqueue_style('core-admin-bar', $admin_bar_uri);
			}
			$js_composer_url = plugins_url();
			$testURL = ABSPATH . 'wp-content/plugins/js_composer/assets/css/js_composer.css';
			if(file_exists($testURL)){
				wp_enqueue_style('js-composer', $js_composer_url . '/js_composer/assets/css/js_composer.css');
			}
		} else {
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_script( 'wp-color-picker' );

            // load icomoon in admin panel
            /*
            wp_register_style( 'icomoon_admin', get_template_directory_uri() . '/assets/css/icomoon.css', false, '1.0.0' );
            wp_enqueue_style( 'icomoon_admin' );
            */

            // vc editor and few other
            wp_register_style( 'vc_fastwp_admin', get_template_directory_uri() . '/assets/css/admin.css', false, '1.0.0' );
            wp_enqueue_style( 'vc_fastwp_admin' );

            // javascript functions
            wp_register_script( 'fastwp_admin_js', get_template_directory_uri() . '/assets/js/admin.js', false, '1.0.0' );
            wp_enqueue_script( 'fastwp_admin_js' );
	    }
	}

	static function check_required_plugins(){
		global $fastwp_pasadena_required_plugins;
		if(function_exists('tgmpa') && is_array($fastwp_pasadena_required_plugins)){
	    	tgmpa( $fastwp_pasadena_required_plugins );
		}
	}

	static function init_visual_composer_blocks(){
		global $fastwp_pasadena_visual_composer_blocks;

		/* Visual Composer configuration parser for FastWP Themes */
		if ((class_exists('WPBakeryVisualComposer') || class_exists('Vc_Manager')) && function_exists('vc_map')) {
			if(count($fastwp_pasadena_visual_composer_blocks) > 0 && is_array($fastwp_pasadena_visual_composer_blocks)){
				foreach($fastwp_pasadena_visual_composer_blocks as $vc_block){
					vc_map($vc_block);
				}
			}
		}
	}

	static function attach_footer_info_page(){
		global $fastwp_pasadena_data, $fastwp_pasadena_custom_shortcode_css;
		if(!isset( $fastwp_pasadena_data['before_footer_page_id']) || empty( $fastwp_pasadena_data['before_footer_page_id'])) 
			return;
		
		$page_id = $fastwp_pasadena_data['before_footer_page_id'];
		$page_object = get_page($page_id);
		if(!isset($page_object->ID)) return;
		$fastwp_pasadena_custom_shortcode_css .= get_post_meta($page_object->ID, '_wpb_shortcodes_custom_css', true );
			
		echo apply_filters('the_content', $page_object->post_content);
	}

	static function set_vc_as_theme() {
		vc_set_as_theme(true);
	}

	static function no_section_defined(){
		echo 'No sections defined !!!';
	}

	static function set_custom_edit_post_columns($columns) {
    	$columns['item_id'] = esc_html__( 'Item ID', 'pasadena' );
	   	return $columns;
	}

	static function set_custom_preview_post_columns($columns) {
    	$columns['preview_img'] = esc_html__( 'Preview', 'pasadena' );
	   	return $columns;
	}

	static function custom_ID_column( $column, $post_id ) {
	    switch ( $column ) {
	        case 'item_id':
	            echo  $post_id ;
	        break;
            case 'preview_img':
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'medium' );
                if( isset( $image[0] ) )
                echo '<img src="' . $image[0] . '" style="max-width: 128px; max-height: 128px;" />';
            break;
	    }
	}

	static function script_loader($scripts){
		if(FASTWP_PASADENA_AUTOLOAD_SCRIPTS == true) return;
		$scripts 	= explode(',', $scripts);
		foreach($scripts as $script){
			$handle = sanitize_title($script);
			wp_enqueue_script($handle);
		}
	}

	static function fastwp_navigation_update($menu_id, $menu_item_db_id, $args){
		if(isset($args['menu-item-menuicon_selected']))
			update_post_meta( $menu_item_db_id, '_menu_item_menuicon_selected', sanitize_key($args['menu-item-menuicon_selected']) );
		if(isset($args['menu-item-menuicon']))
			update_post_meta( $menu_item_db_id, '_menu_item_menuicon', sanitize_key($args['menu-item-menuicon']) );
		if(isset($args['menu-item-menutype']))
			update_post_meta( $menu_item_db_id, '_menu_item_menutype', sanitize_key($args['menu-item-menutype']) );
	}

    /**
    USER PROFILE - EXTRA FIELDS
    */

	static function fastwp_add_user_prof_fields( $user ) {

        $sn = get_the_author_meta( 'social_networks', $user->ID );

    	echo '<h3>' . esc_html__( 'Social Profiles', 'pasadena' ) . '</h3>
    	<table class="form-table form-uexr">';

        if( !empty( $sn ) ) {
            foreach( $sn['icon'] as $k => $ic ) {
                if( !empty( $ic ) ) {
                    echo '<tr>
            			<th><label for="facebook">' . esc_html__( 'Network', 'pasadena' ) . '</label></th>
            			<td>
            				<input type="text" name="social_networks[icon][]" id="social_networks[icon][]" value="' . esc_html( $ic ) . '" class="regular-text" placeholder="Icon" /><br />
            				<span class="description">' . esc_html__( 'Use Fontawesome icons', 'pasadena' ) . '</span> <br />
            				<input type="text" name="social_networks[url][]" id="social_networks[url][]" value="' . esc_url( $sn['url'][$k] ) . '" class="regular-text" placeholder="URL" /><br />
            				<span class="description">' . esc_html__( 'Your social network profile (URL)', 'pasadena' ) . '</span><br />
                            <a href="#" class="delete-uexr button">' . esc_html__( 'Delete', 'pasadena' ) .'</a>
            			</td>
            		</tr>';
                }
            }
        }

        echo '<tr>
            <th></th>
            <td><a href="#" class="add-uexr button">' . esc_html__( 'Add Network', 'pasadena' ) .'</a></td>
        </tr>';

        echo '<tr>
            <th><label for="facebook">' . esc_html__( 'Network', 'pasadena' ) . '</label></th>
            <td>
                <input type="text" name="social_networks[icon][]" id="social_networks[icon][]" value="" class="regular-text" placeholder="Icon" /><br />
                <span class="description">' . esc_html__( 'Use Fontawesome icons', 'pasadena' ) . '</span> <br />
                <input type="text" name="social_networks[url][]" id="social_networks[url][]" value="" class="regular-text" placeholder="URL" /><br />
                <span class="description">' . esc_html__( 'Your social network profile (URL)', 'pasadena' ) . '</span><br />
                <a href="#" class="delete-uexr button">' . esc_html__( 'Delete', 'pasadena' ) .'</a>
            </td>
        </tr>';

    	echo '</table>';

    }

    static function fastwp_save_user_prof_fields( $user_id ) {

    	if ( !current_user_can( 'edit_user', $user_id ) ) return false;

        foreach( array( 'social_networks' ) as $k ) {
            if( isset( $_POST[$k] ) ) {
        	    update_user_meta( $user_id, $k, $_POST[$k] );
            }
        }

    }

}