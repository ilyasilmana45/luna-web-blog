<?php

class FastWP_pasadena_UI {

	static function getMenu($echo = true){
        if( !has_nav_menu( 'primary' ) ) return '<li>' . esc_html__( 'No menu has been asigned.', 'pasadena' ) . '</li>';
	    $menu = wp_nav_menu(array(
	        'theme_location'=> 'primary', //menu id
			'container'		=> false,
            'container_id'  => false,
			'menu_class' 	=> '',
			'menu_id' 		=> false,
			'echo'			=> $echo,
			'fallback_cb'   => array('Walker_Menu_FastWP','do_default_page_menu'),
	        'walker'  		=> new Walker_Menu_FastWP()
	    ));
        return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );
	}

	static function SocialIconsHeader( $echo = true ) {
		global $fastwp_pasadena_data;
        $social_links = '';
        
        if( !empty( $fastwp_pasadena_data['menu_social_media'] ) && is_array( $fastwp_pasadena_data['menu_social_media'] ) ) {
            $social_links .= '<ul class="social-list clearfix">';
            foreach(  $fastwp_pasadena_data['menu_social_media'] as $sn) {
                $lu = array_map( 'trim', explode( '|', $sn ) );
                if( isset( $lu[0] ) && isset( $lu[1] ) ) {
                    $social_links .= '<li><a href="'. esc_html( $lu[1] ) .'"><i class="'. esc_attr( $lu[0] ) .'"></i></a></li>';
                }
            }
            $social_links .= '</ul>';
        }

        if( $echo == false ) return $social_links;
        else echo $social_links;
	}

	static function HeaderLogo( $echo = true ) {
	    global $fastwp_pasadena_data;
		$header_logo = '<div class="logo-wrapper">
					    	<h1 id="logo"><a href="'.esc_url( home_url( '/' ) ).'"' . ( !empty( $fastwp_pasadena_data['fastwp_logo']['url'] ) ? ' style="background-image:url(\'' . esc_url( $fastwp_pasadena_data['fastwp_logo']['url'] ) . '\');"' : '' ) . '></a></h1>
					    </div>';
		if( $echo == false ){
			return 	$header_logo;
		} else {
			echo 	$header_logo;
		}
	}

	static function MenuIconMarkup( $echo = true ) {
		if( $echo == false ) {
		    return '<div id="menu-button">
		        <div class="centralizer">
		          <div class="cursor">
		            <div id="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
		          </div>
		        </div>
		      </div>';
		} else {
            echo '<div id="menu-button">
		        <div class="centralizer">
		          <div class="cursor">
		            <div id="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
		          </div>
		        </div>
		      </div>';
        }
	}

	static function displayMenu( $echo = true ){
    	$menu_markup = '<nav id="main-nav">
    					  <ul id="options" class="clearfix">
    					  	%s
    					  </ul>
    					</nav>';

    	$c_page_tpl	= (is_page() || is_single())? basename(get_page_template()) : '';

    	$menu_markup 	= sprintf( $menu_markup, Fastwp_Pasadena_UI_getMenu( false ) );

    	if($echo == false) return $menu_markup;
        else echo 	$menu_markup;
	}

	static function getPreloaderMarkup(){
		global $fastwp_pasadena_data;

		return sprintf( '<div id="preloader">
                          <div id="status">
                            <div class="parent">
                              <div class="child">
                                <p>%s</p>
                              </div>
                            </div>
                          </div>
                        </div>', esc_html( $fastwp_pasadena_data['preloader_text'] ) );
	}

	static function showPreloader($forceVisible = false){
		global $fastwp_pasadena_data;
		$forceVisible = (
				$forceVisible == true ||
				(
					isset($fastwp_pasadena_data['show_preloader']) &&
					$fastwp_pasadena_data['show_preloader'] == 1 &&
					(
						is_front_page() ||
						!isset($fastwp_pasadena_data['disable_inner_preloader']) ||
						(isset($fastwp_pasadena_data['disable_inner_preloader']) && $fastwp_pasadena_data['disable_inner_preloader'] != 1)
					) && (
						Fastwp_Pasadena_UI_getPreloaderStatusForCurrentPage()
					)
				))? true : false;
		if($forceVisible == true){
			echo Fastwp_Pasadena_UI_getPreloaderMarkup();
			do_action('fastwp_pasadena_after_preloader');
		}
	}

	static function getPreloaderStatusForCurrentPage(){
		return true;
	}

	static function getNavMenuItems($showHome = '0'){
		/* Get data */
		$menu_locations 	= get_nav_menu_locations();
		$menu_query_args 	= array('order'=>'ASC', 'orderby'=>'menu_order', 'post_type'=>'nav_menu_item', 'post_status' => 'publish', 'output_key' => 'menu_order', 'nopaging' => true, 'update_post_term_cache'=>false );
		$primary_menu		= (isset($menu_locations) && isset($menu_locations['primary']))? $menu_locations['primary'] : '';
		$menu_items 		= wp_get_nav_menu_items($primary_menu, $menu_query_args );
		/* Add home button if needed */
		if($showHome === '1'){
			$menu_home_item 		= new stdClass;
			$menu_home_item->title 	= esc_html__('Home' ,'pasadena');
			$menu_home_item->url 	= esc_url( home_url('/') );
			$menu_home_item->type 	= null;
			$menu_home_item->object_id 	= null;
			$menu_home_item->menutype 	= null;
			$menu_items = array_merge(array($menu_home_item), $menu_items);
		}

		return $menu_items;
	}

	static function displayMultipage() {
		global $fastwp_pasadena_item_content, $fastwp_pasadena_custom_shortcode_css;

		do_action( 'fastwp_pasadena_before_generate_sections' );

		$menu = sprintf( '<header id="header" class="clearfix">%s</header>', Fastwp_Pasadena_UI_displayMenu( '', 'nav navbar-nav', false ) );
		$sections_and_pages = Fastwp_Pasadena_UI_getNavMenuItems(0);
		if( !is_array( $sections_and_pages ) ) {
			do_action( 'fastwp_pasadena_no_sections_defined' );
			return;
		}

		$sections_and_pages = apply_filters( 'fastwp_pasadena_filter_sections', $sections_and_pages );

		/* Remove from list individual pages */
		$sections = array();
		for( $i = 0; $i < count( $sections_and_pages ); $i++ ) {
			$type = get_post_meta( $sections_and_pages[$i]->ID, '_menu_item_menutype', true );
			if ( $type == 'section' ) {
				$sections[$sections_and_pages[$i]->object_id] = $sections_and_pages[$i];
			}
		}

		$sections_and_pages = $sections;
		$content = array();

        $args = array(
           'post_type' => 'page',
           'post__in'  => array_keys( $sections_and_pages )
        );

        $the_query = new WP_Query( $args );

        if ( $the_query->have_posts() ) {

        while( $the_query->have_posts() ) {
            $the_query->the_post();

            $id = get_the_ID();

	    	/* Get Visual Composer meta style just if composer is installed and enabled */
	    	if(defined( 'WPB_VC_VERSION' ) ){
				$fastwp_pasadena_custom_shortcode_css .= get_post_meta( $id, '_wpb_shortcodes_custom_css', true );
				$fastwp_pasadena_custom_shortcode_css .= get_post_meta( $id, '_wpb_post_custom_css', true );
			}

            apply_filters( 'the_content', get_the_content() );
		}

        wp_reset_postdata();

        }

		do_action('fastwp_pasadena_sections_generated');

        uasort( $fastwp_pasadena_item_content, function( $a, $b ) {
            if( (int) $a['position'] === (int) $b['position'] ) return 0;
            return ( (int) $a['position'] < (int) $b['position'] ? -1 : 1 );
        } );

        foreach(  $fastwp_pasadena_item_content as $item ) {
            echo $item['markup'];
        }
	}

	static function displayPageGrid() {
		global $fastwp_pasadena_item_content, $fastwp_pasadena_auto_add_home, $fastwp_pasadena_custom_shortcode_css;

		do_action( 'fastwp_pasadena_before_generate_sections' );

     	/* Get Visual Composer meta style just if composer is installed and enabled */
     	if( defined( 'WPB_VC_VERSION' ) ){
            $fastwp_pasadena_custom_shortcode_css .= get_post_meta( get_the_ID(), '_wpb_shortcodes_custom_css', true );
            $fastwp_pasadena_custom_shortcode_css .= get_post_meta( get_the_ID(), '_wpb_post_custom_css', true );
        }

        $fastwp_pasadena_auto_add_home = true;

        apply_filters( 'the_content', get_the_content() );

		do_action('fastwp_pasadena_sections_generated');

        uasort( $fastwp_pasadena_item_content, function( $a, $b ) {
            if( (int) $a['position'] === (int) $b['position'] ) return 0;
            return ( (int) $a['position'] < (int) $b['position'] ? -1 : 1 );
        } );

        foreach(  $fastwp_pasadena_item_content as $item ) {
            echo $item['markup'];
        }
	}

	static function add_custom_css(){

		global $fastwp_pasadena_custom_shortcode_css, $fastwp_pasadena_data;

		$fastwp_pasadena_custom_shortcode_css .= (isset($fastwp_pasadena_data['custom_css']))? $fastwp_pasadena_data['custom_css'] : '';

		if(isset($fastwp_pasadena_data['nav_bg_color']) && !empty($fastwp_pasadena_data['nav_bg_color'])){
			if(is_array($fastwp_pasadena_data['nav_bg_color'])){
				$fastwp_pasadena_data['nav_bg_color'] = FastWP::hex2rgba($fastwp_pasadena_data['nav_bg_color']['color'],$fastwp_pasadena_data['nav_bg_color']['alpha']);
			}
			$fastwp_pasadena_custom_shortcode_css .= sprintf('nav.navbar.affix { background-color:%s;}', $fastwp_pasadena_data['nav_bg_color']);

		}

        if( !empty( $fastwp_pasadena_data['body_font'] ) ) {
            $body_font = array();
            $body_font[] = !empty( $fastwp_pasadena_data['body_font']['font-family'] ) ? 'font-family: ' . $fastwp_pasadena_data['body_font']['font-family'] . '!important' : '';
            $body_font[] = !empty( $fastwp_pasadena_data['body_font']['font-weight'] ) ? 'font-weight: ' . $fastwp_pasadena_data['body_font']['font-weight'] . '!important' : '';
            $body_font[] = !empty( $fastwp_pasadena_data['body_font']['text-align'] ) ? 'text-align: ' . $fastwp_pasadena_data['body_font']['text-align'] . '!important' : '';
            $body_font[] = !empty( $fastwp_pasadena_data['body_font']['font-size'] ) ? 'font-size: ' . $fastwp_pasadena_data['body_font']['font-size'] . '!important' : '';
            $body_font[] = !empty( $fastwp_pasadena_data['body_font']['line-height'] ) ? 'line-height: ' . $fastwp_pasadena_data['body_font']['line-height'] . '!important' : '';
            $body_font[] = !empty( $fastwp_pasadena_data['body_font']['color'] ) ? 'color: ' . $fastwp_pasadena_data['body_font']['color'] . '!important' : '';
            if( !empty( $body_font ) ) {
                $fastwp_pasadena_custom_shortcode_css .= sprintf( 'body{%s}', implode( '; ', array_filter( $body_font ) ) );
            }
        }
        if( !empty( $fastwp_pasadena_data['h1_font'] ) ) {
            $h1_font = array();
            $h1_font[] = !empty( $fastwp_pasadena_data['h1_font']['font-family'] ) ? 'font-family: ' . $fastwp_pasadena_data['h1_font']['font-family'] . '!important' : '';
            $h1_font[] = !empty( $fastwp_pasadena_data['h1_font']['font-weight'] ) ? 'font-weight: ' . $fastwp_pasadena_data['h1_font']['font-weight'] . '!important' : '';
            $h1_font[] = !empty( $fastwp_pasadena_data['h1_font']['text-align'] ) ? 'text-align: ' . $fastwp_pasadena_data['h1_font']['text-align'] . '!important' : '';
            $h1_font[] = !empty( $fastwp_pasadena_data['h1_font']['font-size'] ) ? 'font-size: ' . $fastwp_pasadena_data['h1_font']['font-size'] . '!important' : '';
            $h1_font[] = !empty( $fastwp_pasadena_data['h1_font']['line-height'] ) ? 'line-height: ' . $fastwp_pasadena_data['h1_font']['line-height'] . '!important' : '';
            $h1_font[] = !empty( $fastwp_pasadena_data['h1_font']['color'] ) ? 'color: ' . $fastwp_pasadena_data['h1_font']['color'] . '!important' : '';
            if( !empty( $h1_font ) ) {
                $fastwp_pasadena_custom_shortcode_css .= sprintf( 'h1{%s}', implode( '; ', array_filter( $h1_font ) ) );
            }
        }
        if( !empty( $fastwp_pasadena_data['h2_font'] ) ) {
            $h2_font = array();
            $h2_font[] = !empty( $fastwp_pasadena_data['h2_font']['font-family'] ) ? 'font-family: ' . $fastwp_pasadena_data['h2_font']['font-family'] . '!important' : '';
            $h2_font[] = !empty( $fastwp_pasadena_data['h2_font']['font-weight'] ) ? 'font-weight: ' . $fastwp_pasadena_data['h2_font']['font-weight'] . '!important' : '';
            $h2_font[] = !empty( $fastwp_pasadena_data['h2_font']['text-align'] ) ? 'text-align: ' . $fastwp_pasadena_data['h2_font']['text-align'] . '!important' : '';
            $h2_font[] = !empty( $fastwp_pasadena_data['h2_font']['font-size'] ) ? 'font-size: ' . $fastwp_pasadena_data['h2_font']['font-size'] . '!important' : '';
            $h2_font[] = !empty( $fastwp_pasadena_data['h2_font']['line-height'] ) ? 'line-height: ' . $fastwp_pasadena_data['h2_font']['line-height'] . '!important' : '';
            $h2_font[] = !empty( $fastwp_pasadena_data['h2_font']['color'] ) ? 'color: ' . $fastwp_pasadena_data['h2_font']['color'] . '!important' : '';
            if( !empty( $h2_font ) ) {
                $fastwp_pasadena_custom_shortcode_css .= sprintf( 'h2{%s}', implode( '; ', array_filter( $h2_font ) ) );
            }
        }
        if( !empty( $fastwp_pasadena_data['h3_font'] ) ) {
            $h3_font = array();
            $h3_font[] = !empty( $fastwp_pasadena_data['h3_font']['font-family'] ) ? 'font-family: ' . $fastwp_pasadena_data['h3_font']['font-family'] . '!important' : '';
            $h3_font[] = !empty( $fastwp_pasadena_data['h3_font']['font-weight'] ) ? 'font-weight: ' . $fastwp_pasadena_data['h3_font']['font-weight'] . '!important' : '';
            $h3_font[] = !empty( $fastwp_pasadena_data['h3_font']['text-align'] ) ? 'text-align: ' . $fastwp_pasadena_data['h3_font']['text-align'] . '!important' : '';
            $h3_font[] = !empty( $fastwp_pasadena_data['h3_font']['font-size'] ) ? 'font-size: ' . $fastwp_pasadena_data['h3_font']['font-size'] . '!important' : '';
            $h3_font[] = !empty( $fastwp_pasadena_data['h3_font']['line-height'] ) ? 'line-height: ' . $fastwp_pasadena_data['h3_font']['line-height'] . '!important' : '';
            $h3_font[] = !empty( $fastwp_pasadena_data['h3_font']['color'] ) ? 'color: ' . $fastwp_pasadena_data['h3_font']['color'] . '!important' : '';
            if( !empty( $h3_font ) ) {
                $fastwp_pasadena_custom_shortcode_css .= sprintf( 'h3{%s}', implode( '; ', array_filter( $h3_font ) ) );
            }
        }
        if( !empty( $fastwp_pasadena_data['h4_font'] ) ) {
            $h4_font = array();
            $h4_font[] = !empty( $fastwp_pasadena_data['h4_font']['font-family'] ) ? 'font-family: ' . $fastwp_pasadena_data['h4_font']['font-family'] . '!important' : '';
            $h4_font[] = !empty( $fastwp_pasadena_data['h4_font']['font-weight'] ) ? 'font-weight: ' . $fastwp_pasadena_data['h4_font']['font-weight'] . '!important' : '';
            $h4_font[] = !empty( $fastwp_pasadena_data['h4_font']['text-align'] ) ? 'text-align: ' . $fastwp_pasadena_data['h4_font']['text-align'] . '!important' : '';
            $h4_font[] = !empty( $fastwp_pasadena_data['h4_font']['font-size'] ) ? 'font-size: ' . $fastwp_pasadena_data['h4_font']['font-size'] . '!important' : '';
            $h4_font[] = !empty( $fastwp_pasadena_data['h4_font']['line-height'] ) ? 'line-height: ' . $fastwp_pasadena_data['h4_font']['line-height'] . '!important' : '';
            $h4_font[] = !empty( $fastwp_pasadena_data['h4_font']['color'] ) ? 'color: ' . $fastwp_pasadena_data['h4_font']['color'] . '!important' : '';
            if( !empty( $h4_font ) ) {
                $fastwp_pasadena_custom_shortcode_css .= sprintf( 'h4{%s}', implode( '; ', array_filter( $h4_font ) ) );
            }
        }
        if( !empty( $fastwp_pasadena_data['h5_font'] ) ) {
            $h5_font = array();
            $h5_font[] = !empty( $fastwp_pasadena_data['h5_font']['font-family'] ) ? 'font-family: ' . $fastwp_pasadena_data['h5_font']['font-family'] . '!important' : '';
            $h5_font[] = !empty( $fastwp_pasadena_data['h5_font']['font-weight'] ) ? 'font-weight: ' . $fastwp_pasadena_data['h5_font']['font-weight'] . '!important' : '';
            $h5_font[] = !empty( $fastwp_pasadena_data['h5_font']['text-align'] ) ? 'text-align: ' . $fastwp_pasadena_data['h5_font']['text-align'] . '!important' : '';
            $h5_font[] = !empty( $fastwp_pasadena_data['h5_font']['font-size'] ) ? 'font-size: ' . $fastwp_pasadena_data['h5_font']['font-size'] . '!important' : '';
            $h5_font[] = !empty( $fastwp_pasadena_data['h5_font']['line-height'] ) ? 'line-height: ' . $fastwp_pasadena_data['h5_font']['line-height'] . '!important' : '';
            $h5_font[] = !empty( $fastwp_pasadena_data['h5_font']['color'] ) ? 'color: ' . $fastwp_pasadena_data['h5_font']['color'] . '!important' : '';
            if( !empty( $h5_font ) ) {
                $fastwp_pasadena_custom_shortcode_css .= sprintf( 'h5{%s}', implode( '; ', array_filter( $h5_font ) ) );
            }
        }
        if( !empty( $fastwp_pasadena_data['h6_font'] ) ) {
            $h6_font = array();
            $h6_font[] = !empty( $fastwp_pasadena_data['h6_font']['font-family'] ) ? 'font-family: ' . $fastwp_pasadena_data['h6_font']['font-family'] . '!important' : '';
            $h6_font[] = !empty( $fastwp_pasadena_data['h6_font']['font-weight'] ) ? 'font-weight: ' . $fastwp_pasadena_data['h6_font']['font-weight'] . '!important' : '';
            $h6_font[] = !empty( $fastwp_pasadena_data['h6_font']['text-align'] ) ? 'text-align: ' . $fastwp_pasadena_data['h6_font']['text-align'] . '!important' : '';
            $h6_font[] = !empty( $fastwp_pasadena_data['h6_font']['font-size'] ) ? 'font-size: ' . $fastwp_pasadena_data['h6_font']['font-size'] . '!important' : '';
            $h6_font[] = !empty( $fastwp_pasadena_data['h6_font']['line-height'] ) ? 'line-height: ' . $fastwp_pasadena_data['h6_font']['line-height'] . '!important' : '';
            $h6_font[] = !empty( $fastwp_pasadena_data['h6_font']['color'] ) ? 'color: ' . $fastwp_pasadena_data['h6_font']['color'] . '!important' : '';
            if( !empty( $h6_font ) ) {
                $fastwp_pasadena_custom_shortcode_css .= sprintf( 'h6{%s}', implode( '; ', array_filter( $h6_font ) ) );
            }
        }
        if( !empty( $fastwp_pasadena_data['menu_font'] ) ) {
            $nav_font = array();
            $nav_font[] = !empty( $fastwp_pasadena_data['menu_font']['font-family'] ) ? 'font-family: ' . $fastwp_pasadena_data['menu_font']['font-family'] . '!important' : '';
            $nav_font[] = !empty( $fastwp_pasadena_data['menu_font']['font-weight'] ) ? 'font-weight: ' . $fastwp_pasadena_data['menu_font']['font-weight'] . '!important' : '';
            $nav_font[] = !empty( $fastwp_pasadena_data['menu_font']['text-align'] ) ? 'text-align: ' . $fastwp_pasadena_data['menu_font']['text-align'] . '!important' : '';
            $nav_font[] = !empty( $fastwp_pasadena_data['menu_font']['font-size'] ) ? 'font-size: ' . $fastwp_pasadena_data['menu_font']['font-size'] . '!important' : '';
            $nav_font[] = !empty( $fastwp_pasadena_data['menu_font']['line-height'] ) ? 'line-height: ' . $fastwp_pasadena_data['menu_font']['line-height'] . '!important' : '';
            $nav_font[] = !empty( $fastwp_pasadena_data['menu_font']['color'] ) ? 'color: ' . $fastwp_pasadena_data['menu_font']['color'] . '!important' : '';
            if( !empty( $nav_font ) ) {
                $fastwp_pasadena_custom_shortcode_css .= sprintf( '#nav a{%s}', implode( '; ', array_filter( $nav_font ) ) );
            }
        }

		if ( ! empty( $fastwp_pasadena_custom_shortcode_css ) ) {
			echo '<style type="text/css" data-type="vc-shortcodes-custom-css">';
			echo $fastwp_pasadena_custom_shortcode_css;
			echo '</style>';
		}

		$fastwp_pasadena_custom_shortcode_css = '';

	}

	static function alert($type, $message)
    {
		return sprintf('<div class="alert alert-%s" role="alert">%s</div>', $type, $message);
	}
}

class Walker_Menu_FastWP extends Walker {
   var $db_fields = array(
        'parent' => 'menu_item_parent',
        'id'     => 'db_id'
    );

	function display_element ($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
        $element->hasChildren = isset( $children_elements[$element->ID] ) && !empty( $children_elements[$element->ID] );
        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= ' <ul class="sub-nav ' . FASTWP_PASADENA_MENU_CHILD_CLASS . ' level-' . ( $depth + 1 ) . '" role="menu"> ';
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= ' </ul>';
	}

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
    	$c_page_tpl		= ( is_page() || is_single() ) ? basename( get_page_template() ) : '';

    	if( isset( $item->post_status ) && isset( $item->ID ) && !isset( $item->object_id ) ) {
    		$item->object_id= $item->ID;
    		$item->title 	= $item->post_title;
    		$item->url 		= get_permalink( $item->ID );
    	}

		$type 				= get_post_meta( $item->ID, '_menu_item_menutype', true );
		$onepage_section 	= false;

		if( isset( $type ) && $type != '' && $type != 'page') {
			$onepage_section= true;
		}

		$class = $a_class = array();

        //if( $item->object_id === get_the_ID() ) {
		//    $class[] = 'active';
        //}

		if( isset( $item->hasChildren ) && $item->hasChildren ) {
			$a_class[] = 'sub-nav-toggle ';
		}

        if( !empty( $item->current ) ) {
            $a_class[] = 'selected';
        }

		$a_class[] = isset( $item->classes ) ? implode( ' ', $item->classes ) : '';

		$href 	 = ( $onepage_section == true && ( !isset( $item->object ) || $item->object != 'custom' ) ) ? sprintf( '#%s', FastWP::getMenuSectionId( $item->object_id ) ) : $item->url;
		if( $href != $item->url ) {
			if( $c_page_tpl != 'template-home-grid.php' && $c_page_tpl != 'template-page-grid.php' ) {
				$href 	= site_url() . $href;
			}
		}

        $output 		.= sprintf( "\n<li%s><a href='%s'%s%s>%s</a>",
                        			( !empty( $class ) ? ' class="' . implode( ' ', $class ) . '"' : '' ),
                                    $href,
                                    ( !empty( $a_class ) ? ' class="' . implode( ' ', $a_class ) . '"' : '' ),
                                    ( !empty( $item->target ) ? ' target="' . $item->target . '"' : '' ) .
                                    ( isset($item->hasChildren) && $item->hasChildren ? ' data-toggle="dropdown"' : '' ) .
                                    ( isset( $onepage_section ) && $onepage_section == true && ( !isset($item->hasChildren) || !$item->hasChildren ) ? ' data-scroll=""' : '' ),
                                    $item->title );
    }

	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= '</li>';
	}


	public static function do_default_page_menu($args = array()){
		$defaults 	= array('sort_column' => 'menu_order, post_title', 'menu_class' => 'menu', 'echo' => true, 'link_before' => '', 'link_after' => '');
		$args 		= wp_parse_args( $args, $defaults );
		$args 		= apply_filters( 'wp_page_menu_args', $args );
		$menu 		= '';
		$list_args 	= $args;

		if( ! empty($args['show_home']) ) {
			if( true === $args['show_home'] || '1' === $args['show_home'] || 1 === $args['show_home'] )
				$text = esc_html__('Home','pasadena');
			else
				$text = $args['show_home'];
			$class = '';
			if( is_front_page() && !is_paged() )
				$class = 'class="current_page_item"';
			$menu .= '<li ' . $class . '><a href="' . esc_url( home_url( '/' ) ) . '">' . $args['link_before'] . $text . $args['link_after'] . '</a></li>';
			// If the front page is a page, add it to the exclude list
			if( get_option('show_on_front') == 'page' ) {
				if ( !empty( $list_args['exclude'] ) ) {
					$list_args['exclude'] .= ',';
				} else {
					$list_args['exclude'] = '';
				}
				$list_args['exclude'] .= get_option( 'page_on_front' );
			}
		}

		$list_args['echo'] = false;
		$list_args['title_li'] = '';
		$menu .= str_replace( array( "\r", "\n", "\t" ), '', wp_list_pages( $list_args ) );

		if ( $menu )
			$menu = '<ul class="nav navbar-nav">' . $menu . '</ul>';

		$menu = '<div class="' . esc_attr($args['menu_class']) . '">' . $menu . "</div>\n";
		$menu = apply_filters( 'wp_page_menu', $menu, $args );
		if ( $args['echo'] )
			echo $menu;
		else
			return $menu;
	}
}

