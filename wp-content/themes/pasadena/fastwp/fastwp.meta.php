<?php

class fastwp_pasadena_meta {
	static function get( $id, $opt = '' ){
        $data = get_post_meta( $id );

        if( !empty( $data ) ) {

            $options = array();

            if( !empty( $opt ) ) {
                if( !isset( $data[$opt] ) ) {
                    return false;
                }
                $data = array( $data[$opt] );
            }
            foreach( $data as $option => $value ) {

                if( is_array( $value ) ) {
                    $options[$option] = $value[0];
                } else {
                    $options[$option] = $value;
                }

                if( preg_match( '/(^[a-z]{1}):/', $options[$option] ) ) {

                    if ( ( $serialized = @unserialize( $options[$option] ) ) ) {
                        $options[$option] =  $serialized;
                    }

                }

            }

            return $options;

        }

        return false;

    }
}