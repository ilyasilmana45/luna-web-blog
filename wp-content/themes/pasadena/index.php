<?php

do_action('fastwp_pasadena_before_header');
get_header();
do_action('fastwp_pasadena_before_page_content');

// load all the options
$fastwp_pasadena_options = fastwp_pasadena_get_option( array( 'fwp_hide_blog_title', 'fwp_blog_bg', 'fwp_blog_minititle', 'fwp_blog_title' ) );

echo '<div id="container" class="container clearfix">';

if( empty( $fastwp_pasadena_options['fwp_hide_blog_title'] ) ) {

    $title = $background = '';
    $fastwp_pasadena_title_atts['size'] = 'col2-3';

    if( !empty( $fastwp_pasadena_options['fwp_blog_bg']['url'] ) ) {
        $background = $fastwp_pasadena_options['fwp_blog_bg']['url'];
    }
    if( is_category() || is_tax() ) {
        $title = single_cat_title( ' / ', false );
    } else if( is_search() ) {
        $title = get_search_query();
    } else {
        $title = ( !empty(  $fastwp_pasadena_options['fwp_blog_title'] ) ? esc_html( $fastwp_pasadena_options['fwp_blog_title'] ) : get_bloginfo( 'name' ) );
    }

    echo '<div class="element clearfix col--2-2 with-bg home">
        <div class="borders">
            ' . ( !empty( $background ) ? '<img src="' . $background . '" />' : '' ) . '
            <div class="overlay"></div>
            <div class="parent">
                <div class="bottom">
                    <h2>' . $title . '</h2>
                </div>
            </div>
        </div>
    </div>';

}

fastwp_pasadena_posts( 'blog-list' );

?>

</div>

<?php

do_action('fastwp_pasadena_before_paging_nav');
echo fastwp_pasadena_blog_pagination();
do_action('fastwp_pasadena_after_paging_nav');

get_footer();