<?php

get_header();
the_post();

do_action('fastwp_pasadena_before_page_content');

?>

<section id="content">
    <div class="container clearfix">
        <div class="element clearfix col--5 page-content home auto">
            <h2><?php the_title(); ?></h2>
            <?php $temp_content = get_the_content();
            $temp_content = apply_filters( 'the_content', $temp_content );
            echo apply_filters( 'fastwp_pasadena_filter_section_content', $temp_content, $post );

            wp_link_pages();
            ?>
        </div>

        <?php if ( comments_open() ) { ?>

        <div class="element clearfix col--5 home hover-without-link" style="height: 468px;">
          <div class="greyed">
            <?php fastwp_pasadena_comment_form(); ?>
          </div>
        </div>

        <?php } ?>

        <?php comments_template(); ?>
    </div>
</section>

<?php

do_action('fastwp_pasadena_after_page_content');

get_footer();