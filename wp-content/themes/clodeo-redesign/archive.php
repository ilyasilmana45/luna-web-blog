<?php
//function for get category name
$category = get_category(get_query_var('cat'));

// function for getting tags by category id , this is cutom function in function.php
if (isset($category->term_id)) {
	$list_tag_slug = get_tags_in_use($category->cat_ID, 'all');
} else {
	$list_tag_slug = get_tags_in_use("", 'all');
}

// var_dump("<pre>");
// var_dump($list_tag_slug);
// var_dump("</pre>");
?>

<?php

/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package moto
 */

get_header();

?>

<div class="our-blog-area pt-80 pb-60">
	<div class="container">
		<div class="row">
			<div class="col-12 pb-5">
				<div class="row">
					<div class="col-12 col-md-8 justify-content-center" style="margin: auto">
						<div class="side-widget-search">
							<p class="side-widget-label">
								Search Articles
							</p>
							<?php get_template_part('searchform'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="row">
					<div class="col-12 col-md-12 justify-content-center" style="margin: auto">
						<?php if (have_posts()) : ?>
							<?php if (isset($category->name)) { ?>
								<p class="title-category-tag">
									Category
									<button class="category-tags-button" style="margin: 3px 3px; margin-left:10px">
										<?php echo $category->name ?>
									</button>
								</p>
							<?php } else { ?>
								<p class="title-category-tag">
									Tag
									<button class="category-tags-button" style="margin: 3px 3px; margin-left:10px">
										<?php echo single_tag_title() ?>
									</button>
								</p>
							<?php } ?>
							<div class="border-top border-bottom py-4">
								<?php foreach ($list_tag_slug as $tag) { ?>
									<a href="<?php echo home_url() ?>/tag/<?php echo $tag->slug ?>"><button class="category-tags-button" style="margin: 3px 3px;"><?php echo $tag->name; ?></button></a>
								<?php } ?>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div>
			<?php
			if (have_posts()) : ?>
				<div class="col-12 featured-articles pt-5">
					<div class="row">
						<?php
						while (have_posts()) : the_post();
							get_template_part('template-parts/blog', 'category');
						endwhile;
						?>
					</div>
				</div>
				<div class="col-12 text-center justify-content-center pt-5">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center justfy-content-center" style="margin:auto">
							<?php moto_blog_pagination(); ?>

						</div>
					</div>
				</div>
				<?php get_template_part('template-parts/blog', 'try-clodeo') ?>
			<?php
			else :
				get_template_part('template-parts/content', 'none');
			endif; ?>
		</div><!-- #main -->
	</div><!-- #primary -->
</div><!-- #primary -->

<?php

get_footer();
