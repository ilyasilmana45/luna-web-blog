<?php 
	$moto_opt = moto_get_opt();
	$column = '';
	if($moto_opt['moto_copyright_column']== '1'){
		$column = 12 .' text-center';
	}elseif($moto_opt['moto_copyright_column']== '3'){
		$column = 4;
	}else{
		$column = 6;
	}
?>

<div class="col-sm-<?php echo esc_attr( $column ); ?> col-xs-12">
	<div class="copyright-text">
		<p><img src="<?php  echo esc_url( $moto_opt['moto_head_logo']['url']); ?>" alt="logo">
			<?php 
			$moto_opt = moto_get_opt();
			if(isset( $moto_opt['moto_copyright'] ) && $moto_opt['moto_copyright']!=='' ){
				echo wp_kses( $moto_opt['moto_copyright'] , array(
					'a' => array(
						'href' => array(),
						'title' => array()
					),
					'br' => array(),
					'em' => array(),
					'strong' => array(),
					'img' => array(
						'src' => array(),
						'alt' => array()
					),
				));
			}else{  
			?> &copy; <?php echo date("Y").' ';  esc_html_e('PT. Clodeo Indonesia.  All Rights Reserved.', 'moto' ); 
			}
		?></p>
	</div>
</div>

