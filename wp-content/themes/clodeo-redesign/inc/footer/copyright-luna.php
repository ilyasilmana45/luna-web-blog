<div id="copyright-clodeo" class="col-12">
    <div class="row">
        <div class="col-1 text-center align-self-center">
            <img className="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/images/clodeo-logo.png" alt="Clodeo Aplikasi Manajemen Online Shop" />
        </div>
        <div class="col-8 text-left copyright-text align-self-center">
            <p class="copyright-p">©<?php echo date("Y") . ' '; ?> PT. Piri Aplikasi Indonesia. All rights reserved.</p>
        </div>
        <div class="col-3 text-right">
            <div class="row justify-content-end">
                <div class="col-2 text-center px-1">
                    <a href="#" rel="nofollow" target="_blank">
                        <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri() ?>/images/media-social/footer-fb.png" />
                    </a>
                </div>
                <div class="col-2 text-center px-1">
                    <a href="https://www.instagram.com/lunapos.id/" rel="nofollow" target="_blank">
                        <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri() ?>/images/media-social/footer-instagram.png" />
                    </a>
                </div>
                <div class="col-2 text-center px-1">
                    <a href="#" rel="nofollow" target="_blank">
                        <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri() ?>/images/media-social/footer-twitter.png" />
                    </a>
                </div>
                <div class="col-2 text-center px-1">
                    <a href="#" rel="nofollow" target="_blank">
                        <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri() ?>/images/media-social/footer-youtube.png" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>