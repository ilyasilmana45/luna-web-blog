<?php
	$moto_opt = moto_get_opt();

	$moto_banner_bg_color = $moto_banner_bg_repeat = $moto_banner_bg_size = $moto_banner_bg_attacement = $moto_banner_bg_position = $moto_banner_bg_image = "";	

	if ( isset( $moto_opt['moto_breadcrumbs_bg_optins'] ) ) {
		$moto_banner_bg_options = $moto_opt['moto_breadcrumbs_bg_optins'];	
	}

	if(isset($moto_banner_bg_options['background-color'])){
		$moto_banner_bg_color = $moto_banner_bg_options['background-color'];
	}

	if(isset($moto_banner_bg_options['background-repeat'])){
		$moto_banner_bg_repeat = $moto_banner_bg_options['background-repeat'];
	}

	if(isset($moto_banner_bg_options['background-size'])){
		$moto_banner_bg_size = $moto_banner_bg_options['background-size'];
	}

	if(isset($moto_banner_bg_options['background-attachment'])){
		$moto_banner_bg_attacement = $moto_banner_bg_options['background-attachment'];
	}

	if(isset($moto_banner_bg_options['background-position'])){
		$moto_banner_bg_position = $moto_banner_bg_options['background-position'];
	}
	if(isset($moto_banner_bg_options['background-image'])){
		$moto_banner_bg_image = $moto_banner_bg_options['background-image'];
	}

	$_motoid = get_the_ID();
	$moto_banner_img = get_post_meta($_motoid,'_moto_banner_img',true);
	$moto_banner_background = get_post_meta($_motoid,'_moto_banner_color',true);

?>


<?php

  //show bar and content
  if( '1' == isset($moto_opt['moto_page_title_bar']) ? $moto_opt['moto_page_title_bar'] : '' ){ ?>
	 <!-- breadcrumbs start -->
	<section class="breadcrumbs-area breadcrumbs-bg" style="background-image: url(<?php if( $moto_banner_img ){echo esc_url( $moto_banner_img ); }else{ echo esc_url( $moto_banner_bg_image ); } ?>) ; background-color:<?php if( $moto_banner_background ){ echo esc_attr( $moto_banner_background ); }else{ echo esc_attr( $moto_banner_bg_color ); } ?>;  background-repeat: <?php echo esc_attr( $moto_banner_bg_repeat ); ?>; background-size: <?php echo esc_attr( $moto_banner_bg_size ); ?>; background-attachment: <?php echo esc_attr( $moto_banner_bg_attacement ); ?>; background-position: <?php echo esc_attr( $moto_banner_bg_position ); ?>">
		<div class="<?php if( $moto_opt['moto_breadcrumbs_full'] == true ){ echo 'container-fluid'; }else{ echo 'container'; }?>">
			<div class="row">
				<div class="col-md-12">
				<?php 
					$breadcrumbs_title_position_cmb = get_post_meta($_motoid,'_moto_breadcrumbs_position',true);
					$breadcrumbs_title_position_redux = $moto_opt['moto_breadcrumbs_text'];



					if(!empty( $breadcrumbs_title_position_cmb )){
							$breadcrumbs_title_position = $breadcrumbs_title_position_cmb;
					}else{
						if( isset( $breadcrumbs_title_position_redux ) ){
						   $breadcrumbs_title_position = $breadcrumbs_title_position_redux;
						}			
					} 
				?>

					<div class="breadcrumbs breadcrumbs-title-<?php echo esc_attr( $breadcrumbs_title_position ); ?>">
						<?php 
						if( $moto_opt['moto_breadcrumbs_area']==true ): ?>
						<!---breadcrumbs title start-->
						<h2 class="page-title"><?php wp_title(''); ?></h2>
						<!---breadcrumbs title end -->
						<?php endif; ?>
							
						<div class="page-title-bar">
							<?php 
								if( $moto_opt['moto_breadcrumbs_content_blog'] == '2' ){

									if(function_exists('woocommerce_breadcrumb')){
										if(is_woocommerce()){
											woocommerce_breadcrumb();
										} else {
											moto_breadcrumbs();
										}
									} else {
										moto_breadcrumbs();
									}
									
								}elseif($moto_opt['moto_breadcrumbs_content_blog'] == '3'){
									?>
									<!---breadcrumbs serch start-->
									<div class="page-title-search-box">
										<form action="<?php echo esc_url(home_url( '/' )); ?>" method="GET">
											<input type="text" placeholder="search" name="s">
											<button type="submit"> <i class="fa fa-search"></i> </button>
										</form>
									</div>
									<!---breadcrumbs serch end-->		
									<?php
								} else{
									
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- breadcrumbs end -->
	<?php
	//show content only
 }elseif( '2' == isset($moto_opt['moto_page_title_bar']) ? $moto_opt['moto_page_title_bar'] : '' ) {


 }else { ?>
	<div class="breadcrumbs-area breadcrumbs-bg breadcrumbs-area-default">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="breadcrumbs breadcrumbs-title-left">
						<h2 class="page-title"><?php wp_title(''); ?></h2>
						<div class="page-title-bar">
							<?php 

								if(function_exists('woocommerce_breadcrumb')){
									if(is_woocommerce()){
										woocommerce_breadcrumb();
									} else {
										moto_breadcrumbs();
									}
								} else {
									moto_breadcrumbs();
								}

							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
<?php }
