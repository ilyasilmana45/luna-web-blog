<?php 
/*
 * Moto Header top bar menu
 * Author: codecarnival
 * Author URI: http://hastech.company
 * Version: 1.0.0
 * ======================================================
 */
   $moto_opt = moto_get_opt(); 
   if( isset($moto_opt['moto_header_show']) ? $moto_opt['moto_header_show'] : '' == true ):

   $topbar_widht = $moto_opt['moto_header_top_width'];
		if( isset( $topbar_widht ) && true == $topbar_widht ){
			$topbar_widht = 'container-fluid';
		}else {
			$topbar_widht = 'container';
	}

?>
	<div class="header-top-area">
		<div class="<?php echo esc_attr( $topbar_widht ); ?>">
			<div class="row">
				<div class="col-md-8 col-sm-8">
				<?php 						
					if( $moto_opt['moto_left_content_section'] == '1'){
						?>
						<div class="header-social">
							<ul>
							   <?php  
									foreach($moto_opt['moto_social_icons'] as $key=>$value ) { 
									 if($value!=''){
										if($key=='vimeo'){
										 echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>';
										} else {
										 echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-'.esc_attr($key).'"></i></a></li>';
										}
									   }
									} 
								?>
							</ul>
						</div>
						<?php
					}elseif( $moto_opt['moto_left_content_section'] == '2'){
						?>
						<div class="top-bar-left-menu">
							<?php 
								wp_nav_menu(array(
									'theme_location' => 'left-menu',
									'container'      => false,
								));
							?>
						</div>
						<?php
					}elseif( $moto_opt['moto_left_content_section'] == '4'){
						?>								
						<div class="top-bar-left-content">
							<?php
								// if( isset($moto_opt['moto_left_text_area']) && $moto_opt['moto_left_text_area']!='' ) {
								// 	echo wp_kses($moto_opt['moto_left_text_area'], array(
								// 		'a' => array(
								// 			'href' => array(),
								// 			'title' => array()
								// 		),
								// 		'br' => array(),
								// 		'em' => array(),
								// 		'strong' => array(),
								// 		'img' => array(
								// 			'src' => array(),
								// 			'alt' => array()
								// 		),
								// 	));
								// }else{
								// 	esc_html_e('Lorem ipsum dolor sit amet', 'moto');
								// }
							?>
							<ul>
								<li>
									<a>
										<p>CS: <a href="tel:+6281377332863"><strong>0813. 7733. 2863</strong></a></p>
										<p>Marketing: <a href="tel:+6281214340802"><strong>0812. 1434. 0802</strong></a></p>
									</a>
								</li>
								<li class="separator"></li>
								<li>
									<a href="mailto:support@clodeo.com">
										<strong>support@clodeo.com</strong>
									</a>
								</li>
								<li class="separator"></li>
								<li>
									<a href="https://wa.me/6281377332863" target="_blank">
										<div style="float: left; padding: 5px 0px;">
											<img src="<?php echo site_url(); ?>/wp-content/uploads/2019/01/whatsapp.png">
                                        </div>
										<div style="float: left; padding: 0px 10px;">
											<p>Whatsapp</p>
											<p>Us Now</p>
                                        </div>
									</a>
								</li>
							</ul>
						</div>								
						<?php
					}elseif( $moto_opt['moto_left_content_section'] == '5'){
					}else{
						?>
						<div class="header-info">
							<?php if( !empty( $moto_opt['moto_left_contact_info'] ) ): ?>
								<span><a href="tel:<?php echo esc_html( $moto_opt['moto_left_contact_info'] ); ?>">  <?php echo esc_html( $moto_opt['moto_left_contact_info'] ); ?></a></span>
							<?php endif; if( !empty($moto_opt['moto_left_contact_email'] ) ): ?>
								<span class="mail-us"><a href="mailto:<?php echo esc_html( $moto_opt['moto_left_contact_email'] ); ?>" target="_top">  <?php echo esc_html( $moto_opt['moto_left_contact_email'] ); ?></a></span>

							<?php endif; ?>

						</div>
					<?php
					}
				?>
				</div> 
				<div class="col-md-4 col-sm-4">
				<?php $moto_opt = moto_get_opt();
					if( $moto_opt['moto_right_contactinfo'] == '1'){
						?>
						<div class="header-social text-right">
							<ul>
							   <?php 
									foreach($moto_opt['moto_social_icons'] as $key=>$value ) { 
									 if($value!=''){
										if($key=='vimeo'){
										 echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>';
										} else {
										 echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-'.esc_attr($key).'"></i></a></li>';
										}
									   }
									} 
								?>
							</ul>
						</div>
						<?php
					}elseif( $moto_opt['moto_right_contactinfo'] == '2'){
						?>
						<div class="top-bar-left-menu text-right">
							<?php 
								wp_nav_menu(array(
									'theme_location' => 'right-menu',
								));
							?>
						</div>
						<?php
					}elseif( $moto_opt['moto_right_contactinfo'] == '4'){
						?>								
						<div class="top-bar-left-content text-right">
							<p>
								<?php if( isset($moto_opt['moto_right_text_area']) && $moto_opt['moto_right_text_area']!='' ) {
									echo wp_kses($moto_opt['moto_right_text_area'], array(
										'a' => array(
											'href' => array(),
											'title' => array()
										),
										'br' => array(),
										'em' => array(),
										'strong' => array(),
										'img' => array(
											'src' => array(),
											'alt' => array()
											),
										));
									}else{
										esc_html_e('Lorem ipsum dolor sit amet', 'moto');
									}
								?>
								</p>
						</div>								
						<?php
					}elseif( $moto_opt['moto_right_contactinfo'] == '5'){
					}else{
						?>
						<div class="header-info text-right">
							<?php if( !empty( $moto_opt['moto_right_contact_info'] ) ): ?>
								<span><a href="tel:<?php echo esc_html( $moto_opt['moto_right_contact_info'] ); ?>">  <?php echo esc_html( $moto_opt['moto_right_contact_info'] ); ?></a></span>
							<?php endif; if( !empty( $moto_opt['moto_right_contact_email'] ) ): ?>
								<span class="mail-us"><a href="mailto:<?php echo esc_html( $moto_opt['moto_right_contact_email'] ); ?>" target="_top">  <?php echo esc_html( $moto_opt['moto_right_contact_email'] ); ?></a></span>
							<?php endif; ?>

						</div>
					<?php
					}
				?>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>