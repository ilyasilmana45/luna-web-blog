<?php 
	/*
	 * Moto_ Main menu
	 * Author: codecarnival
	 * Author URI: http://hastech.company
	 * Version: 1.0.0
	 * ======================================================
	 */
	$moto_opt = moto_get_opt();

	$header_widht = $moto_opt['moto_header_full_width'];
	if( isset( $header_widht ) && true == $header_widht ){
		$header_widht = 'container-fluid';
	}else {
		$header_widht = 'container';
	}


	$moto_header_sticky_class = "";
	$moto_header_sticky = $moto_opt['moto_header_sticky'];
	if ( isset( $moto_header_sticky ) && true == $moto_header_sticky ) {
	$moto_header_sticky_class = "header-sticky";
	}

	$moto_header_transparent_class = "";
	$moto_header_transparent = $moto_opt['moto_header_transparent'];
	if ( isset( $moto_header_transparent ) && true == $moto_header_transparent ) {
	$moto_header_transparent_class = "header-transparent";
	}

?>
<div class="header-area header-style-2 <?php echo esc_attr( $moto_header_sticky_class ); ?> <?php echo esc_attr( $moto_header_transparent_class ); ?>">
	<div class="header-menu-wrap">
		<div class="header-mid hr-style-1">
			<div class="<?php echo esc_attr( $header_widht ); ?>">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="header-mid-inner logo-left-search-right">
							<div class="site-title">
								<?php 
								if( isset( $moto_opt['moto_head_logo']['url'])){
								?>

								<a href="<?php echo esc_url( home_url('/')); ?>" title="<?php echo esc_attr( get_bloginfo('name','moto')); ?>" rel="home" >
							<?php if ( $moto_opt['moto_main_logo']=='1'){ ?>
							<img src="<?php  echo esc_url( $moto_opt['moto_head_logo']['url']); ?>" alt="<?php  echo get_bloginfo('name'); ?>">
								<?php } else{
									 if( $moto_opt['moto_main_logo']=='2' ){echo esc_html( $moto_opt['moto_logo_text'] );} 
									}?>
							</a>

								<?php
								}else{ ?> 
								<h3><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php if($moto_opt['moto_logo_text']){ echo esc_html( $moto_opt['moto_logo_text'] );} else{ bloginfo( 'name' );
									} ?></a>
								</h3>
								<?php }
								?>
							</div>
							<div class="header-search hidden-xs">
								<form action="<?php echo esc_url(home_url( '/' )); ?>" method="GET">
									<input type="text" name="s" placeholder="Search" />
									<button><i class="fa fa-search"></i></button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="header-buttom">
			<div class="<?php echo esc_attr( $header_widht ); ?>">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="sticky_menu_logo">
							<div class="site-title hidden">
								<?php 
								if( isset( $moto_opt['moto_head_logo']['url'])){
								?>
								<a href="<?php echo esc_url( home_url('/')); ?>" title="<?php echo esc_attr( get_bloginfo('name','moto')); ?>" rel="home" >
							<?php if ( $moto_opt['moto_main_logo']=='1'){ ?>
							<img src="<?php  echo esc_url( $moto_opt['moto_head_logo']['url']); ?>" alt="<?php  echo get_bloginfo('name'); ?>">
								<?php } else{
									 if( $moto_opt['moto_main_logo']=='2' ){echo esc_html( $moto_opt['moto_logo_text'] );} 
									}?>
							</a>
								<?php
								}else{ ?> 
								<h3><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php if($moto_opt['moto_logo_text']){ echo esc_html( $moto_opt['moto_logo_text'] );} else{ bloginfo( 'name' );	} ?></a>
								</h3>
								<?php }
								?>
							</div>
							<div class="primary-nav-wrap nav-horizontal">
								<nav>
									<?php
										wp_nav_menu(array(
											'theme_location' => 'primary',
											'container'      => false,
											'fallback_cb'    => 'moto_fallback'
										));
									?>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Mobile Menu  -->
		<div class="mobile-menu"></div>
	</div>
</div>