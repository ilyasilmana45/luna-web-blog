<?php 

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
 
 require get_template_directory() . '/inc/widgets/company-info-widget.php';
 require get_template_directory() . '/inc/widgets/recent-post.php';
 require get_template_directory() . '/inc/widgets/newsletter.php';
 require get_template_directory() . '/inc/widgets/author-info.php';
 require get_template_directory() . '/inc/widgets/video-popup.php';
 require get_template_directory() . '/inc/widgets/single-banner.php';
 
 if(!function_exists('moto_widgets_init')){
	function moto_widgets_init() {
		register_sidebar( array(
			'name'          => esc_html__( 'Ritht Sidebar', 'moto' ),
			'id'            => 'sidebar-right',
			'description'   => esc_html__( 'Add widgets here.', 'moto' ),
			'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="sidebar-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => esc_html__( 'Left Sidebar', 'moto' ),
			'id'            => 'sidebar-left',
			'description'   => esc_html__( 'Add widgets here.', 'moto' ),
			'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="sidebar-title">',
			'after_title'   => '</h3>',
		) );

		$moto_opt = moto_get_opt();

		if(isset( $moto_opt['moto_footer_layoutcolumns'] ) ){

			$footer_widget_column = $moto_opt['moto_footer_layoutcolumns'];

		}else{
			$footer_widget_column = 4;
		}
		
		for( $footer = 1; $footer <= $footer_widget_column; $footer++){

			register_sidebar( array(
				'name'          => 'Footer ' .$footer,
				'id'            => 'moto-footer-' .$footer,
				'description'   => esc_html__( 'Add widgets here.', 'moto' ),
				'before_widget' => '<div id="%1$s" class="single-footer widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="footer-title">',
				'after_title'   => '</h4>',
			) );
		}
			 
	}		 
}
add_action( 'widgets_init', 'moto_widgets_init' );
 