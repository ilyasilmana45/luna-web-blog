<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package moto
 */

if ( ! is_active_sidebar( 'sidebar-shop' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area" role="complementary">
	<div class="col-md-3 col-sm-12 col-xs-12">
		<?php dynamic_sidebar( 'sidebar-shop' ); ?>
	</div>
</aside><!-- #secondary -->
