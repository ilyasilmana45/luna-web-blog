#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-10-20 03:57+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#. Name of the template
msgid "Both Sidebar Full"
msgstr ""

#: functions.php:55
msgid "Primary"
msgstr ""

#: functions.php:56
msgid "One Page Menu"
msgstr ""

#: functions.php:57
msgid "Top Bar Left menu"
msgstr ""

#: functions.php:58
msgid "Top Bar Right menu"
msgstr ""

#: functions.php:59
msgid "Footer Copyright Menu"
msgstr ""

#. If there are characters in your language that are not supported by Open Sans, translate this to 'off'. Do not translate into your own language.
#: functions.php:111
msgctxt "Open Sans font: on or off"
msgid "on"
msgstr ""

#. If there are characters in your language that are not supported by Raleway, translate this to 'off'. Do not translate into your own language.
#: functions.php:116
msgctxt "Raleway font: on or off"
msgid "on"
msgstr ""

#: functions.php:325
msgid "Create Menu"
msgstr ""

#. Name of the template
msgid "Right sidebar"
msgstr ""

#: search.php:18
#, php-format
msgid "Search Results for: %s"
msgstr ""

#: comments.php:38
#, php-format
msgctxt "comments title"
msgid "One comment on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s comments on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:64
msgid "Comments are closed."
msgstr ""

#: comments.php:93
msgid "Submit Comment"
msgstr ""

#: comments.php:97
msgid "Leave a Comment"
msgstr ""

#. Name of the template
msgid "Both Sidebar"
msgstr ""

#. Name of the template
msgid "Left sidebar"
msgstr ""

#. Name of the template
#: inc/metabox/metabox.php:25
msgid "Full Width"
msgstr ""

#: searchform.php:14
msgctxt "placeholder"
msgid "Search Here"
msgstr ""

#: template-parts/content-search.php:24
msgid "0"
msgstr ""

#: template-parts/content-search.php:24
msgid "1"
msgstr ""

#: template-parts/content-search.php:24
msgid "Comment off"
msgstr ""

#: template-parts/content-search.php:29 template-parts/blog-layout.php:29
#: template-parts/content-page.php:19 template-parts/content-single.php:100
msgid "Pages:"
msgstr ""

#: template-parts/content-search.php:31 template-parts/blog-layout.php:31
#: template-parts/content-single.php:104
msgid "Page"
msgstr ""

#: template-parts/content-none.php:14
msgid "Nothing Found"
msgstr ""

#: template-parts/content-none.php:21
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: template-parts/content-none.php:24
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: template-parts/content-none.php:30
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: template-parts/blog-layout.php:21
msgid "By : "
msgstr ""

#: template-parts/blog-layout.php:23 template-parts/content-single.php:112
msgid "No Comment"
msgstr ""

#: template-parts/blog-layout.php:23
msgid "01 Comment"
msgstr ""

#: template-parts/blog-layout.php:23 template-parts/content-single.php:112
msgid "% Comments"
msgstr ""

#: template-parts/blog-layout.php:23
msgid "Comments Off"
msgstr ""

#. %s: Name of current post
#. %s: Name of current post
#: template-parts/content-page.php:31 inc/template-tags.php:72
#, php-format
msgid "Edit %s"
msgstr ""

#: template-parts/content-single.php:111
msgid "By :"
msgstr ""

#: template-parts/content-single.php:112
msgid "1 Comment"
msgstr ""

#: template-parts/content-single.php:112
msgid "Comments off"
msgstr ""

#: template-parts/content-single.php:118
msgid "Share:"
msgstr ""

#: template-parts/content-single.php:136
msgid "prev post"
msgstr ""

#: template-parts/content-single.php:141
msgid "next post"
msgstr ""

#: inc/template-tags.php:28
#, php-format
msgctxt "post date"
msgid "Posted on %s"
msgstr ""

#: inc/template-tags.php:33
#, php-format
msgctxt "post author"
msgid "by %s"
msgstr ""

#. used between list items, there is a space after the comma
#. used between list items, there is a space after the comma
#: inc/template-tags.php:50 inc/template-tags.php:56
msgid ", "
msgstr ""

#: inc/template-tags.php:52
#, php-format
msgid "Posted in %1$s"
msgstr ""

#: inc/template-tags.php:58
#, php-format
msgid "Tagged %1$s"
msgstr ""

#. %s: post title
#: inc/template-tags.php:65
#, php-format
msgid "Leave a Comment<span class=\"screen-reader-text\"> on %s</span>"
msgstr ""

#: inc/comment-nav.php:6
msgid "Older Comments"
msgstr ""

#: inc/comment-nav.php:11
msgid "Newer Comments"
msgstr ""

#: inc/comment-form.php:31 inc/comment-form.php:102
msgid "Your comment is awaiting moderation."
msgstr ""

#: inc/comment-form.php:37 inc/comment-form.php:108
#, php-format
msgid "<cite class=\"author-name\">%s</cite>"
msgstr ""

#: inc/comment-form.php:42 inc/comment-form.php:113
#, php-format
msgid "<div class=\"comment-date-time\">%1$s at %2$s</div>"
msgstr ""

#: inc/comment-form.php:46 inc/comment-form.php:117
msgid "(Edit)"
msgstr ""

#: inc/global-functions.php:17
msgid "Blog"
msgstr ""

#: inc/global-functions.php:44
msgid "Read more"
msgstr ""

#: inc/global-functions.php:244 inc/footer/copyright.php:32
msgid "Copyright"
msgstr ""

#: inc/global-functions.php:244 inc/footer/copyright.php:32
msgid " All Rights Reserved."
msgstr ""

#: inc/header/header-top-bar.php:73 inc/header/header-top-bar.php:144
msgid "Lorem ipsum dolor sit amet"
msgstr ""

#: inc/metabox/metabox.php:11
msgid "Post Settings"
msgstr ""

#: inc/metabox/metabox.php:19
msgid "Select Page Layout"
msgstr ""

#: inc/metabox/metabox.php:24
msgid "Select Layout"
msgstr ""

#: inc/metabox/metabox.php:26 inc/widgets/widget-register.php:29
msgid "Left Sidebar"
msgstr ""

#: inc/metabox/metabox.php:27
msgid "Right Sidebar"
msgstr ""

#: inc/metabox/metabox.php:35
msgid "Page Settings"
msgstr ""

#: inc/metabox/metabox.php:43
msgid "Select Menu Style"
msgstr ""

#: inc/metabox/metabox.php:48
msgid "Default"
msgstr ""

#: inc/metabox/metabox.php:49
msgid "One page"
msgstr ""

#: inc/metabox/metabox.php:55
msgid "Enable Breadcrumbs"
msgstr ""

#: inc/metabox/metabox.php:60
msgid "Yes"
msgstr ""

#: inc/metabox/metabox.php:61
msgid "No"
msgstr ""

#: inc/metabox/metabox.php:67 inc/metabox/metabox.php:69
msgid "Breadcrumb Background Color"
msgstr ""

#: inc/metabox/metabox.php:75
msgid "Upload Breadcrumb Image"
msgstr ""

#: inc/metabox/metabox.php:77
msgid "upload image here"
msgstr ""

#: inc/metabox/metabox.php:85
msgid "BG Overlay Color"
msgstr ""

#: inc/metabox/metabox.php:87
msgid "Breadcrumbs Overlay Color"
msgstr ""

#: inc/metabox/metabox.php:93
msgid "Overlay Opacity"
msgstr ""

#: inc/metabox/metabox.php:95
msgid "Breadcrumbs Overlay Opacity ( ex: .0 - 1)"
msgstr ""

#: inc/metabox/metabox.php:100
msgid "Breadcrumbs Text Position"
msgstr ""

#: inc/metabox/metabox.php:102
msgid "Breadcrumbs text position"
msgstr ""

#: inc/metabox/metabox.php:105
msgid "Select position"
msgstr ""

#: inc/metabox/metabox.php:106
msgid "Position left"
msgstr ""

#: inc/metabox/metabox.php:107
msgid "Position center"
msgstr ""

#: inc/metabox/metabox.php:108
msgid "Position right"
msgstr ""

#: inc/metabox/metabox.php:114
msgid "Page Title Color"
msgstr ""

#: inc/metabox/metabox.php:116
msgid "Breadcrumbs title color"
msgstr ""

#: inc/metabox/metabox.php:121
msgid "Page Title Font Size"
msgstr ""

#: inc/metabox/metabox.php:123
msgid "Breadcrumbs title font size"
msgstr ""

#: inc/metabox/metabox.php:128
msgid "Breadcrumbs Top Padding"
msgstr ""

#: inc/metabox/metabox.php:130 inc/metabox/metabox.php:137
msgid "inset padding ex-80"
msgstr ""

#: inc/metabox/metabox.php:135
msgid "Breadcrumbs Bottom Padding"
msgstr ""

#: inc/metabox/metabox.php:142
msgid "Page Padding"
msgstr ""

#: inc/metabox/metabox.php:144
msgid "Page padding"
msgstr ""

#: inc/metabox/metabox.php:151
msgid "Nivo Slider Title One"
msgstr ""

#: inc/metabox/metabox.php:157
msgid "Title One"
msgstr ""

#: inc/metabox/metabox.php:159
msgid "Top title"
msgstr ""

#: inc/metabox/metabox.php:164 inc/metabox/metabox.php:247
#: inc/metabox/metabox.php:305
msgid "Font Size"
msgstr ""

#: inc/metabox/metabox.php:166
msgid "Top title font size"
msgstr ""

#: inc/metabox/metabox.php:172
msgid "Title One Text Color"
msgstr ""

#: inc/metabox/metabox.php:174
msgid "Top title color"
msgstr ""

#: inc/metabox/metabox.php:180
msgid "Title One Animation"
msgstr ""

#: inc/metabox/metabox.php:182
msgid "Select title one animation"
msgstr ""

#: inc/metabox/metabox.php:185 inc/metabox/metabox.php:260
#: inc/metabox/metabox.php:318 inc/metabox/metabox.php:393
msgid "bounceOut"
msgstr ""

#: inc/metabox/metabox.php:186 inc/metabox/metabox.php:261
#: inc/metabox/metabox.php:319 inc/metabox/metabox.php:394
msgid "bounceOutDown"
msgstr ""

#: inc/metabox/metabox.php:187 inc/metabox/metabox.php:262
#: inc/metabox/metabox.php:320 inc/metabox/metabox.php:395
msgid "bounceOutLeft"
msgstr ""

#: inc/metabox/metabox.php:188 inc/metabox/metabox.php:263
#: inc/metabox/metabox.php:321 inc/metabox/metabox.php:396
msgid "bounceOutRight"
msgstr ""

#: inc/metabox/metabox.php:189 inc/metabox/metabox.php:264
#: inc/metabox/metabox.php:322 inc/metabox/metabox.php:397
msgid "bounceOutUp"
msgstr ""

#: inc/metabox/metabox.php:190 inc/metabox/metabox.php:265
#: inc/metabox/metabox.php:323 inc/metabox/metabox.php:398
msgid "fadeIn"
msgstr ""

#: inc/metabox/metabox.php:191 inc/metabox/metabox.php:266
#: inc/metabox/metabox.php:324 inc/metabox/metabox.php:399
msgid "fadeInDown"
msgstr ""

#: inc/metabox/metabox.php:192 inc/metabox/metabox.php:267
#: inc/metabox/metabox.php:325 inc/metabox/metabox.php:400
msgid "fadeInDownBig"
msgstr ""

#: inc/metabox/metabox.php:193 inc/metabox/metabox.php:268
#: inc/metabox/metabox.php:326 inc/metabox/metabox.php:401
msgid "fadeInLeft"
msgstr ""

#: inc/metabox/metabox.php:194 inc/metabox/metabox.php:269
#: inc/metabox/metabox.php:327 inc/metabox/metabox.php:402
msgid "fadeInLeftBig"
msgstr ""

#: inc/metabox/metabox.php:195 inc/metabox/metabox.php:270
#: inc/metabox/metabox.php:328 inc/metabox/metabox.php:403
msgid "fadeInRight"
msgstr ""

#: inc/metabox/metabox.php:196 inc/metabox/metabox.php:271
#: inc/metabox/metabox.php:329 inc/metabox/metabox.php:404
msgid "fadeInRightBig"
msgstr ""

#: inc/metabox/metabox.php:197 inc/metabox/metabox.php:272
#: inc/metabox/metabox.php:330 inc/metabox/metabox.php:405
msgid "fadeInUp"
msgstr ""

#: inc/metabox/metabox.php:198 inc/metabox/metabox.php:273
#: inc/metabox/metabox.php:331 inc/metabox/metabox.php:406
msgid "fadeInUpBig"
msgstr ""

#: inc/metabox/metabox.php:199 inc/metabox/metabox.php:274
#: inc/metabox/metabox.php:332 inc/metabox/metabox.php:407
msgid "bounceIn"
msgstr ""

#: inc/metabox/metabox.php:200 inc/metabox/metabox.php:275
#: inc/metabox/metabox.php:333 inc/metabox/metabox.php:408
msgid "bounceInDown"
msgstr ""

#: inc/metabox/metabox.php:201 inc/metabox/metabox.php:276
#: inc/metabox/metabox.php:334 inc/metabox/metabox.php:409
msgid "bounceInLeft"
msgstr ""

#: inc/metabox/metabox.php:202 inc/metabox/metabox.php:277
#: inc/metabox/metabox.php:335 inc/metabox/metabox.php:410
msgid "bounceInRight"
msgstr ""

#: inc/metabox/metabox.php:203 inc/metabox/metabox.php:278
#: inc/metabox/metabox.php:336 inc/metabox/metabox.php:411
msgid "bounceInUp"
msgstr ""

#: inc/metabox/metabox.php:211
msgid "Nivo Slider Title Two"
msgstr ""

#: inc/metabox/metabox.php:216
msgid "Title Two"
msgstr ""

#: inc/metabox/metabox.php:218 inc/metabox/metabox.php:225
msgid "Bottom title"
msgstr ""

#: inc/metabox/metabox.php:223
msgid "Title Two Highlight Text"
msgstr ""

#: inc/metabox/metabox.php:230
msgid "Title Two Highlight Text Color"
msgstr ""

#: inc/metabox/metabox.php:232 inc/metabox/metabox.php:241
msgid "bottom title color"
msgstr ""

#: inc/metabox/metabox.php:239
msgid "Title Two Text Color"
msgstr ""

#: inc/metabox/metabox.php:249 inc/metabox/metabox.php:307
msgid "Bottom title font size"
msgstr ""

#: inc/metabox/metabox.php:255
msgid "Title Two Animation"
msgstr ""

#: inc/metabox/metabox.php:257
msgid "Select title two animation"
msgstr ""

#: inc/metabox/metabox.php:285 inc/metabox/metabox.php:290
#: inc/metabox/metabox.php:292
msgid "Description"
msgstr ""

#: inc/metabox/metabox.php:297
msgid "Description Text Color"
msgstr ""

#: inc/metabox/metabox.php:300
msgid "Top text color"
msgstr ""

#: inc/metabox/metabox.php:313
msgid "Animation"
msgstr ""

#: inc/metabox/metabox.php:315
msgid "Select text animation"
msgstr ""

#: inc/metabox/metabox.php:343
msgid "Slider Button"
msgstr ""

#: inc/metabox/metabox.php:348
msgid "Button Text"
msgstr ""

#: inc/metabox/metabox.php:350
msgid "Bottom text"
msgstr ""

#: inc/metabox/metabox.php:356
msgid "Button Link"
msgstr ""

#: inc/metabox/metabox.php:358
msgid "Bottom text link"
msgstr ""

#: inc/metabox/metabox.php:364
msgid "Button Text Color"
msgstr ""

#: inc/metabox/metabox.php:367
msgid "Button text color"
msgstr ""

#: inc/metabox/metabox.php:372
msgid "Button Background Color"
msgstr ""

#: inc/metabox/metabox.php:374
msgid "Button background color"
msgstr ""

#: inc/metabox/metabox.php:379
msgid "Button Background Hover Color"
msgstr ""

#: inc/metabox/metabox.php:381
msgid "Button background hover color"
msgstr ""

#: inc/metabox/metabox.php:388
msgid "Button Animation"
msgstr ""

#: inc/metabox/metabox.php:390
msgid "Select button animation"
msgstr ""

#: inc/metabox/metabox.php:419
msgid "Aditional Fields"
msgstr ""

#: inc/metabox/metabox.php:426
msgid "Enable Direction Navigation"
msgstr ""

#: inc/metabox/metabox.php:427
msgid "Prev & Next arrows"
msgstr ""

#: inc/metabox/metabox.php:432
msgid "Enable Control Navigation"
msgstr ""

#: inc/metabox/metabox.php:433
msgid "eg 1,2,3..."
msgstr ""

#: inc/metabox/metabox.php:439
msgid "Width Control"
msgstr ""

#: inc/metabox/metabox.php:445
msgid "Height Control"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:62
msgid "WPHash Helper Plugin"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:70
msgid "CMB2"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:76
msgid "Redux Framework"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:81
msgid "Contact Form 7"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:86
msgid "King Composer"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:91
msgid "Mailchimp For WP"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:96
msgid "One Click Demo"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:125
msgid "Install Required Plugins"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:126
msgid "Install Plugins"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:127
#, php-format
msgid "Installing Plugin: %s"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:128
msgid "Something went wrong with the plugin API."
msgstr ""

#: inc/tgm-plugin/required-plugins.php:129
#, php-format
msgid "This theme requires the following plugin: %1$s."
msgid_plural "This theme requires the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm-plugin/required-plugins.php:134
#, php-format
msgid "This theme recommends the following plugin: %1$s."
msgid_plural "This theme recommends the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm-plugin/required-plugins.php:139
#, php-format
msgid ""
"Sorry, but you do not have the correct permissions to install the %1$s "
"plugin."
msgid_plural ""
"Sorry, but you do not have the correct permissions to install the %1$s "
"plugins."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm-plugin/required-plugins.php:144
#, php-format
msgid ""
"The following plugin needs to be updated to its latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgid_plural ""
"The following plugins need to be updated to their latest version to ensure "
"maximum compatibility with this theme: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm-plugin/required-plugins.php:149
#, php-format
msgid "There is an update available for: %1$s."
msgid_plural "There are updates available for the following plugins: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm-plugin/required-plugins.php:154
#, php-format
msgid ""
"Sorry, but you do not have the correct permissions to update the %1$s plugin."
msgid_plural ""
"Sorry, but you do not have the correct permissions to update the %1$s "
"plugins."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm-plugin/required-plugins.php:159
#, php-format
msgid "The following required plugin is currently inactive: %1$s."
msgid_plural "The following required plugins are currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm-plugin/required-plugins.php:164
#, php-format
msgid "The following recommended plugin is currently inactive: %1$s."
msgid_plural "The following recommended plugins are currently inactive: %1$s."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm-plugin/required-plugins.php:169
#, php-format
msgid ""
"Sorry, but you do not have the correct permissions to activate the %1$s "
"plugin."
msgid_plural ""
"Sorry, but you do not have the correct permissions to activate the %1$s "
"plugins."
msgstr[0] ""
msgstr[1] ""

#: inc/tgm-plugin/required-plugins.php:174
msgid "Begin installing plugin"
msgid_plural "Begin installing plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/tgm-plugin/required-plugins.php:179
msgid "Begin updating plugin"
msgid_plural "Begin updating plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/tgm-plugin/required-plugins.php:184
msgid "Begin activating plugin"
msgid_plural "Begin activating plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/tgm-plugin/required-plugins.php:189
msgid "Return to Required Plugins Installer"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:190
msgid "Plugin activated successfully."
msgstr ""

#: inc/tgm-plugin/required-plugins.php:191
msgid "The following plugin was activated successfully:"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:192
#, php-format
msgid "No action taken. Plugin %1$s was already active."
msgstr ""

#: inc/tgm-plugin/required-plugins.php:193
#, php-format
msgid ""
"Plugin not activated. A higher version of %s is needed for this theme. "
"Please update the plugin."
msgstr ""

#: inc/tgm-plugin/required-plugins.php:194
#, php-format
msgid "All plugins installed and activated successfully. %1$s"
msgstr ""

#: inc/tgm-plugin/required-plugins.php:195
msgid "Please contact the administrator of this site for help."
msgstr ""

#: inc/bredcrumb/breadcrumb.php:28
msgid "Home"
msgstr ""

#: inc/bredcrumb/breadcrumb.php:29
#, php-format
msgid "Archive by Category \"%s\""
msgstr ""

#: inc/bredcrumb/breadcrumb.php:30
#, php-format
msgid "Archive for \"%s\""
msgstr ""

#: inc/bredcrumb/breadcrumb.php:31
#, php-format
msgid "Search Results for \"%s\" Query"
msgstr ""

#: inc/bredcrumb/breadcrumb.php:32
#, php-format
msgid "Posts Tagged \"%s\""
msgstr ""

#: inc/bredcrumb/breadcrumb.php:33
#, php-format
msgid "Articles Posted by %s"
msgstr ""

#: inc/bredcrumb/breadcrumb.php:34
msgid "Error 404"
msgstr ""

#: inc/widgets/single-banner.php:15
msgid "This widget show banner"
msgstr ""

#: inc/widgets/single-banner.php:19
msgid "moto: Banner"
msgstr ""

#: inc/widgets/single-banner.php:45
msgid "Banner Image"
msgstr ""

#: inc/widgets/single-banner.php:100 inc/widgets/video-popup.php:120
#: inc/widgets/author-info.php:183 inc/widgets/newsletter.php:72
#: inc/widgets/recent-post.php:121
msgid "Title:"
msgstr ""

#: inc/widgets/single-banner.php:105 inc/widgets/video-popup.php:125
#: inc/widgets/author-info.php:174
msgid "Upload Image"
msgstr ""

#: inc/widgets/single-banner.php:113
msgid "Banner Link:"
msgstr ""

#: inc/widgets/widget-register.php:19
msgid "Ritht Sidebar"
msgstr ""

#: inc/widgets/widget-register.php:21 inc/widgets/widget-register.php:31
#: inc/widgets/widget-register.php:53
msgid "Add widgets here."
msgstr ""

#: inc/widgets/video-popup.php:15
msgid "This widget show video thumbnail and video"
msgstr ""

#: inc/widgets/video-popup.php:19
msgid "moto: Video PopUp"
msgstr ""

#: inc/widgets/video-popup.php:50
msgid "video popup bg image"
msgstr ""

#: inc/widgets/video-popup.php:133
msgid "Video Link:"
msgstr ""

#: inc/widgets/video-popup.php:137 inc/widgets/company-info-widget.php:130
#: inc/widgets/author-info.php:187
msgid "Content:"
msgstr ""

#: inc/widgets/company-info-widget.php:11
msgid "Our short description  ."
msgstr ""

#: inc/widgets/company-info-widget.php:12
msgid "moto: Short Description"
msgstr ""

#: inc/widgets/company-info-widget.php:123
msgid "Upload Image:"
msgstr ""

#: inc/widgets/company-info-widget.php:127
msgid "Upload"
msgstr ""

#: inc/widgets/company-info-widget.php:134 inc/widgets/author-info.php:197
msgid "Facebook Link:"
msgstr ""

#: inc/widgets/company-info-widget.php:138 inc/widgets/author-info.php:201
msgid "Google Plus Link:"
msgstr ""

#: inc/widgets/company-info-widget.php:142 inc/widgets/author-info.php:205
msgid "Twitter Link:"
msgstr ""

#: inc/widgets/company-info-widget.php:146
msgid "Rss Link:"
msgstr ""

#: inc/widgets/company-info-widget.php:152 inc/widgets/author-info.php:213
msgid "Linkedin Link:"
msgstr ""

#: inc/widgets/author-info.php:15
msgid "This widget show author informations"
msgstr ""

#: inc/widgets/author-info.php:19
msgid "moto: Author Informations"
msgstr ""

#: inc/widgets/author-info.php:51
msgid "Author Image"
msgstr ""

#: inc/widgets/author-info.php:192
msgid "Social Title:"
msgstr ""

#: inc/widgets/author-info.php:209
msgid "Youtube Link:"
msgstr ""

#: inc/widgets/author-info.php:217
msgid "Pinterest Link:"
msgstr ""

#: inc/widgets/author-info.php:221
msgid "Instagram Link:"
msgstr ""

#: inc/widgets/newsletter.php:11
msgid "moto contact for address email and phone no."
msgstr ""

#: inc/widgets/newsletter.php:13
msgid "moto: Newsletter"
msgstr ""

#: inc/widgets/newsletter.php:76
msgid "Description:"
msgstr ""

#: inc/widgets/newsletter.php:80
msgid "Shortcode:"
msgstr ""

#: inc/widgets/recent-post.php:14
msgid "moto blog recent post here"
msgstr ""

#: inc/widgets/recent-post.php:18
msgid "moto: Recent Post"
msgstr ""

#: inc/widgets/recent-post.php:37
msgid "Recent Posts"
msgstr ""

#: inc/widgets/recent-post.php:126
msgid "No. of Item of posts to show:"
msgstr ""

#: inc/widgets/recent-post.php:131
msgid "Title Word"
msgstr ""

#: inc/widgets/recent-post.php:137
msgid "Display post date?"
msgstr ""

#. Name of the theme
msgid "Moto"
msgstr ""

#. Description of the theme
msgid "WordPress Landing Page Theme"
msgstr ""

#. URI of the theme
msgid "http://demo.wphash.com/moto/"
msgstr ""

#. Author of the theme
msgid "codecarnival"
msgstr ""

#. Author URI of the theme
msgid "http://hastech.company/"
msgstr ""
