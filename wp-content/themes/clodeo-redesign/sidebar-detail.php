<div class="row side-bar">
    <div class="col-12 blog-title">
        <?php echo the_title(); ?>
    </div>
    <div class="col-12 blog-date-comment">
        <div class="row">
            <div class="col-6 col-sm-6 col-md-12 col-lg-6 date-time">
                <span class="side-date" style="position:relative">
                    <?php echo the_date(); ?>
                </span>
            </div>
            <div class="col-6 col-sm-6 col-md-12 col-lg-6 date-time">
                <a href="#comment-section">
                    <span class="comment" style="position:relative">
                        <?php echo get_comments_number(); ?> Comments
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="col-12 blog-author">
        <div class="row">
            <div class="col-12 author-image text-left">
                <?php echo get_avatar(get_the_author_meta('ID')) ?>
                <span class="author-name">
                    <?php echo get_the_author_meta('display_name') ?>
                </span>
            </div>
        </div>
    </div>
    <div class="col-12 blog-side-search-articles">
        <div class="side-widget-search">
            <p class="side-widget-label">
                Search Articles
            </p>
            <?php get_template_part('searchform'); ?>
        </div>
    </div>
    <div class="col-12 pt-5">
        <div class="side-widget-hyperlink ">
            <p class="side-widget-hyperlink-label">Yuk kelola bisnis onlinemu dengan Clodeo sekarang!</p>
            <p class="side-widget-hyperlink-desc">
                Gunakan Clodeo agar kegiatan bisnis menjadi lebih mudah, cepat, bisa terpantau dimanapun dan kapanpun. Daftar dan coba GRATIS 30 hari!
            </p>
            <a href="https://app.clodeo.com/" target="_blank" class="side-widget-hyperlink-link">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/arrow-down.png" class="pr-3" style="margin-bottom: 2px;" />Coba Clodeo Gratis
            </a>
        </div>
    </div>
</div>
<?php if (get_the_tags()) : ?>
    <div class="col-12 side-links pb-3 pt-5">
        <p>Tags :</p>
        <?php foreach (get_the_tags() as $tags) { ?>
            <a href="<?php echo home_url() ?>/tag/<?php echo $tags->slug ?>"><button class="category-tags-button" style="margin: 3px 3px;"><?php echo $tags->name; ?></button></a>
        <?php } ?>
    </div>
<?php endif; ?>
</div><!-- /.blog-sidebar -->