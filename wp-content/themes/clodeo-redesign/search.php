<?php



/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package moto
 */
get_header();
?>
<div class="our-blog-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12 featured-articles">
				<?php
				if (have_posts()) : ?>
					<div class="col-12 pb-5">
						<div class="row">
							<div class="col-12 col-md-8 justify-content-center" style="margin: auto">
								<div class="side-widget-search">
									<p class="side-widget-label">
										Search Articles
									</p>
									<?php get_template_part('searchform'); ?>
								</div>
							</div>
						</div>
					</div>
					<header class="page-header pt-4">
						<h1 class="page-title" style="font-size: 16px; margin-left: 10px">
							<?php
							printf(esc_html__('Search Results for: %s', 'moto'), '<span>' . get_search_query() . '</span>');
							?>
						</h1>
					</header>
					<div id="card-main-blog">
						<div class="card-list">
							<div class="card-wrapper row">
								<?php
								/* Start the Loop */
								while (have_posts()) : the_post();
									/**
									 * Run the loop for the search to output the results.
									 * If you want to overload this in a child theme then include a file
									 * called content-search.php and that will be used instead.
									 */

									//template parts for search page
									get_template_part('template-parts/content', 'search');

								endwhile;
								?>
								<div class="col-12 pt-5 text-center justify-content-center pb-5">
									<?php moto_blog_pagination(); ?>
								</div>
								<?php get_template_part('template-parts/blog', 'try-clodeo') ?>
							<?php
						else :

							get_template_part('template-parts/content', 'none');

						endif; ?>
							</div>
						</div>
					</div>
			</div>
			<!-- <div class="col-md-4"> -->
			<?php
			// get_sidebar('main'); 
			?>
			<!-- </div> -->

		</div><!-- #row -->
	</div><!-- #container -->
</div><!-- #primary -->

<?php
get_footer();
