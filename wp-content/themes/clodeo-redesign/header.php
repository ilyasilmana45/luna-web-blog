<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="border-nav">
        <div class="container ">
            <div class="row ">
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg navbar-light sticky-top">
                        <a class="navbar-brand" href="<?php echo home_url() ?>">
                            <img id="navbar-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/clodeo-logo.png" alt="">
                        </a>
                        <button class="navbar-toggler btn btn-outline-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                            </ul>
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link active" href="<?php echo home_url() ?>">Blog</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="https://help.lunapos.id/" target="_blank">Tutorial</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="https://lunapos.id/kontak" target="_blank">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class=""></div>
            </div>
        </div>
    </div>