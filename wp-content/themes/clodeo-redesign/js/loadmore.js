jQuery(function($){ // use jQuery code inside this to avoid "$ is not defined" error
    var ppp = 2; // Post per page
    var pageNumber = 1;

    function load_posts(){
        pageNumber++;
        var postCount = ppp * pageNumber;
        var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=load_more_posts_ajax';
        
        $.ajax({
            type: "POST",
            dataType: "html",
            url: ajax_posts.ajaxurl,
            data: str,
            success: function(data){
                var $data = $(data);

                if($data.length){
                    $(".card-list").append($data);
                    $(".load-more-button").attr("disabled",false); // Disable the button, temp.

                    if ( postCount >= ajax_posts.post_count ) {
                        $(".load-more-button").hide();
                    }
                } else{
                    $(".load-more-button").hide();
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }

        });
        return false;
    }

    $(".load-more-button").on("click",function(){ // When btn is pressed.
        $(".load-more-button").attr("disabled",true); // Disable the button, temp.
        load_posts();
    });
});