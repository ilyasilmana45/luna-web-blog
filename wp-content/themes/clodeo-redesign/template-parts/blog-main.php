<div id="content-block" class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                <!-- Banner Component Main Blog -->
                <?php get_template_part('template-parts/blog', 'banner'); ?>

                <!-- EOF Banner Component Main Blog -->
            </div>

            <div class="col-12 featured-articles">
                <!-- Card Component Main Blog -->
                <?php get_template_part('template-parts/blog', 'card'); ?>
                <!-- EOF Card Component Main Blog -->
            </div>
        </div>
    </div>
    <?php get_template_part('template-parts/blog', 'try-clodeo') ?>
</div>