<div class="col-12 pt-5">
    <div class="row try-clodeo">
        <div class="col-12 col-md-6 text-center content-hyperlink-clodeo try-clodeo-content pl-2 pl-md-5">
            <div class="title text-center text-md-left py-4 py-md-2">
                Kini lebih tenang ada Clodeo
            </div>
            <div class="content text-center text-md-left">
                Bisnis online maupun offline bisa dikelola dengan mudah dalam satu aplikasi. Tunggu apa lagi?
            </div>
            <div class="footer text-center text-md-left">
                <a href="https://app.clodeo.com/" target="_blank" class="side-widget-hyperlink-link">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/arrow-down.png" class="pr-2" style="margin-bottom: 2px;" /> Mulai kelola bisnismu sekarang
                </a>
            </div>
        </div>
        <div class="col-12 col-md-6 pr-0 pl-0  text-right pb-0">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/try-clodeo.png" class="try-clodeo-image img-fluid" />
        </div>
    </div>
</div>