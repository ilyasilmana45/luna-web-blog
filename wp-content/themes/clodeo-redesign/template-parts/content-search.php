<?php

/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package moto
 */
$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), 'moto_blog_img', true);
?>
<div id="post-<?php the_ID(); ?>" class="col-md-4 py-4 py-md-3">
	<a class="no-underline" href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark">
		<div class="row blog-card-row">
			<?php if (has_post_thumbnail()) : ?>
				<div class="col-12 pl-0 pr-0">
					<img src="<?php echo esc_url($image[0]); ?>" alt="<?php echo esc_attr('Blog image'); ?>" />
				</div>
			<?php endif; ?>
			<?php if (get_the_tags()) : ?>
				<?php $index = 0; ?>
				<div class="col-12 px-4 pt-4">
					<div class="row ">
						<?php foreach (get_the_tags() as $tags) { ?>
							<?php if($index == 2){ $index=0; break; } else {$index++;} ?>
							<div class="col-6">
								<p class="breadcrumbs"><?php echo $tags->name; ?></p>
							</div>
						<?php } ?>
					</div>
				</div>
			<?php
			endif;
			?>

			<div class="col-12 px-4 py-3">
				<?php the_title(sprintf('<p class="card-title">', esc_url(get_permalink())), '</p>'); ?>
			</div>
			<div class="col-12 card-date-time px-4">
				<p class="card-date-time">
					<?php echo get_the_date(); ?>
					<span class="card-author">
						<?php
						echo get_the_author_meta('display_name');
						?>
					</span>
				</p>
			</div>
		</div>
	</a>
</div>