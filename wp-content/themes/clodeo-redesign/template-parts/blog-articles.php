<?php
$post = get_post();

$title = isset($post->post_title) ? $post->post_title : '';
$id    = isset($post->ID) ? $post->ID : 0;
$author_id = isset($post->post_author) ? $post->post_author : '';
$query = new WP_Query(array(
    'posts_per_page' => 6,
    'post_type'      => 'post',
    'post__not_in'  => array($id),
));
$feature_articles = $query->posts;
$tagsIndex = 0;

?>

<div class="row blog-main">
    <div class="col-12 featured-articles">
        <p class="title">Featured Articles</p>
        <div class="row">
            <?php
                foreach ($feature_articles as $post) {
                $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), 'moto_blog_img', true);        
            ?>
                <div id="post-<?php the_ID(); ?>" class="col-md-4 py-4 py-md-4">
                    <a class="no-underline" href="<?php esc_url(the_permalink()) ?>" rel="bookmark">
                        <div class="row blog-card-row">
                            <div class="col-12 pl-0 pr-0">
                                <?php if (has_post_thumbnail()) { ?>
                                    <img src="<?php echo esc_url($image[0]); ?>" alt="<?php echo esc_attr('Blog image'); ?>" class="img-fluid" />
                                <?php } else { ?>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured-articles/card-1.png" class="img-fluid" style="width: 100%" />
                                <?php } ?>
                            </div>
                            <div class="col-12 pt-3">
                                <?php if (get_the_tags()) :  ?>
                                    <div class="row ">
                                        <?php foreach (get_the_tags() as $tags) { ?>
                                            <?php if ($tagsIndex == 2) {
                                                $tagsIndex = 0;
                                                break;
                                            } else {
                                                $tagsIndex++;
                                            } ?>
                                            <div class="col-6">
                                                <p class="breadcrumbs"><?php echo $tags->name ?></p>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php endif ?>
                            </div>

                            <div class="col-12">
                                <?php the_title(sprintf('<p class="card-title">', esc_url(get_permalink())), '</p>'); ?>
                            </div>
                            <div class="col-12 card-date-time">
                                <p class="card-date-time">
                                    <?php echo get_the_date(); ?>
                                    <span class="card-author">
                                        <?php
                                        echo get_the_author_meta('display_name', $author_id);
                                        ?>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>