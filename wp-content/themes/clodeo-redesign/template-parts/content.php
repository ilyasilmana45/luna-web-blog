<?php

/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package moto
 */

$moto_opt = moto_get_opt();
$moto_blog_layout_opts = '';

if (isset($moto_opt['moto_sidebarblog_pos'])) {
	$moto_blog_layout_opts = $moto_opt['moto_sidebarblog_pos'];
}

if ($moto_blog_layout_opts == 'single') {
	if (have_posts()) :
		/* Start the Loop */
		while (have_posts()) : the_post();
?>
			<!---full width-->
			<div class="col-md-10 col-md-offset-1">
				<?php get_template_part('template-parts/blog', 'layout'); ?>
			</div>
		<?php
		endwhile; /* End Loop */
	endif;
} elseif ($moto_blog_layout_opts == 'twocolumn') {

	echo "two column";

	if (have_posts()) :
		/* Start the Loop */
		while (have_posts()) : the_post();

		?>
			<!---2 column width-->
			<div class="col-lg-6 col-md-6 col-sm-6">
				<?php get_template_part('template-parts/blog', 'layout'); ?>
			</div>
	<?php endwhile;/* End Loop */
	endif;
	?>
	<div class="col-md-12 text-center">
		<?php moto_blog_pagination(); ?>
	</div>
	<?php } elseif ($moto_blog_layout_opts == 'threecolumn') {
	echo "three column";
	if (have_posts()) :
		/* Start the Loop */
		while (have_posts()) : the_post(); ?>
			<!---3 column width-->
			<div class="col-lg-4 col-md-4 col-sm-6">
				<?php get_template_part('template-parts/blog', 'layout'); ?>
			</div>
	<?php endwhile;/* End Loop */
	endif;
	?>
	<div class="col-md-12 text-center">
		<?php moto_blog_pagination(); ?>
	</div>
<?php } elseif ($moto_blog_layout_opts == 'left') { ?>
	<!---left sidebar-->
	<div class="col-lg-3 col-md-3 mb-30">
		<?php get_sidebar('left'); ?>
	</div>
	<div class="col-lg-9 col-md-9">
		<?php
		if (have_posts()) :
			/* Start the Loop */
			while (have_posts()) : the_post();

				get_template_part('template-parts/blog', 'layout');

			endwhile;/* End Loop */
		endif;
		?>
		<?php moto_blog_pagination(); ?>
	</div>
<?php } else { ?>
	<!---right sidebar-->
	<div class="col-12 col-sm-12 col-md-8">
		<?php get_template_part('template-parts/blog', 'main'); ?>
	</div>
	<div class="col-12 col-sm-12  col-md-4 col-lg-4 mb-30 pt-4 pt-md-0">
		<?php get_sidebar('main'); ?>
	</div>
<?php
} ?>