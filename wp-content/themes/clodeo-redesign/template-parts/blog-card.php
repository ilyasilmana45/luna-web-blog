<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$card_blog_query_args = array(
    'post_type'      => 'post',
    'post_status' => 'publish',
    'posts_per_page' => '6',
    'paged' => $paged
);
$card_blog_query = new WP_Query($card_blog_query_args);
$index = 0;
?>

<div id="card-main-blog">
    <div class="card-list">
        <div class="card-wrapper row">
            <?php
            while ($card_blog_query->have_posts()) {
                $card_blog_query->the_post();
                $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), 'moto_blog_img', true);
            ?>
                <div id="post-<?php the_ID(); ?>" class="col-6 col-md-6 py-4 py-md-3">
                    <a class="no-underline" href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark">
                        <div class="row blog-card-row">
                            <div class="col-12 pl-0 pr-0">
                                <?php if (has_post_thumbnail()) { ?>
                                    <img src="<?php echo esc_url($image[0]); ?>" alt="<?php echo esc_attr('Blog image'); ?>" class="img-fluid" />
                                <?php } else { ?>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/no-image.png" class="img-fluid" style="width: 100%" />
                                <?php } ?>
                            </div>
                            <div class="col-12 px-4 pt-4">
                                <?php if (get_the_tags()) :  ?>
                                    <div class="row ">
                                        <?php foreach (get_the_tags() as $tags) { ?>
                                            <?php if ($index == 2) {
                                                $index = 0;
                                                break;
                                            } else {
                                                $index++;
                                            } ?>
                                            <div class="col-6">
                                                <p class="breadcrumbs"><?php echo $tags->name ?></p>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php endif ?>
                            </div>
                            <div class="col-12 px-4">
                                <?php the_title(sprintf('<p class="card-title">', esc_url(get_permalink())), '</p>'); ?>
                            </div>
                            <div class="col-12 card-date-time px-4">
                                <p class="card-date-time">
                                    <?php echo get_the_date(); ?>
                                    <span class="card-author">
                                        <?php
                                        echo get_the_author_meta('display_name');
                                        ?>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
            <?php
            }
            // wp_reset_postdata();
            ?>
            <div class="col-12 text-center justify-content-center pt-5">
                <div class="row">
                    <div class="col-10 col-sm-10 col-md-10 col-lg-6 text-center justfy-content-center" style="margin:auto">
                        <?php moto_blog_pagination(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="col-md-12 text-center load-more">
        <div class="load-more-button">
            <i class="fas fa-chevron-down pr-1"></i> Load More Articles
        </div>
    </div> -->
</div>