<?php
$banner_query_args = array(
    'posts_per_page' => 9,
    'post_type' => 'post',
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',

    'date_query' => array(
        array(
            'after' => '6 month ago'
        )
    )
);
$banner_posts = new WP_Query($banner_query_args);

$index = 0;
$tagsIndex = 0;
$indicator = 0;

?>
<div id="banner-main-blog" class="carousel slide carousel-fade pb-5" data-ride="carousel">
    <div class="carousel-inner">
        <ol id="carousel-slider-home-page" class="carousel-indicators">
            <?php while ($banner_posts->have_posts()) {
                $banner_posts->the_post(); ?>
                <li data-target="#banner-main-blog" data-slide-to="<?php echo $indicator ?>" class="<?php $indicator == 0 ? print('active') : print('') ?>"></li>
            <?php $indicator++;
            }; ?>
        </ol>
        <?php while ($banner_posts->have_posts()) {
            $banner_posts->the_post();
            $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), 'moto_blog_img', true);
        ?>
            <div class="carousel-item carousel-main coursel-jpeg-as-background <?php $index == 0 ? print('active') : print('') ?>">
                <a href="<?php echo get_permalink() ?>">
                    <div class="carousel-main-custom-item pb-4">
                        <div class="row carousel-item-wrapper" style="margin-top: -50px;">
                            <div class="col-md-12 col-sm-12 carousel-card">
                                <div class="carousel-card-cover carousel-relative">
                                    <?php if (has_post_thumbnail()) { ?>
                                        <img src="<?php echo esc_url($image[0]); ?>" alt="<?php echo esc_attr('Blog image'); ?>" class="img-fluid img-rounded" />
                                    <?php } else { ?>
                                        <div class="no-image-div">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/no-image-logo.png" class="img-fluid img-rounded" />
                                        </div>
                                    <?php } ?>
                                    <div class="caption-image">
                                        <span class="caption-text">FEATURED</span>
                                    </div>
                                </div>
                                <div class="carousel-card-body">
                                    <div class="row card-content-header">
                                        <div class="col-12 pt-4 px-0">
                                            <?php if (get_the_tags()) :  ?>
                                                <div class="row ">
                                                    <?php foreach (get_the_tags() as $tags) { ?>
                                                        <?php if ($tagsIndex == 2) {
                                                            $tagsIndex = 0;
                                                            break;
                                                        } else {
                                                            $tagsIndex++;
                                                        } ?>
                                                        <div class="col-6">
                                                            <p class="breadcrumbs"><?php echo $tags->name ?></p>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                    <div class="row card-content-content pt-10">
                                        <p><?php echo the_title() ?></p>
                                    </div>
                                    <div class="row">
                                        <p class="text-content">
                                            <?php
                                            echo wp_trim_words(get_the_content(null, true), 50)
                                            ?>
                                        </p>
                                    </div>
                                    <div class="card-content-footer col-12">
                                        <div class="row">
                                            <div class="col-12 card-content-body">
                                                <p class="date">
                                                    <?php the_date() ?>
                                                    <span class="pl-20 author"><?php echo get_the_author_meta('display_name') ?></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        <?php $index++;
        } ?>
    </div>
</div>