<?php get_header(); ?>
<div class="blog-main row">
    <div class="col-12">
        <?php while (have_posts()) {
            the_post();
            $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), 'moto_blog_img', true);
        ?>
            <div class="row">
                <div class="col-12 blog-breadcrumb text-left">
                    <ol class="blog-breadcrumb-list">
                        <li class="item"><a href="<?php echo home_url() ?>">Home</a></li>
                        <li class="item active" aria-current="page"><?php echo the_title(); ?></li>
                    </ol>
                    </nav>
                </div>
                <div class="col-12 py-4">
                    <?php if (has_post_thumbnail()) { ?>
                        <img src="<?php echo esc_url($image[0]); ?>" alt="<?php echo esc_attr('Blog image'); ?>" class="img-fluid" />
                    <?php } else { ?>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/no-image.png" class="img-fluid" style="width: 100%" />
                    <?php } ?>
                </div>
                <div class="col-12 py-3 blog-text-description">
                    <?php
                    the_content(
                        sprintf(
                            wp_kses(
                                /* translators: %s: Post title. Only visible to screen readers. */
                                __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentynineteen'),
                                array(
                                    'span' => array(
                                        'class' => array(),
                                    ),
                                )
                            ),
                            get_the_title()
                        )
                    );
                    ?>
                </div>
                <div id="comment-section" class="col-12 py-3 blog-comment-section">
                    <?php
                    if (comments_open() || get_comments_number()) :
                        comments_template();
                    endif;
                    ?>
                </div>
            </div>
        <?php  }  ?>
    </div>
</div>