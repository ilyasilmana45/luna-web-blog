<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package moto
 */

?>

</div><!-- #content -->
<!-- START FOOTER AREA -->
<div id="footer-block">
	<div class="container py-5">
		<div class="row">
			<div class="col-12 col-md-2 d-none d-md-block text-center" style="position: relative;">
				<div class="image-footer pt-5">
					<a href="<?php echo home_url() ?>">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/clodeo-logo.png" style="width:100px" alt="" class="img-fluid">
					</a>
				</div>
			</div>
			<div class="col-md-6 pt-5">
				<div class="row">
					<div class="col-md-4 pt-2">
						<div class="content-footer">
							<div class="footer-title">
								Perusahaan
							</div>
							<div class="footer-content">
								<ul class="footer-list">
									<li><a href="https://lunapos.id/tentang-kami">Tentang Kami</a></li>
									<li><a href="https://lunapos.id/bermitra-dengan-kami">Bermitra dengan Kami</a></li>
									<li><a href="https://help.lunapos.id/">Bantuan</a></li>
									<li><a href="https://lunapos.id/kebijakan-privasi">Kebijakan Privasi</a></li>
									<li><a href="https://lunapos.id/syarat-dan-ketentuan">Syarat & Ketentuan</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 pt-2">
						<div class="content-footer">
							<div class="footer-title">
								Fitur
							</div>
							<div class="footer-content">
								<ul class="footer-list">
									<li><a href="https://lunapos.id/fitur/accounting-management-system">Accounting Management System</a></li>
									<li><a href="https://lunapos.id/fitur/item-bundling-and-ingredients">Item Bundling and Ingredients</a></li>
									<li><a href="https://lunapos.id/fitur/table-and-franchise-management">Table and Franchise Management</a></li>
									<li><a href="https://lunapos.id/fitur/split-bill">Split Bill</a></li>
									<li><a href="https://lunapos.id/fitur/merge-bill">Merge Bill</a></li>
									<li><a href="https://lunapos.id/fitur/multi-payment">Multi Payment</a></li>
									<li><a href="https://lunapos.id/fitur/offline-multi-station">Offline Mult Station</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 pt-2">
						<div class="content-footer">
							<div class="footer-title">
								Quick Link
							</div>
							<div class="footer-content">
								<ul class="footer-list">
									<li><a href="https://lunapos.id/aplikasi-kasir">Aplikasi Kasir</a></li>
									<li><a href="https://lunapos.id/aplikasi-pos">Aplikasi POS</a></li>
									<li><a href="https://lunapos.id/kasir-restoran">Kasir Restoran</a></li>
									<li><a href="https://lunapos.id/software-kasir">Software Kasir</a></li>
									<li><a href="https://lunapos.id/software-pos">Software POS</a></li>
									<li><a href="https://lunapos.id/aplikasi-point-of-sales">Aplikasi Point of Sales</a></li>
									<li><a href="https://lunapos.id/kasir-pos">Kasir POS</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 pt-5">
				<div class="content-footer pt-2">
					<div class="footer-title">
						Info Kontak
					</div>
					<div class="footer-content">
						<ul class="footer-list">
							<li>
								<div class="row">
									<div class="col-md-2">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/location.png" class="img-fluid" />
									</div>
									<div class="col-md-10">
										Jl. Hang Jebat IV No. 5,<br/> Kebayoran Baru, Jakarta Selatan 12120
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-md-2">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/telephone.png" class="img-fluid" />
									</div>
									<div class="col-md-10">+62 822 8888 2300</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-md-2">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/email.png" class="img-fluid" />
									</div>
									<div class="col-md-10">
										Inquiry: hello@lunapos.id
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div id="footer-news-letter" class="col-12">
				<div class="row py-5">
					<div class="col-12 col-md-3 text-center text-md-right">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/message.png" />
					</div>
					<div class="col-md-6 text-center text-md-left py-2">
						<div class="title-news-letter">
							Join our Newsletter
						</div>
						<div class="content-news-letter">
							Sign up to receive a monthly email on the latest Clodeo updates, features, and news!
						</div>
					</div>
					<div id="news-letter-input" class="col-md-3 text-center text-md-left pt-2">
						<div class="input-group">
							<input type="text" class="form-control newsletter" placeholder="Your Email Address" aria-describedby="basic-addon2">
							<div class="input-group-append">
								<button id="basic-addon2" class="btn btn-primary btn-subscribe">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/arrow-right.png" />
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php get_template_part('inc/footer/copyright','luna') ?>
	</div>
</div>
<?php wp_footer(); ?>
</body>
<script>
	window.onload = function() {
		var elem = document.getElementsByClassName("liveChatIntercom");
		elem[0].onclick = function() {
			Intercom('show');
		}
	}
</script>

</html>