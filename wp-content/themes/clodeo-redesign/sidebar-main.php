<?php
$popular_posts_query_args = array(
    'posts_per_page' => 3,
    'post_type'      => 'post',
    'meta_key'       => 'post_views_count',
    'orderby'        => 'meta_value_num',
    'order'          => 'DESC'
);
$popular_posts = new WP_Query($popular_posts_query_args);

?>

<div class="side-widget">
    <div class="side-widget-search">
        <p class="side-widget-label">
            Search Articles
        </p>
        <?php get_template_part('searchform'); ?>
    </div>
    <div class="side-widget-categories pt-5">
        <p class="side-widget-label">
            Categories
        </p>
        <ul class="list-group">
            <?php foreach (get_categories() as $categories) { ?>
                <li class="list-group-item"><a href="<?php echo get_category_link($categories) ?>"><?php echo $categories->cat_name ?></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="side-widget-recent pt-5">
        <p class="side-widget-label">
            Popular Posts
        </p>
        <?php while ($popular_posts->have_posts()) {
            $popular_posts->the_post();
            $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()), 'moto_blog_img', true);
        ?>
            <a href="<?php echo esc_url(get_permalink()) ?>">
                <div class="row side-widget-content py-3 py-md-1">
                    <div class="col-12 col-md-4 text-center text-md-left align-self-center">
                        <?php if (has_post_thumbnail()) { ?>
                            <img class="img-fluid side-widget-image" src="<?php echo esc_url($image[0]); ?>" alt="<?php echo esc_attr('Blog image'); ?>"/>
                        <?php } else { ?>
                            <img class="img-fluid side-widget-image" src="https://images.unsplash.com/photo-1507525428034-b723cf961d3e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjExMjU4fQ&w=1000&q=80" alt=""/>
                        <?php } ?>
                    </div>
                    <div class="col-12 col-md-8 side-widget-text text-center text-md-left pt-3 align-self-center">
                        <?php the_title() ?>
                        <p class="side-widget-author"><?php echo get_the_author_meta('display_name') ?></p>
                    </div>
                </div>
            </a>
        <?php } ?>
    </div>
    <div class="side-widget-tags topics">
        <p class="side-widget-label pl-md-2">
            Topics
        </p>
        <a href="<?php echo home_url() ?>/tag/fitur" ><span class="badge badge-pill badge-general">Fitur</span></a>
        <a href="<?php echo home_url() ?>/tag/cash-on-delivery" ><span class="badge badge-pill badge-general">Cash On Delivery</span></a>
        <a href="<?php echo home_url() ?>/tag/tips" ><span class="badge badge-pill badge-general">Tips</span></a>
        <a href="<?php echo home_url() ?>/tag/aplikasi-akuntansi" ><span class="badge badge-pill badge-general">Aplikasi Akuntansi</span></a>
        <a href="<?php echo home_url() ?>/tag/bisnis-online" ><span class="badge badge-pill badge-general">Bisnis Online</span></a>
    </div>
    <div class="side-widget-hyperlink">
        <p class="side-widget-hyperlink-label">Yuk kelola bisnis onlinemu dengan Clodeo sekarang!</p>
        <p class="side-widget-hyperlink-desc">
            Gunakan Clodeo agar kegiatan bisnis menjadi lebih mudah, cepat, bisa terpantau dimanapun dan kapanpun. Daftar dan coba GRATIS 30 hari!
        </p>
        <a href="https://app.clodeo.com/" target="_blank" class="side-widget-hyperlink-link">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/arrow-down.png" class="pr-3" style="margin-bottom: 2px;" />Coba Clodeo Gratis
        </a>
    </div>
</div>