<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package moto
 */

?>

	</div><!-- #content -->
	<!-- START FOOTER AREA -->
	<footer id="footer" class="footer-area">
		<?php // moto_footer_area(); ?>

		<div class="footer-top-area">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-2 col-xs-12">
						<div id="pages-2" class="single-footer widget widget_pages">
							<h4 class="footer-title">Produk</h4>
							<ul>
								<li>
									<a href="https://clodeo.com/404">Clodeo</a>
								</li>
								<li>
									<a href="https://clodeo.com/kemanan-dan-privasi/">Keamanan dan Privasi</a>
								</li>
								<li>
									<a href="https://clodeo.com/syarat-penggunaan/">Syarat Penggunaan</a>
								</li>
							</ul>
						</div>
					</div>

					<div class="col-sm-6 col-md-2 col-xs-12">
						<div id="pages-3" class="single-footer widget widget_pages">
							<h4 class="footer-title">Perusahaan</h4>
							<ul>
								<li>
									<a href="https://clodeo.com/tentang-clodeo/">Tentang Clodeo</a>
								</li>
								<li>
									<a href="https://clodeo.com/404">Karir di Clodeo</a>
								</li>
								<li>
									<a href="https://clodeo.com/hubungi-clodeo/">Hubungi Clodeo</a>
								</li>
							</ul>
						</div>
					</div>

					<div class="col-sm-6 col-md-2 col-xs-12">
						<div id="pages-3" class="single-footer widget widget_pages">
							<h4 class="footer-title">Partner</h4>
							<ul>
								<li>
									<a href="https://clodeo.com/404">Partner Clodeo</a>
								</li>
								<li>
									<a href="https://clodeo.com/404">Menjadi Partner Clodeo</a>
								</li>
							</ul>
						</div>
					</div>

					<div class="col-sm-6 col-md-2 col-xs-12">
						<div
							id="custom_html-2"
							class="widget_text single-footer widget widget_custom_html"
						>
							<h4 class="footer-title">Ikuti Kami</h4>
							<div class="textwidget custom-html-widget">
								<ul>
									<li>
										<a href="https://www.facebook.com/clodeoapp">Facebook</a>
									</li>
									<li>
										<a href="https://twitter.com/clodeo_id?lang=id">Twitter</a>
									</li>
									<li>
										<a href="https://www.instagram.com/clodeo/">Instagram</a>
									</li>
									<li>
										<a href="https://www.youtube.com/channel/UC_QfZDU5OFYvRiF4kI8Mfsw?view_as=subscriber">Youtube</a>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-md-4 col-xs-12">
						<div
							id="custom_html-3"
							class="widget_text single-footer widget widget_custom_html"
						>
							<h4 class="footer-title">Info Kontak</h4>
							<div class="textwidget custom-html-widget">
								<div class="info-kontak">
									<img
										src="https://i1.wp.com/clodeo.com/wp-content/uploads/2018/07/rectangle-5@2x-2.png?w=640"
										alt=""
										data-recalc-dims="1"
										class="jetpack-lazy-image jetpack-lazy-image--handled"
										data-lazy-loaded="1"
									/><noscript
										><img
											src="https://i1.wp.com/clodeo.com/wp-content/uploads/2018/07/rectangle-5@2x-2.png?w=640"
											alt=""
											data-recalc-dims="1"
									/></noscript>
									<div class="detail-kontak">
										<p>
											Jl. BKR No.24<br />Kel Cijagra, Kec Lengkong,<br />Bandung
										</p>
									</div>
								</div>
								<div class="info-kontak">
									<img
										src="https://i1.wp.com/clodeo.com/wp-content/uploads/2018/07/rectangle-5@2x-1.png?w=640"
										alt=""
										data-recalc-dims="1"
										class="jetpack-lazy-image jetpack-lazy-image--handled"
										data-lazy-loaded="1"
									/><noscript
										><img
											src="https://i1.wp.com/clodeo.com/wp-content/uploads/2018/07/rectangle-5@2x-1.png?w=640"
											alt=""
											data-recalc-dims="1"
									/></noscript>
									<div class="detail-kontak"><p><a href="tel:+6281377332863">0813. 7733. 2863</p></a></div>
								</div>
								<div class="info-kontak">
									<img
										src="https://i1.wp.com/clodeo.com/wp-content/uploads/2018/07/rectangle-5@2x.png?w=640"
										alt=""
										data-recalc-dims="1"
										class="jetpack-lazy-image jetpack-lazy-image--handled"
										data-lazy-loaded="1"
									/><noscript
										><img
											src="https://i1.wp.com/clodeo.com/wp-content/uploads/2018/07/rectangle-5@2x.png?w=640"
											alt=""
											data-recalc-dims="1"
									/></noscript>
									<div class="detail-kontak">
										<p>Inquiry: <a href="mailto:hello@clodeo.com">hello@clodeo.com</a></p>
										<p>Support: <a href="mailto:support@clodeo.com">support@clodeo.com</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php moto_copyright_text(); ?>
	</footer>
</div><!-- #page -->
</div>

<?php wp_footer(); ?>
    


</body>
</html>
