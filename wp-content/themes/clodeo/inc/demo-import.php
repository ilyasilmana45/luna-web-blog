<?php
function moto_import_files() {
  return array(
    array(
        'import_file_name'             => 'Standard Demo',
        'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demo-content/default/moto.xml',
        'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demo-content/default/moto.wie',
        'local_import_customizer_file' => trailingslashit( get_stylesheet_directory() ) . 'inc/demo-content/default/moto.dat',
        'local_import_redux'           => array(
          array(
            'file_path'   => trailingslashit( get_template_directory() ) . 'inc/demo-content/default/moto.json',
            'option_name' => 'moto_opt',
          ),
        ),
        'import_preview_image_url'     => get_template_directory_uri() .'/screenshot.png',
        'import_notice'                => __( 'After you import this demo, you will have setup all content.', 'moto' ),
    ),
    array(
      'import_file_name'           => 'Comming Soon',
      'local_import_file'            => get_stylesheet_directory() . '/inc/demo-content/default/moto.xml',
      'import_widget_file_url'     => get_stylesheet_directory() . '/inc/demo-content/default/moto.wie',
      'local_import_customizer_file' => get_stylesheet_directory() . '/inc/demo-content/default/moto.dat',
      'local_import_customizer_file' => get_stylesheet_directory() . '/inc/demo-content/default/moto.json',
      
      ),
      'import_preview_image_url'     => get_stylesheet_directory_uri().'/inc/demo-content/img/1.jpg',
    );


////////////////////////////////////////////////////////////////////
 
}
add_filter( 'pt-ocdi/import_files', 'moto_import_files' );

function moto_after_import_setup() {
    // Assign menus to their locations.
    $header_menu = get_term_by( 'name', 'One Page Nav', 'nav_menu' );

        set_theme_mod( 'nav_menu_locations' , array( 
				'onepage' => $header_menu->term_id,
			  
             ) 
        );

    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'moto_after_import_setup' );