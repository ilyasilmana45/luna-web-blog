<?php 
/*
 * Moto Global Function
 * Author: codecarnival
 * Author URI: http://hastech.company
 * Version: 1.0.0
 * ======================================================
 */
 
 //Blog Header
	function moto_blog_header(){ 
		$moto_opt = moto_get_opt();
		if (isset( $moto_opt['moto_blog_text'] )){
			echo esc_html($moto_opt['moto_blog_text']);
		}
		else{
			esc_html_e('Blog', 'moto');
		}
	}
	
	
 
  /*
 * =======================================================
 *    Register Post Excerpt Function   
 * =======================================================
 */

	 function moto_post_excerpt(){
  
		$moto_opt = moto_get_opt();
		
		if(isset ($moto_opt['moto_excerpt_length'])){
		 echo wp_trim_words( get_the_content(), $moto_opt['moto_excerpt_length'], '' );
		}else{
		 echo wp_trim_words( get_the_content(), 29, '' );
		}
	  
	 }
  /*=====Register blog read more Function====*/
	function moto_read_more(){
		$moto_opt = moto_get_opt();

		if( isset( $moto_opt ) ){ 
			echo esc_html( isset($moto_opt['moto_readmore_text']) ? $moto_opt['moto_readmore_text'] : 'Ream More' ); 
		}	
	}

	/*=====footer area ====*/
	function moto_footer_area(){
		$moto_opt = moto_get_opt();
		
		if( isset($moto_opt['moto_footer_widget']) ? $moto_opt['moto_footer_widget'] : '' == true || isset($moto_opt['moto_footer_widget']) ? $moto_opt['moto_footer_widget'] : '' == '' ):
		?>
		<div class="footer-top-area">
			<div class="<?php if( isset($moto_opt['moto_footer_widget_width']) ? $moto_opt['moto_footer_widget_width'] : '' == true ){ echo esc_attr('container-fluid');}else{ echo esc_attr( 'container' );} ?>">
				<div class="row">
					<?php 

						 $footer_widget_column = isset($moto_opt['moto_footer_layoutcolumns']) ? $moto_opt['moto_footer_layoutcolumns'] : '';

						if( $footer_widget_column ){
							$footer_widget_column = $footer_widget_column;
						}else{
							$footer_widget_column = 4;
						}
						$footer_sidebar_count= $footer_widget_column;  
						
						if( 0 != $footer_sidebar_count ) {  
						if( '' == $footer_sidebar_count ) $footer_sidebar_count = $footer_widget_column;
						$footer_sidebar_class = floor( 12/$footer_sidebar_count ); 
						
						for( $footer = 1; $footer <= $footer_sidebar_count; $footer++ ) { 
						 if ( is_active_sidebar('moto-footer-' . $footer) ) : ?>	
					<div class="col-sm-6 col-md-<?php echo esc_attr( $footer_sidebar_class ); ?>  col-xs-12 ">

						<?php dynamic_sidebar('moto-footer-' . $footer); ?>
					</div>
						<?php endif; } } ?>
				</div>
			</div>
		</div>
		
		<?php
		
		endif;
	}
	
	/*====footer copyright text area====*/
	
	function moto_copyright_text(){
		$moto_opt = moto_get_opt();

		if( isset($moto_opt['moto_footer_bottom_show']) ? $moto_opt['moto_footer_bottom_show'] : '' == true || isset($moto_opt['moto_footer_bottom_show']) ? $moto_opt['moto_footer_bottom_show'] : '' =='' ){
		
		?>
		
		<div class="footer-bottom">
			<div class="<?php if( isset($moto_opt['moto_footer_copyright_width']) ? $moto_opt['moto_footer_copyright_width'] : '' == true ){ echo esc_attr('container-fluid');}else{ echo esc_attr( 'container' );} ?>">
				<div class="copyright">
					<div class="row">
						<?php if( isset($moto_opt['moto_copyright_style']) ? $moto_opt['moto_copyright_style'] : '' == '1' ){ ?>
							<?php 				
								$layout = $moto_opt['moto_footer']['enabled'];
								if ($layout): foreach( $layout as $key=>$value ) {
								switch($key) {
									case 'copyright': get_template_part( 'inc/footer/copyright' ); 
									break;	
									
									case 'socialicon': get_template_part( 'inc/footer/social','icon' );
									break;
									case 'copyrightmenu': get_template_part( 'inc/footer/copyright','menu' );
									break;		 
								}
							}
							endif;	
							?>
							<?php
						} elseif( isset($moto_opt['moto_copyright_style']) ? $moto_opt['moto_copyright_style'] : '' == '2' ){
							?>
					<div class="col-md-12 text-center">
		                <div class="footer-all">
							<div class="conct-border two">
								<div class="col-md-4 col-sm-4">
									<div class="single-address">
										<div class="media">
										<?php if( $moto_opt['moto_phone_icon'] ): ?>
											<div class="media-left">
												<i class="<?php echo esc_html($moto_opt['moto_phone_icon']);?>"></i>
											</div>
										<?php endif; ?>
										<?php if( $moto_opt['moto_phone_text'] ): ?>
											<div class="media-body text-center">
												<p><?php echo esc_html($moto_opt['moto_phone_text']);?> <br> <?php echo esc_html($moto_opt['moto_phone_text_2']);?></p>
											</div>
										<?php endif; ?>
										</div>
									</div>
								</div>
								<div class="col-md-4 col-sm-4">
									<div class="single-address">
										<div class="media">
										<?php if( $moto_opt['moto_mail_icon'] ): ?>
											<div class="media-left tw">
												<i class="<?php echo esc_html($moto_opt['moto_mail_icon']);?>"></i>
											</div>
										<?php endif; ?>
										<?php if( $moto_opt['moto_mail_text'] ): ?>
											<div class="media-body text-center">
												<p><?php echo esc_html($moto_opt['moto_mail_text']);?> <br> <?php echo esc_html($moto_opt['moto_mail_text_2']);?></p>
											</div>
										<?php endif; ?>
										</div>
									</div>
								</div>
								<div class="col-md-4 col-sm-4">
									<div class="single-address">
										<div class="media">
										<?php if( $moto_opt['moto_address_icon'] ): ?>
											<div class="media-left three">
												<i class="<?php echo esc_html($moto_opt['moto_address_icon']);?>"></i>
											</div>
										<?php endif; ?>
										<?php if( $moto_opt['moto_address_text'] ): ?>
											<div class="media-body text-center">
												<p><?php echo esc_html($moto_opt['moto_address_text']);?> <br> <?php echo esc_html($moto_opt['moto_address_text_2']);?></p>
											</div>
										<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
		                	<?php if( $moto_opt['moto_copyright2_logo']['url'] ): ?>
		                    <div class="footer-logo logo">
		                        <a href="<?php echo esc_url( home_url('/') ); ?>"><img src="<?php echo esc_url( $moto_opt['moto_copyright2_logo']['url'] ); ?>" alt="<?php echo esc_attr('Footer Logo'); ?>"></a>
		                    </div>
							<?php endif; ?>
		                    <div class="footer-icon">
		                        <p><?php 
		                        		if(isset( $moto_opt['moto_copyright_copany_info'] ) && $moto_opt['moto_copyright_copany_info']!=='' ){
										echo wp_kses( $moto_opt['moto_copyright_copany_info'] , array(
											'a'       => array(
											'href'    => array(),
											'title'   => array()
											),
											'br'      => array(),
											'em'      => array(),
											'strong'  => array(),
											'img'     => array(
												'src' => array(),
												'alt' => array()
											),
										));
									}


		                        ?></p>
		                        <?php if( $moto_opt['moto_copyright_social_icon_show']== true): ?>
		                        <ul>
		                         	<?php 
		                         		foreach($moto_opt['moto_social_icons'] as $key=>$value ) { 
											 if($value!=''){
												if($key=='vimeo'){
												 echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>';
												} else {
												 echo '<li><a class="'.esc_attr($key).' social-icon" href="'.esc_url($value).'" title="'.ucwords(esc_attr($key)).'" target="_blank"><i class="fa fa-'.esc_attr($key).'"></i></a></li>';
												}
											   }
											} 
		                            ?>
		                        </ul>
		                    <?php endif; ?>
		                    </div>
		                    <div class="footer-text">
		                         <p><?php 
		                        		if( isset( $moto_opt['moto_copyright_info_two'] ) && $moto_opt['moto_copyright_info_two']!=='' ){
										echo wp_kses( $moto_opt['moto_copyright_info_two'] , array(
											'a'       => array(
											'href'    => array(),
											'title'   => array()
											),
											'br'      => array(),
											'em'      => array(),
											'strong'  => array(),
											'img'     => array(
												'src' => array(),
												'alt' => array()
											),
										));
									}


		                        ?></p>
		                    </div>
		                </div>
           			</div>


						<?php

						}else{
							?>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
								<div class="copyright-text">
									<p><?php 
										esc_html_e('Copyright', 'moto'); ?> &copy; <?php echo date("Y").' '.get_bloginfo('name');  esc_html_e(' All Rights Reserved.', 'moto' ); 

									?></p>
								</div>
							</div>
						<?php
								}
							} 
						?>
					</div>
				</div>
			</div>
		</div>
	
	<?php 
		}