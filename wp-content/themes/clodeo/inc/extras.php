<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package moto
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function moto_body_classes( $classes ) {
	$moto_opt = moto_get_opt();

	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// custom body class 

	$moto_layout_width = isset($moto_opt['moto_layout_width']) ? $moto_opt['moto_layout_width'] : '';
	if ( isset( $moto_layout_width ) ) {
		$moto_layout_width_value = $moto_layout_width;
	} else {
		$moto_layout_width_value = $moto_layout_width;
	};

	if( isset( $moto_layout_width_value ) && 'boxed-layout' == $moto_layout_width_value){
		$classes[] = 'boxed-layout-active';
	}else{
		$classes[] = 'wide-layout-active';
	}

	// Header layout classes
	$header_style = isset($moto_opt ['moto_header_layout']) ? $moto_opt ['moto_header_layout'] : '';

	if ($header_style == '1') {
	 	$classes[] = 'header-style-1';
	}

	if ($header_style == '2') {
	 	$classes[] = 'header-style-2';
	}

	if ($header_style == '3') {
	 	$classes[] = 'header-one-page';
	}
    if (isset($moto_opt)) {
     	$classes[] = 'redux_active';
    }

	return $classes;
}
add_filter( 'body_class', 'moto_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function moto_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'moto_pingback_header' );
