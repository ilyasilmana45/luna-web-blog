<?php

	add_action('cmb2_meta_boxes','moto_meta_boxes');
	if( ! function_exists('moto_meta_boxes') ){

		function moto_meta_boxes(){
			$prefix = '_moto_';

			$posts = new_cmb2_box( array(
				'id'           		 => $prefix . '_moto_post_extra_optons',
				'title'        		 => esc_html__( 'Post Settings', 'moto' ),
				'object_types' 		 => array('post'),
				'context'      		 => 'normal',
				'priority'     		 => 'high',
				'show_names'         => true,
			) );

			$posts->add_field( array(
				'name'               =>  esc_html__( 'Select Page Layout', 'moto' ),
				'id'                 => $prefix.'post_layout',
				'type'        		 => 'select',
				'default'     		 => 'default',
				'options'     		 => array(
					''     	 		=> esc_html__( 'Select Layout', 'moto' ),
					'full'     	 	=> esc_html__( 'Full Width', 'moto' ),
					'left'       	=> esc_html__( 'Left Sidebar', 'moto' ),
					'right'       	=> esc_html__( 'Right Sidebar', 'moto' ),
				),
			) );


			//page breadcrump support
			$pages = new_cmb2_box( array(
				'id'           		 => $prefix . '_moto_page_optons',
				'title'        		 => esc_html__( 'Page Settings', 'moto' ),
				'object_types' 		 => array( 'page','post'),
				'context'      		 => 'normal',
				'priority'     		 => 'high',
				'show_names'         => true,
			) );
			//breadcrumb 
			$pages->add_field( array(
				'name'               =>  esc_html__( 'Select Menu Style', 'moto' ),
				'id'                 => $prefix.'page_menu_style',
				'type'        		 => 'select',
				'default'     		 => 'default',
				'options'     		 => array(
					'default'     	 => esc_html__( 'Default', 'moto' ),
					'one_page'       => esc_html__( 'One page', 'moto' ),
				),
			) );

			//breadcrumb 
			$pages->add_field( array(
				'name'               =>  esc_html__( 'Enable Breadcrumbs', 'moto' ),
				'id'                 => $prefix.'page_titlebar_enable',
				'type'        		 => 'select',
				'default'     		 => 'yes',
				'options'     		 => array(
					'yes'     		 => esc_html__( 'Yes', 'moto' ),
					'no'             => esc_html__( 'No', 'moto' ),
				),
			) );

			//breadcrumb color
			$pages->add_field( array(
				'name'               => esc_html__( 'Breadcrumb Background Color', 'moto' ),
				'id'          		 => $prefix .'banner_color',
				'desc'        		 => esc_html__( 'Breadcrumb Background Color', 'moto' ),
				'type'               => 'colorpicker',
			) );

			//breadcrumb image
			$pages->add_field( array(
				'name'               => esc_html__( 'Upload Breadcrumb Image', 'moto' ),
				'id'          		 => $prefix .'banner_img',
				'desc'        		 => esc_html__( 'upload image here', 'moto' ),
				'context'      		 => 'normal',
				'priority'    		 => 'high',
				'type'               => 'file',
			) );

			//page title color
			$pages->add_field( array(
				'name'               => esc_html__( 'BG Overlay Color', 'moto' ),
				'id'                 => $prefix .'page_title_bg_overlay_color',
				'desc'               => esc_html__( 'Breadcrumbs Overlay Color', 'moto' ),
				'type'               => 'colorpicker',
			) );

			//page title color
			$pages->add_field( array(
				'name'               => esc_html__( 'Overlay Opacity', 'moto' ),
				'id'                 => $prefix .'page_title_overlay_opacity',
				'desc'               => esc_html__( 'Breadcrumbs Overlay Opacity ( ex: .0 - 1)', 'moto' ),
				'type'               => 'text_small',
			) );

			$pages->add_field( array(
				'name'             => esc_html__( 'Breadcrumbs Text Position', 'moto' ),
				'id'               => $prefix .'breadcrumbs_position',
				'desc'             => esc_html__( 'Breadcrumbs text position', 'moto' ),
				'type'             => 'select',
				'options'          => array(
					'' 		   	   => esc_html__('Select position','moto'),
					'left' 		   => esc_html__('Position left','moto'),
					'center' 	   => esc_html__('Position center','moto'),
					'right'  	   => esc_html__('Position right','moto'),
				)
			) );

			//page title color
			$pages->add_field( array(
				'name'               => esc_html__( 'Page Title Color', 'moto' ),
				'id'                 => $prefix .'page_title_color',
				'desc'               => esc_html__( 'Breadcrumbs title color', 'moto' ),
				'type'               => 'colorpicker',
			) );
			//page title font size
			$pages->add_field( array(
				'name'               => esc_html__( 'Page Title Font Size', 'moto' ),
				'id'                 => $prefix .'page_title_font_sizes',
				'desc'               => esc_html__( 'Breadcrumbs title font size', 'moto' ),
				'type'               => 'text_small',
			) );
			//breadcrumb height
			$pages->add_field( array(
				'name'               => esc_html__( 'Breadcrumbs Top Padding', 'moto' ),
				'id'                 => $prefix .'breadcrumb_padding_top',
				'desc'               => esc_html__( 'inset padding ex-80', 'moto' ),
				'type'               => 'text_small',
			) );
			//breadcrumb height
			$pages->add_field( array(
				'name'               => esc_html__( 'Breadcrumbs Bottom Padding', 'moto' ),
				'id'                 => $prefix .'breadcrumb_padding_bottom',
				'desc'               => esc_html__( 'inset padding ex-80', 'moto' ),
				'type'               => 'text_small',
			) );
			//page padding
			$pages->add_field( array(
				'name'               => esc_html__( 'Page Padding', 'moto' ),
				'id'                 => $prefix .'page_padding',
				'desc'               => esc_html__( 'Page padding', 'moto' ),
				'type'               => 'text_small',
			) );
			
			//nivo slider
			$nivoslider = new_cmb2_box( array(
				'id'                 => $prefix . '_moto_nivo_slider',
				'title'              => esc_html__( 'Nivo Slider Title One', 'moto' ),
				'object_types'       => array( 'nivo_slider'), // Post type
			) );
			
			//nivo Title One
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Title One', 'moto' ),
				'id'                 => $prefix .'nivo_top_title',
				'desc'               => esc_html__( 'Top title', 'moto' ),
				'type'               => 'text',
			) );
			//nivo slider top title font size
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Font Size', 'moto' ),
				'id'                 => $prefix .'nivo_toptitle_font_size',
				'desc'               => esc_html__( 'Top title font size', 'moto' ),
				'type'               => 'text_small',
				'default'            => '60',
			) );
			//nivo slider top title font color
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Title One Text Color', 'moto' ),
				'id'                 => $prefix .'nivo_top_title_color',
				'desc'               => esc_html__( 'Top title color', 'moto' ),
				'default'            => ' #24273e',
				'type'               => 'colorpicker',
			) );
			//nivo slider top title animation
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Title One Animation', 'moto' ),
				'id'                 => $prefix .'nivo_top_title_animation',
				'desc'               => esc_html__( 'Select title one animation', 'moto' ),
				'type'               => 'select',
				'options'            => array(
					'bounceOut'      => esc_html__('bounceOut','moto'),
					'bounceOutDown'  => esc_html__('bounceOutDown','moto'),
					'bounceOutLeft'  => esc_html__('bounceOutLeft','moto'),
					'bounceOutRight' => esc_html__('bounceOutRight','moto'),
					'bounceOutUp'    => esc_html__('bounceOutUp','moto'),
					'fadeIn'         => esc_html__('fadeIn','moto'),
					'fadeInDown'     => esc_html__('fadeInDown','moto'),
					'fadeInDownBig'  => esc_html__('fadeInDownBig','moto'),
					'fadeInLeft'     => esc_html__('fadeInLeft','moto'),
					'fadeInLeftBig'  => esc_html__('fadeInLeftBig','moto'),
					'fadeInRight'    => esc_html__('fadeInRight','moto'),
					'fadeInRightBig' => esc_html__('fadeInRightBig','moto'),
					'fadeInUp'       => esc_html__('fadeInUp','moto'),
					'fadeInUpBig'    => esc_html__('fadeInUpBig','moto'),
					'bounceIn'       => esc_html__('bounceIn','moto'),
					'bounceInDown'   => esc_html__('bounceInDown','moto'),
					'bounceInLeft'   => esc_html__('bounceInLeft','moto'),
					'bounceInRight'  => esc_html__('bounceInRight','moto'),
					'bounceInUp'     => esc_html__('bounceInUp','moto'),
				),
				'default'            => 'bounceInRight',
			) );
			
			//nivo slider title two
			$nivoslider = new_cmb2_box( array(
				'id'           		 => $prefix . '_moto_nivo_slider_title_two',
				'title'        		 => esc_html__( 'Nivo Slider Title Two', 'moto' ),
				'object_types' 		 => array( 'nivo_slider'), // Post type
			) );
			//nivo slider title two text
			$nivoslider->add_field( array(
				'name'             	 => esc_html__( 'Title Two', 'moto' ),
				'id'                 => $prefix .'nivo_bottom_title',
				'desc'               => esc_html__( 'Bottom title', 'moto' ),
				'type'               => 'text',
			) );
			//nivo slider title two highlight text
			$nivoslider->add_field( array(
				'name'             	 => esc_html__( 'Title Two Highlight Text', 'moto' ),
				'id'                 => $prefix .'nivo_bottom_title_highlight',
				'desc'               => esc_html__( 'Bottom title', 'moto' ),
				'type'               => 'text',
			) );
			//nivo slider title two text color
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Title Two Highlight Text Color', 'moto' ),
				'id'                 => $prefix .'nivo_bottom_title_highlight_color',
				'desc'               => esc_html__( 'bottom title color', 'moto' ),
				'default'            => ' #f69323',
				'type'               => 'colorpicker',
			) );
			
			//nivo slider title two text color
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Title Two Text Color', 'moto' ),
				'id'                 => $prefix .'nivo_bottom_title_color',
				'desc'               => esc_html__( 'bottom title color', 'moto' ),
				'default'            => ' #24273e',
				'type'               => 'colorpicker',
			) );
			//nivo slider bottom title font size
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Font Size', 'moto' ),
				'id'                 => $prefix .'nivo_bottomtitle_font_size',
				'desc'               => esc_html__( 'Bottom title font size', 'moto' ),
				'type'               => 'text_small',
				'default'            => '60',
			) );
			//nivo slider title two animation
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Title Two Animation', 'moto' ),
				'id'                 => $prefix .'nivo_title_two_animation',
				'desc'               => esc_html__( 'Select title two animation', 'moto' ),
				'type'               => 'select',
				'options'            => array(
					'bounceOut'      => esc_html__('bounceOut','moto'),
					'bounceOutDown'  => esc_html__('bounceOutDown','moto'),
					'bounceOutLeft'  => esc_html__('bounceOutLeft','moto'),
					'bounceOutRight' => esc_html__('bounceOutRight','moto'),
					'bounceOutUp'    => esc_html__('bounceOutUp','moto'),
					'fadeIn'         => esc_html__('fadeIn','moto'),
					'fadeInDown'     => esc_html__('fadeInDown','moto'),
					'fadeInDownBig'  => esc_html__('fadeInDownBig','moto'),
					'fadeInLeft'     => esc_html__('fadeInLeft','moto'),
					'fadeInLeftBig'  => esc_html__('fadeInLeftBig','moto'),
					'fadeInRight'    => esc_html__('fadeInRight','moto'),
					'fadeInRightBig' => esc_html__('fadeInRightBig','moto'),
					'fadeInUp'       => esc_html__('fadeInUp','moto'),
					'fadeInUpBig'    => esc_html__('fadeInUpBig','moto'),
					'bounceIn'       => esc_html__('bounceIn','moto'),
					'bounceInDown'   => esc_html__('bounceInDown','moto'),
					'bounceInLeft'   => esc_html__('bounceInLeft','moto'),
					'bounceInRight'  => esc_html__('bounceInRight','moto'),
					'bounceInUp'     => esc_html__('bounceInUp','moto'),
				),
				'default'            => 'bounceInRight',
			) );
			//nivo slider description
			$nivoslider = new_cmb2_box( array(
				'id'           		 => $prefix . '_moto_nivo_desc',
				'title'        		 => esc_html__( 'Description', 'moto' ),
				'object_types'       => array( 'nivo_slider'), // Post type
			) );
			//nivo slider description
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Description', 'moto' ),
				'id'                 => $prefix .'nivo_description',
				'desc'               => esc_html__( 'Description', 'moto' ),
				'type'               => 'textarea_small',
			) );
			//nivo slider description color
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Description Text Color', 'moto' ),
				'id'                 => $prefix .'nivo_description_color',
				'default'            => ' #24273e',
				'desc'               => esc_html__( 'Top text color', 'moto' ),
				'type'               => 'colorpicker',
			) );
			//nivo slider description font size
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Font Size', 'moto' ),
				'id'                 => $prefix .'nivo_desc_font_size',
				'desc'               => esc_html__( 'Bottom title font size', 'moto' ),
				'type'               => 'text_small',
				'default'            => '14',
			) );
			//nivo slider description animation
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Animation', 'moto' ),
				'id'                 => $prefix .'nivo_desc_animation',
				'desc'               => esc_html__( 'Select text animation', 'moto' ),
				'type'               => 'select',
				'options'            => array(
					'bounceOut'      => esc_html__('bounceOut','moto'),
					'bounceOutDown'  => esc_html__('bounceOutDown','moto'),
					'bounceOutLeft'  => esc_html__('bounceOutLeft','moto'),
					'bounceOutRight' => esc_html__('bounceOutRight','moto'),
					'bounceOutUp'    => esc_html__('bounceOutUp','moto'),
					'fadeIn'         => esc_html__('fadeIn','moto'),
					'fadeInDown'     => esc_html__('fadeInDown','moto'),
					'fadeInDownBig'  => esc_html__('fadeInDownBig','moto'),
					'fadeInLeft'     => esc_html__('fadeInLeft','moto'),
					'fadeInLeftBig'  => esc_html__('fadeInLeftBig','moto'),
					'fadeInRight'    => esc_html__('fadeInRight','moto'),
					'fadeInRightBig' => esc_html__('fadeInRightBig','moto'),
					'fadeInUp'       => esc_html__('fadeInUp','moto'),
					'fadeInUpBig'    => esc_html__('fadeInUpBig','moto'),
					'bounceIn'       => esc_html__('bounceIn','moto'),
					'bounceInDown'   => esc_html__('bounceInDown','moto'),
					'bounceInLeft'   => esc_html__('bounceInLeft','moto'),
					'bounceInRight'  => esc_html__('bounceInRight','moto'),
					'bounceInUp'     => esc_html__('bounceInUp','moto'),
				),
				'default'            => 'bounceInRight',
			) );
			//nivo slider button
			$nivoslider = new_cmb2_box( array(
				'id'           		 => $prefix . '_moto_nivo_button',
				'title'        		 => esc_html__( 'Slider Button', 'moto' ),
				'object_types' 		 => array( 'nivo_slider'), // Post type
			) );
			//nivo slider button text
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Button Text', 'moto' ),
				'id'                 => $prefix .'nivo_button_text',
				'desc'               => esc_html__( 'Bottom text', 'moto' ),
				'type'               => 'text',
			) );
		
			//nivo slider button link
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Button Link', 'moto' ),
				'id'                 => $prefix .'nivo_button_link',
				'desc'               => esc_html__( 'Bottom text link', 'moto' ),
				'type'               => 'text_url',
			) );
		
			//nivo slider button text color
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Button Text Color', 'moto' ),
				'id'                 => $prefix .'nivo_button_text_color',
				'default'            => ' #fff',
				'desc'               => esc_html__( 'Button text color', 'moto' ),
				'type'               => 'colorpicker',
			) );
			//nivo slider button bg color
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Button Background Color', 'moto' ),
				'id'                 => $prefix .'nivo_button_bg_color',
				'desc'               => esc_html__( 'Button background color', 'moto' ),
				'type'               => 'colorpicker',
			) );
			//nivo slider button bg color
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Button Background Hover Color', 'moto' ),
				'id'                 => $prefix .'nivo_button_bg_hover',
				'desc'               => esc_html__( 'Button background hover color', 'moto' ),
				'default'            => '#ffae00 ',
				'type'               => 'colorpicker',
			) );
						
			//nivo slider button animation
			$nivoslider->add_field( array(
				'name'               => esc_html__( 'Button Animation', 'moto' ),
				'id'                 => $prefix .'nivo_button_animation',
				'desc'               => esc_html__( 'Select button animation', 'moto' ),
				'type'               => 'select',
				'options'            => array(
					'bounceOut'      => esc_html__('bounceOut','moto'),
					'bounceOutDown'  => esc_html__('bounceOutDown','moto'),
					'bounceOutLeft'  => esc_html__('bounceOutLeft','moto'),
					'bounceOutRight' => esc_html__('bounceOutRight','moto'),
					'bounceOutUp'    => esc_html__('bounceOutUp','moto'),
					'fadeIn'         => esc_html__('fadeIn','moto'),
					'fadeInDown'     => esc_html__('fadeInDown','moto'),
					'fadeInDownBig'  => esc_html__('fadeInDownBig','moto'),
					'fadeInLeft'     => esc_html__('fadeInLeft','moto'),
					'fadeInLeftBig'  => esc_html__('fadeInLeftBig','moto'),
					'fadeInRight'    => esc_html__('fadeInRight','moto'),
					'fadeInRightBig' => esc_html__('fadeInRightBig','moto'),
					'fadeInUp'       => esc_html__('fadeInUp','moto'),
					'fadeInUpBig'    => esc_html__('fadeInUpBig','moto'),
					'bounceIn'       => esc_html__('bounceIn','moto'),
					'bounceInDown'   => esc_html__('bounceInDown','moto'),
					'bounceInLeft'   => esc_html__('bounceInLeft','moto'),
					'bounceInRight'  => esc_html__('bounceInRight','moto'),
					'bounceInUp'     => esc_html__('bounceInUp','moto'),
				),
				'default'            => 'bounceInRight',
			) );
				
			//nivo slider  aditional 
			$nivoslider = new_cmb2_box( array(
				'id'           		 => $prefix . '_moto_nivo_aditional',
				'title'        		 => esc_html__( 'Aditional Fields', 'moto' ),
				'object_types' 		 => array( 'nivo_slider'), // Post type
			) );
			
			$nivoslider->add_field( array(
				'id'   				 => $prefix . 'nivo_prev_next',
				'type'  			 => 'checkbox',
				'name' 				 => esc_html__('Enable Direction Navigation','moto'),
				'desc' 				 => esc_html__('Prev & Next arrows','moto'),
			));
			$nivoslider->add_field( array(
				'id'   				 => $prefix . 'nivo_control_navigation',
				'type'   			 => 'checkbox',
				'name' 				 => esc_html__('Enable Control Navigation','moto'),
				'desc' 				 => esc_html__('eg 1,2,3...','moto'),
			));

			$nivoslider->add_field( array(
				'id'   				 => $prefix . 'nivo_control_width',
				'type'   			 => 'text_small',
				'name' 				 => esc_html__('Width Control','moto'),
			));

			$nivoslider->add_field( array(
				'id'   				 => $prefix . 'nivo_control_height',
				'type'   			 => 'text_small',
				'name' 				 => esc_html__('Height Control','moto'),
			));
			
			
	}
}


