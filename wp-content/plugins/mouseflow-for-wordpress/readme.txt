=== Mouseflow for Wordpress ===
Contributors: Mouseflow
Tags: mouseflow, heatmaps, webanalytics, reporting, insights, recordings, funnel, conversion
Requires at least: 2.0.0
Tested up to: 5.3
Stable tag: 5.3



== Description ==
### Mouseflow: The easy way to improve usability and increase conversions.	

Here at Mouseflow, we believe that everyone should have the tools to build great websites.

Mouseflow provides everything your team needs to understand your customers, gain actionable insights - and act on these insights to improve usability and increase your conversion rate.


#### Improve Usability and Conversions
- Create a great website that makes it easy for visitors to convert to customers
- Identify usability issues that prevent users from converting
- Get a better understanding of your users: Watch recordings and see precisely why they are struggling and what to do about it


#### Measure Your Marketing
- Measure your users’ engagement with your content
- Easily create feedback campaigns to learn what your customers need or want


#### Identify and Track Errors
- Quickly identify code-related errors on your website so you can react immediately
- See what the error is instead of having to rely on third-party descriptions


#### Improve Internal Communication and Decision Making
- Spot trends easily with heatmaps and use them as visual aids for your team
- Collect evidence to support decisions, based on a mix of quantitative and qualitative data




###The Mouseflow Toolbox###
At Mouseflow we have extensive experience delivering high definition Wordpress Heatmaps and [Conversion Optimization tools specifically for Wordpress users](https://mouseflow.com/integrations/wordpress/).

### Heatmaps and Session Replay
#### Understand your users and gain insights.
Knowing your customers’ behaviors is critical for a successful online business. Heatmaps and Session Recordings will show you exactly where users scroll to, what they click on, and where they focus. On top of this, our Machine Learning Algorithms will show you exactly where there are user experience issues and programming errors on your site, allowing you to react before they become an issue for your visitors.


### Funnels and Form Analytics
#### Discover where visitors are dropping out of your funnel.
You can’t improve what you don’t measure. With our funnel and form analytics, you get a clear vision of how many visitors turn into customers - and more importantly, it shows you precisely where visitors give up before they convert. Armed with this knowledge, you can drastically increase your conversions by improving your funnels and forms.


### User Feedback
#### Let your customers tell you what's wrong.
Get even closer to your customers and what they think of your website by using our interactive surveys. Easy to implement and extremely customizable. You can also set our survey to appear when the customer experiences an error on your site, allowing you to get instant feedback on problems and issues.


== Installation ==

Please follow these steps to use the plugin:
  <enter>
1. Open up your WordPress CMS by going to yourWebPage/wp-login.php and logging in
2. Click on "Plugins" in the menu on the left hand side
3. Click on "Add New" just below the "Plugins" link
4. Search for "Mouseflow" in the search bar in the upper right hand corner
5. Click the "Install Now" button next to the "Mouseflow for WordPress" tile
6. Once the "Mouseflow for WordPress" plugin has been installed click the blue "activate" button
7. Scroll down the list of plugins and click "settings" link under "Mouseflow for WordPress"
8. Click the "Insert Tracking Code" tile
9. Insert the following snippet of code in the available field under "Insert Tracking code":
10. Click “Save Changes”
  <enter>
Mouseflow is now installed and will start tracking as soon as the next customer visits your site.
  <enter>
Having trouble? You can find our [full installation guide here](https://support.mouseflow.com/support/solutions/articles/44001539575-wordpress-integration).

== Screenshots ==
1. The Mouseflow Dashboard gives you an easy overview of your traffic
2. Mouseflow Funnels is the intuitive way to track your conversions
3. Geo heatmaps shows you exactly where your visitors are from - it’s a great tool for remarketing campaigns


== Frequently Asked Questions ==
	
= Installing Mouseflow =

Once you have installed our Mouseflow for WordPress plugin please do as follows:
  <enter>
1. Open up your WordPress CMS by going to yourWebPage/wp-login.php and logging in
2. Click on "Plugins" in the menu on the left hand side
3. Click on "Add New" just below the "Plugins" link
4. Search for "Mouseflow" in the search bar in the upper right hand corner
5. Click the "Install Now" button next to the "Mouseflow for WordPress" tile
6. Once the "Mouseflow for WordPress" plugin has been installed click the blue "activate" button
7. Scroll down the list of plugins and click "settings" link under "Mouseflow for WordPress"
8. Click the "Insert Tracking Code" tile
9. Insert the following snippet of code in the available field under "Insert Tracking code":
10. Click “Save Changes”
  <enter>
Mouseflow is now installed and will start tracking as soon as the next customer visits your site.
  <enter>
Having trouble? You can find our [full installation guide here](https://support.mouseflow.com/support/solutions/articles/44001539575-wordpress-integration).

= Where do I get the tracking code? =

You get the tracking code by signing up on [mouseflow.com](https://mouseflow.com) You can easily create an account for free by following this link: [mouseflow.com/sign-up/](https://mouseflow.com/sign-up/)

= Getting the most out of Mouseflow =

Are you wondering how you can get the most out of Mouseflow? Our [training library](https://mouseflow.com/training/) will make you a Mouseflow expert in no time.

= Is Mouseflow right for me? =

Of course! Mouseflow has helped more than 125,000 customers, ranging from bloggers and owners of small e-commerce shops to large corporations and digital agencies to improve their own and their clients' websites. We have extensive experience delivering high definition Wordpress Heatmaps and conversion optimisation tools specifically for Wordpress users: https://mouseflow.com/integrations/wordpress/

= How about security? Is my data safe? =

Mouseflow takes security very seriously. We only use the most trusted and secure data centres for our platform. Access is restricted to a few select employees and data from SSL-encrypted pages are only transmitted across encrypted SSL connections. [Read all about Mouseflow security here](https://support.mouseflow.com/support/solutions/articles/44001550671-how-about-security-is-my-data-safe-).

= Is Mouseflow GDPR and CCPA compliant? =

Yes, Mouseflow is fully compliant with both GDPR and CCPA. You can read all about it here: https://mouseflow.com/gdpr/

= Does Mouseflow affect the browsing experience? =

Mouseflow does **not** affect a user's browsing experience. If you are curious about the technical details you can [learn all about it here ](https://support.mouseflow.com/support/solutions/articles/44001303656-does-mouseflow-affect-the-browsing-experience-)


= Issues with tracking =

1. Make sure you've inserted the script code in the settings page.
2. Check that you have the wp_footer() function in the blog template.
3. Check your blog's html source (Page / View source) and search for "mouseflow". In case the script found at the end of the <body> section, but still not working, you're probably running the site from a different domain than the one you entered on your mouseflow account. Make sure that the domains are matching.
4. Read more here: [Recording Status is 'Not installed'](https://support.mouseflow.com/support/solutions/articles/44001496492-recording-status-is-not-installed-).
5. Get in touch: [Mouseflow Support](http://mouseflow.com/support/)