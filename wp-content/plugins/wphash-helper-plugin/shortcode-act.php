<?php
//define
define( 'SPLG_URL', plugins_url() );
define( 'SPLG_DIR', dirname( __FILE__ ) );

// Call shortcode file
include_once(SPLG_DIR. '/include/shortcode/video-popup.php');
include_once(SPLG_DIR. '/include/shortcode/features-box.php');
include_once(SPLG_DIR. '/include/shortcode/apps-screenshot.php');
include_once(SPLG_DIR. '/include/shortcode/map.php');
include_once(SPLG_DIR. '/include/shortcode/animation-text.php');
include_once(SPLG_DIR. '/include/shortcode/moto-slider.php');
include_once(SPLG_DIR. '/include/shortcode/single-blog.php');
include_once(SPLG_DIR. '/include/shortcode/pricing-table.php');
include_once(SPLG_DIR. '/include/shortcode/team.php');

