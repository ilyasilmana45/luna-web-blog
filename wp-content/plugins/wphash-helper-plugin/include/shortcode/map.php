<?php 
/*
 * Moto Google Map ShortCode
 * Author: Hastech
 * Author URI: http://hastech.company
 * Version: 1.0.0
 * ======================================================
 * 
/**
 * =======================================================
 *    KC Shortcode Map
 * =======================================================
 */

add_action('init', 'moto_google_map'); // Call kc_add_map function ///
if(!function_exists('moto_google_map')):
	function moto_google_map(){
		if(function_exists('kc_add_map')): // if kingComposer is active
		kc_add_map(
		    array(
		        'moto_google_map'  => array( // <-- shortcode tag name
		            'name'        => esc_html__('Moto Google Map',  'moto'),
		            'description' => esc_html__('Show google maps with your style',  'moto'),
		            'icon'        => 'fa-map',
		            'category'    => esc_html__('Moto', 'moto'),
		            'params'      => array(
		        
			         	'General' => array(
			         		array(
								'name'        => 'map_random_id',
								'label'       => '',
								'type'        => 'random',
								'description' => '',
							),
							array(
								'type'		=> 'text',
								'name'		=> 'map_api_key',
								'label'		=> esc_html__( 'Google Maps API Key', 'moto' ),
								'description'   => esc_html__('Enter your Google Maps API Key. e.g. AIzaSyBMlLa3XrAmtemtf97Z2YpXwPLlimRK7Pk', 'moto'),
								'value'	=> 'AIzaSyBMlLa3XrAmtemtf97Z2YpXwPLlimRK7Pk',
						
							),
							array(
								'type'		=> 'text',
								'name'		=> 'map_latitude_longitude',
								'label'		=> esc_html__( 'Google Maps Latitude & Longitude', 'moto' ),
								'description'   => esc_html__('Google Maps Latitude & Longitude, e.g. 40.6700, -73.9400', 'moto'),
								'value'	=> '40.6700, -73.9400',
								
							),
							array(
								'type'		=> 'text',
								'name'		=> 'map_height',
								'label'		=> esc_html__( 'Map Height (px)', 'moto' ),
								'value'		=> 450
							),
							array(
								'name'          => 'disable_mouse_scroll',
								'label'         => esc_html__(' Disable Wheel Zoom', 'moto'),
								'type'          => 'toggle',
								'description'   => esc_html__(' Disable the zoom action when mouse wheel focus on maps.', 'moto')
							),
							array(
								'name' => 'map_marker',
								'label' => esc_html__(' Map Marker', 'moto'),
								'type' => 'attach_image', 
								'description' => esc_html__(' Upload your map marker', 'moto'),
							),
							array(
								'type'			=> 'textarea',
								'label'			=> esc_html__( 'Map Style', 'moto' ),
								'name'			=> 'map_style',
								'value' => base64_encode('[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]'),

								'description'	=> __( 'Go to <a href="https://snazzymaps.com/" target=_blank>Snazzy Maps</a> and Choose/Customize your Map Style. Click on your demo and copy JavaScript Style Array', 'moto' )
							),
							array(
								'type'			     => 'toggle',
								'label'			     => esc_html__( 'Show overlap contact form', 'moto' ),
								'name'			     => 'show_stcf',
								'description'	     => esc_html__( 'Enable a contact form above the maps', 'moto' )
							),
							array(
								'type'		=> 'text',
								'name'		=> 'moto_contact_form_title',
								'label'		=> esc_html__( 'Contact Form Title', 'moto' ),
								'value'		=> esc_html__( 'Get in Touch', 'moto' ),
								'relation'		     => array(
									'parent'         => 'show_stcf',
									'show_when'      => 'yes'
								)
							),
							array(
								'type'			     => 'textarea',
								'label'			     => esc_html__( 'Contact form shortcode', 'moto' ),
								'name'			     => 'moto_contact_form_sc',
								'description'	     => esc_html__( 'Shortcode content which generated by contact form 7. For example: [contact-form-7 id="4" title="Contact form 1"]', 'moto' ),
								'value'		=> base64_encode( '[contact-form-7 id="4" title="Contact form 1"]'),
								'relation'		     => array(
									'parent'         => 'show_stcf',
									'show_when'      => 'yes'
								)
							),

							array(
								'name'		=> 'css_contact_style',
								'type'		=> 'css',
								'options'	=> array(
									array(
										"screens" => "any,1024,999,767,479",
										'Title'   => array(
												array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Title Color', 'moto'),
		                    					    'selector' => '+ .contact-form-map-area .contact-title' 
		                    					),
		                    					array(
		                    						'property' => 'font-family', 
		                    						'label'    => esc_html__('Title Font Family',  'moto'), 
		                    						'selector' => '+ .contact-form-map-area .contact-title'
		                    					),
		                    					array( 
		                    						'property' => 'font-size', 
		                    						'label'    => esc_html__('Title Font Size', 'moto'),
		                    						'selector' => '+ .contact-form-map-area .contact-title' 
		                    					),
		                    					array(
		                    						'property' => 'font-weight', 
		                    						'label'    => esc_html__('Title Font Weight', 'moto'),
		                    						'selector' => '+  .contact-form-map-area .contact-title'
		                    					),
		                    					array(
													'property' => 'text-transform', 
													'label'    => esc_html__('Title Text Transform', 'moto'),
													'selector' => '+  .contact-form-map-area .contact-title'
												),
												array(
													'property'  => 'padding', 
													'label' 	=> esc_html__('Padding', 'moto'),
													'selector'  => '+ .contact-form-map-area .contact-title'
												),
												array(
													'property'  => 'margin', 
													'label' 	=> esc_html__('Margin', 'moto'),
													'selector'  => '+ .contact-form-map-area .contact-title'
												),
		                    					
		                    				),

										'Contact Form' => array(
											array('property' => 'width', 'label' => 'Width Wrap', 'value' => '40%', 'selector' => '.contact-form'),
											array('property' => 'color', 'label' => 'Input Color', 'value' => '#666666', 'selector' => '+ #contact-form input:not(.raees-submit-btn), #contact-form textarea, #contact-form input::-moz-placeholder, #contact-form textarea::-moz-placeholder, #contact-form input::-webkit-input-placeholder, #contact-form textarea::-webkit-input-placeholder'),
											array('property' => 'border-color', 'label' => 'Input border Color', 'value' => '#c1c1c1', 'selector' => '+ #contact-form input, #contact-form textarea'),
											array('property' => 'background', 'selector' => '.contact-form-map-area .contact-form'),
											array('property' => 'padding', 'label' => 'Padding Wrap', 'value' => '55px 30px 40px 30px', 'selector' => '.contact-form'),
											array('property' => 'margin', 'label' => 'Margin Wrap', 'selector' => '.contact-form'),
										),

										'Button Style' => array(
											array('property' => 'color', 'label' => 'Color', 'value' => '#fefefe', 'selector' => '#contact-form .wpcf7-form .raees-submit-btn.wpcf7-form-control.wpcf7-submit'),
											array('property' => 'border', 'label' => 'Border', 'selector' => '#contact-form .wpcf7-form .raees-submit-btn.wpcf7-form-control.wpcf7-submit'),
											array('property' => 'background', 'value' => '#00d379',  'selector' => '#contact-form .wpcf7-form .raees-submit-btn.wpcf7-form-control.wpcf7-submit')
										),

										'Button Hover Style' => array(
											array('property' => 'color', 'label' => 'Color', 'value' => '#FFFFFF', 'selector' => '#contact-form .wpcf7-form .raees-submit-btn.wpcf7-form-control.wpcf7-submit:hover'),
											array('property' => 'border', 'label' => 'Border', 'selector' => '#contact-form .wpcf7-form .raees-submit-btn.wpcf7-form-control.wpcf7-submit:hover'),
											array('property' => 'background', 'value' => '#00d379', 'selector' => '#contact-form .wpcf7-form .raees-submit-btn.wpcf7-form-control.wpcf7-submit:hover')
										)
										
									)
								),
								'relation'		     => array(
									'parent'         => 'show_stcf',
									'show_when'      => 'yes'
								)
							),


			                array(
			                    'name'        => 'custom_css_class',
			                    'label'       => esc_html__('CSS Class', 'moto'),
			                    'description' => esc_html__('Custom css class for css customisation', 'moto'),
			                    'type'        => 'text'
			                ),
			         	), // content
	                    

		            )// Params
		        )// end shortcode key
		    )// first array
		); // End add map
		endif;
	}
endif;


 /*
 * =======================================================
 *    Register Shortcode   
 * =======================================================
 */


 if(!function_exists('moto_google_map_shortcode')){
	function moto_google_map_shortcode($atts,$content){
	ob_start();
			$moto_google_map = shortcode_atts( array(
				'map_random_id'       		=> '',
				'map_api_key'       		=> '',
				'map_latitude_longitude'    => '',
				'map_height'       			=> '',
				'disable_mouse_scroll'      => '',
				'map_marker'       			=> '',
				'map_style'       			=> '',
				'show_stcf'       			=> '',
				'moto_contact_form_sc'    => '',
				'moto_contact_form_title' => '',
				'custom_css'       			=> '',
				'custom_css_class' 			=> '',
			),$atts); 
			extract( $moto_google_map );

		$wrap_class  = apply_filters( 'kc-el-class', $atts );
		if( !empty( $custom_class ) ):
			$wrap_class[] = $custom_class;
		endif;
		$extra_class =  implode( ' ', $wrap_class );



		$map_marker_img = wp_get_attachment_image_src( $map_marker, 'full');
		$map_marker_img_url = $map_marker_img[0];





	?>
	<div class="<?php echo esc_attr( $extra_class ); ?> <?php echo esc_attr( $custom_css_class ); ?>">

		<div class="contact-form-map-area">
			<!-- Google Map Start -->
	        <div class="google-map-area">
	            <!--  Map Section -->
	            <div id="contacts" class="map-area">
	                <div id="googleMap-<?php echo esc_attr( $map_random_id ); ?>" style="width:100%;height:<?php echo esc_attr( $map_height ); ?>px;"></div>
	            </div>
	        </div>
	        <!-- Google Map End -->
	        <!-- Contact Form Container Start -->
			<?php if (isset($show_stcf) && 'yes' == $show_stcf): ?>
				<div class="container">
	                <div class="contact-form">
	                    <h2 class="contact-title"><?php echo esc_html( $moto_contact_form_title ); ?></h2>
	                	<div id="contact-form">
	                		
							<?php echo do_shortcode( $moto_contact_form_sc ) ?>

	                	</div>
	                </div>
			    </div>
			<?php endif ?>
	        <!-- Contact Form Container End -->
		</div>
     
	</div>


	<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo esc_js( $map_api_key ); ?>"></script>
	<script>

		$disable_mouse_scroll = <?php echo esc_js( $disable_mouse_scroll ) == 'yes' ? 'true': 'false'; ?>;

        function init() {
            var mapOptions = {
                zoom: 11,
                scrollwheel: $disable_mouse_scroll,
                center: new google.maps.LatLng(<?php echo esc_js( $map_latitude_longitude ); ?>),
                styles: <?php echo $map_style ?>
            };
            var mapElement = document.getElementById('googleMap-<?php echo esc_js( $map_random_id); ?>');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(<?php echo esc_js( $map_latitude_longitude ); ?>),
                map: map,
                icon: '<?php echo esc_js( $map_marker_img_url ); ?>',
                title: 'Snazzy!'
            });
        }
        google.maps.event.addDomListener(window, 'load', init);
	</script>



	<?php
		return ob_get_clean();
	}
	add_shortcode('moto_google_map' ,'moto_google_map_shortcode');
}