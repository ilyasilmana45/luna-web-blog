<?php 
/*
 * Moto video ShortCode
 * Author: Hastech
 * Author URI: http://hastech.company
 * Version: 1.0.0
 * ======================================================
 * 
/**
 * =======================================================
 *    KC Shortcode Map
 * ======================================================= */
 add_action('init', 'moto_video_sections'); // Call kc_add_map function ///
if(!function_exists('moto_video_sections')):
	function moto_video_sections(){
		if(function_exists('kc_add_map')): // if kingComposer is active
		kc_add_map(
		    array(
		        'moto_video'  => array( // <-- shortcode tag name
		            'name'        => esc_html__('Video', 'moto'),
		            'description' => esc_html__('Description Here', 'moto'),
		            'icon'        => 'fa-header',
		            'category'    => 'Moto',
		            'params'      => array(
		        // .............................................
		        // ..... // Content TAB
		        // .............................................
		         	'General' => array(
						array(
							'type' 			=> 'icon_picker',
							'name'		 	=> 'icon',
							'label' 		=> esc_html__( 'Icon', 'moto' ),
							'value'			=> 'fa-play',
							'description' 	=> esc_html__( 'Select icon display on front side', 'moto' ),
						),
						array(
							'name'        	=> 'title',
							'label'      	=> esc_html__('Title','moto'),
							'type'       	=> 'text',
							'admin_label'	=> true,
							'value'       	=> 'watch our featured video'
						),
						array(
							'name'			=> 'button_link',
							'label'			=> esc_html__( 'Link Button', 'moto' ),
							'type'			=> 'link',
							'description'	=> esc_html__( 'Set read more text displayed on the button.', 'moto' ),
						),
		                array(
		                    'name'          => 'custom_css_class',
		                    'label'         => esc_html__('CSS Class','moto'),
		                    'description'   => esc_html__('Custom css class for css customisation','moto'),
		                    'type'          => 'text'
		                )
						
		         	), // content
		        // .............................................
		        // ..... // Styling
		        // .............................................
		                    'styling' => array(
		                    	array(
		                    		'name'   => 'custom_css',
		                    		'type'   => 'css',
		                    		'options' => array(
		                    			array(
		                    				'screens' => 'any,1024,999,767,479',
		                    				'Title'   => array(
		                    					array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Color',  'moto'),
		                    					    'selector' => '+ .watch-click-video' 
		                    					),
		                    					array(
		                    						'property' => 'font-family', 
		                    						'label'    => esc_html__('Font Family', 'moto'),
		                    						'selector' => '+  .watch-click-video'
		                    					),
		                    					array( 
		                    						'property' => 'font-size', 
		                    						'label'    => esc_html__('Font Size','moto'), 
		                    						'selector' => '+ .watch-click-video' 
		                    					),
		                    					array( 
		                    						'property' => 'line-height', 
		                    						'label'    => esc_html__('Line Height','moto'), 
		                    						'selector' => '+ .watch-click-video' 
		                    					),
		                    					array(
		                    						'property' => 'font-weight', 
		                    						'label'    => esc_html__('Font Weight', 'moto'),
		                    						'selector' => '+  .watch-click-video'
		                    					),
		                    					array(
													'property' => 'text-transform', 
													'label'    => esc_html__('Text Transform', 'moto'), 
													'selector' => '+  .watch-click-video'
												),
												array(
		                    						'property' => 'text-align', 
		                    						'label'    => esc_html__('Text Align', 'moto'), 
		                    						'selector' => '+ .watch-click-video'
		                    					),
		                    				),
											/// Icon
											'Icon' => array(
												array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Color','moto'),  
		                    					    'selector' => '+ .watch-click-video i' 
		                    					),
		                    					array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Hover Color','moto'),  
		                    					    'selector' => '+ .watch-click-video i' 
		                    					),
		                    					array( 
		                    					    'property' => 'background-color', 
		                    					    'label'    => esc_html__('Background', 'moto'), 
		                    					    'selector' => '+ .watch-click-video i' 
		                    					),
		                    					array( 
		                    					    'property' => 'background-color', 
		                    					    'label'    => esc_html__('Hover Background','moto'),  
		                    					    'selector' => '+ .watch-click-video i:hover' 
		                    					),
												array( 
		                    						'property' => 'font-size', 
		                    						'label'    => esc_html__('Font Size','moto'), 
		                    						'selector' => '+ .watch-click-video i' 
		                    					),
												array(
													'property'  => 'padding', 
													'label' 	=> esc_html__('Padding', 'moto'), 
													'selector'  => '+ .watch-click-video i'
												),
												array(
													'property'  => 'margin', 
													'label' 	=> esc_html__('Margin', 'moto'), 
													'selector'  => '+ .watch-click-video i'
												),
											),
											/// Button Box
											'Button Box' =>array(
												array( 
		                    					    'property' => 'background-color', 
		                    					    'label'    => esc_html__('Button Background', 'moto'), 
		                    					    'selector' => '+ .watch-click-video' 
		                    					),
												array( 
		                    					    'property' => 'background-color', 
		                    					    'label'    => esc_html__('Button Hover Background', 'moto'), 
		                    					    'selector' => '+ .watch-click-video:hover' 
		                    					),
												array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Hover Text Color', 'moto'), 
		                    					    'selector' => '+ .single-video-button .video-popup:hover' 
		                    					),
		                    					array(
		                    						'property' => 'box-shadow', 
		                    						'label'    => esc_html__('Button Box Shadow', 'moto'),
		                    						'selector' => '+  .watch-click-video'
		                    					),
												array(
		                    						'property' => 'border-radius', 
		                    						'label'    => esc_html__('Button Box Radius', 'moto'),
		                    						'selector' => '+  .watch-click-video'
		                    					),
												array(
		                    						'property' => 'border', 
		                    						'label'    => esc_html__('Button Box Border', 'moto'),
		                    						'selector' => '+  .watch-click-video'
		                    					),
												array(
													'property'  => 'padding', 
													'label' 	=> esc_html__('Button Box Padding', 'moto'), 
													'selector'  => '+ .watch-click-video'
												),
												array(
													'property'  => 'margin', 
													'label' 	=> esc_html__('Button Box Margin', 'moto'), 
													'selector'  => '+ .watch-click-video'
												),
											),
		                    			)
		                    		) //End of options
		                    	)
		                    ), //End of styling
                            'animate' => array(
								array(
									'name'    => 'animate',
									'type'    => 'animate'
								)
							), //End of animate
		        // .............................................
		        // .............................................
		        // .............................................
		        /////////////////////////////////////////////////////////
		            )// Params
		        )// end shortcode key
		    )// first array
		); // End add map
		endif;
	}
endif;

 
 
 /*
 * =======================================================
 *    Register Brand Shortcode   
 * =======================================================
 */
 //[moto_brand  button_link="" image="" custom_css_class=""]
 if(!function_exists('moto_video_shortcode')){
	function moto_video_shortcode( $atts, $content){
	ob_start();
			$video = shortcode_atts(array(
		   'icon'       	  =>'',
		   'button_link'      =>'',
		   'title'       	  =>'',
		   'custom_css'       =>'',
		   'custom_css_class' =>'',
			),$atts); 
			extract( $video );
		//custom class		
		$wrap_class  = apply_filters( 'kc-el-class', $atts );
		if( !empty( $custom_class ) ):
			$wrap_class[] = $custom_class;
		endif;
		$extra_class =  implode( ' ', $wrap_class );
		
		if ( !empty( $button_link ) ) {
			$link_arr = explode( '|', $button_link );
			if ( !empty( $link_arr[0] ) ) {
				$link_url = $link_arr[0];
			} else {
				$link_url = 'https://www.youtube.com/watch?v=8uZ98Pa3bPE';
			}
		} else {
			$link_url = 'https://www.youtube.com/watch?v=8uZ98Pa3bPE';
		}
	?>
	<div class="<?php echo esc_attr( $extra_class ); ?> <?php echo esc_attr( $custom_css_class ); ?>">
		<div class="single-video-button text-left">
			<a class="video-popup watch-click-video hero-btn" href="<?php echo esc_url( $link_url ); ?>">
				<i class="<?php echo esc_attr( $icon ); ?>"></i><?php echo esc_html( $title ); ?>
			</a>
			
		</div>
	</div>
	<?php
	
		return ob_get_clean();
	}
	add_shortcode('moto_video' ,'moto_video_shortcode');
}
 