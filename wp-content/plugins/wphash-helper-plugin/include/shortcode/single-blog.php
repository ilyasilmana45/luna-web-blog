<?php 
/*
  * Moto home Blog Posts Shortcode 
  * Author: Hastech
  * Author URI: http://hastech company
  * Version: 1.0.0
  *
  *
  * =======================================================
  *    Register Blog Posts shortcode
  * =======================================================
*/

add_action('init', 'moto_blog_posts_maping_func'); // Call kc_add_map function ///
if(!function_exists('moto_blog_posts_maping_func')){
	function moto_blog_posts_maping_func(){

		if(function_exists('kc_add_map')){
			kc_add_map(
			    array(
			        'moto_blog_posts'  => array( // <-- shortcode tag name
			            'name'        => esc_html__('Blog Posts', 'moto'),
			            'description' => esc_html__('Blog Posts Section', 'moto'),
			            'icon'        => 'fa-header',
						'category'    => esc_html__('Moto', 'moto'),
			            'params'      => array(
						
			        		// Content TAB
				         	'General' => array(
								array(
									'name' => 'item_limit',
									'description' => __('Specify number of post that you want to show. Set 0 to get all post', 'moto'),
									'type' => 'number_slider',
									'value' => 3,
									'options' => array(
										'min' => 0,
										'max' => 15,
										'show_input' => true
									),
									'admin_label' => true,
								),
								array(
									'name' => 'limit_content',
									'description' => __('Content Limit', 'moto'),
									'type' => 'number_slider',
									'value' => 18,
									'options' => array(
										'min' => 0,
										'max' => 200,
										'show_input' => true
									),
									'admin_label' => true,
								),
								array(
									'name' => 'show_date',
									'label' => 'Show Date ?',
									'type' => 'toggle', 
									'value' => 'yes', 
									'description' => 'This option will show/hide Date on the post top of image',
								),
								array(
									'name' => 'show_author',
									'label' => 'Show Author ?',
									'type' => 'toggle', 
									'value' => 'yes', 
									'description' => 'This option will show/hide Date on the post top of image',
								),
								array(
									'name' => 'show_comment',
									'label' => 'Show Comments ?',
									'type' => 'toggle', 
									'value' => 'yes', 
									'description' => 'This option will show/hide Date on the post top of image',
								),
				                array(
				                    'name'        => 'custom_css_class',
				                    'label'       => esc_html__( 'CSS Class', 'moto' ),
				                    'description' => esc_html__( 'Custom CSS class for css customisation', 'moto' ),
				                    'type'        => 'text'
				                ),
				         	), // content

							// styling tab
							'styling' => array(
								array(
									'name'    => 'custom_css',
									'type'    => 'css',
									'options' => array(
										array(
											'screens' => 'any,1024,999,767,479',
											'Title'   => array(
												array( 
													'property' => 'color', 
													'label'    => 'Color', 
													'selector' => '+ .single-post-title' 
												),
												array( 
													'property' => 'color', 
													'label'    => 'Hover Color', 
													'selector' => '+ .single-post-title:hover' 
												),
												array(
													'property' => 'font-family', 
													'label'    => 'Font Family', 
													'selector' => '+ .single-post-title'
												),
												array( 
													'property' => 'font-size', 
													'label'    => 'Font Size', 
													'selector' => '+ .single-post-title' 
												),
												array(
													'property' => 'font-weight', 
													'label'    => 'Font Weight', 
													'selector' => '+ .single-post-title'
												),
												array(
													'property' => 'text-transform', 
													'label'    => 'Text Transform', 
													'selector' => '+ .single-post-title'
												),
											),
											'Content'   => array(
												array( 
													'property' => 'color', 
													'label'    => 'Color', 
													'selector' => '+ .single-blog-content p' 
												),
												array(
													'property' => 'font-family', 
													'label'    => 'Font Family', 
													'selector' => '+ .single-blog-content p'
												),
												array( 
													'property' => 'font-size', 
													'label'    => 'Font Size', 
													'selector' => '+ .single-blog-content p' 
												),
												array(
													'property' => 'font-weight', 
													'label'    => 'Font Weight', 
													'selector' => '+ .single-blog-content p'
												),
												array(
													'property' => 'text-transform', 
													'label'    => 'Text Transform', 
													'selector' => '+ .single-blog-content p'
												),
											),
											'Meta Style'   => array(
												array( 
													'property' => 'color', 
													'label'    => 'Date Color', 
													'selector' => '+ .single-blog-content p' 
												),
												array( 
													'property' => 'color', 
													'label'    => 'Hover Color', 
													'selector' => '+ .single-blog-meta a:hover' 
												),
												array( 
													'property' => 'background', 
													'label'    => 'Date Background Color', 
													'selector' => '+ .single-blog-content p' 
												),
												
											),
											'Button'   => array(
												array( 
													'property' => 'color', 
													'label'    => 'Color', 
													'selector' => '+ .single-read-more a' 
												),
												array( 
													'property' => 'color', 
													'label'    => 'Hover Color', 
													'selector' => '+ .single-read-more a:hover' 
												),
												array(
													'property' => 'font-family', 
													'label'    => 'Font Family', 
													'selector' => '+ .single-read-more a'
												),
												array( 
													'property' => 'font-size', 
													'label'    => 'Font Size', 
													'selector' => '+ .single-read-more a' 
												),
												array(
													'property' => 'font-weight', 
													'label'    => 'Font Weight', 
													'selector' => '+ .single-read-more a'
												),
												array(
													'property' => 'text-transform', 
													'label'    => 'Text Transform', 
													'selector' => '+ .single-read-more a'
												),
												array(
													'property' => 'text-transform', 
													'label'    => 'Text Transform', 
													'selector' => '+ .single-read-more a'
												),
											),	
										)
									) //End of options
								)
							), //End of styling

							// animate tab
							'animate' => array(
								array(
									'name'    => 'animate',
									'type'    => 'animate'
								)
							), //End of animate						
						
							
			            )// Params
			        )// end shortcode key
			    )// first array
			); // End add map
		}
	}
}



//[moto_blog_posts item_limit="" show_date="" show_comment="" limit_words="" items_on_row=""]
if( !function_exists ('moto_blog_posts_shortcode_func')){
	function moto_blog_posts_shortcode_func( $atts,$content){
		$shortcode = shortcode_atts( array(
			'item_limit'  		=> '',
			'limit_content'  	=> '',
			'show_date'  		=> '',
			'show_author'  		=> '',
			'show_comment'  		=> '',
			'show_date_author'  => '',
			'limit_words'  		=> '',
		    'custom_css'        => '',			
			'custom_css_class'  => '',

		), $atts);

		extract($shortcode);

		$blog_posts = new WP_Query(array(
			'post_type' 		=> 'post',
			'posts_per_page' 	=> $item_limit == 0 ? -1 : $item_limit,
		));

		//custom class		
		$wrap_class  = apply_filters( 'kc-el-class', $atts );
		$wrap_class[] = 'home-blog-area';
		if( ! empty( $custom_class ) ) :
			$wrap_class[] = $custom_class;
		endif;
		$extra_class = implode( ' ', $wrap_class );


		ob_start();
			
?>

<div class="row">
  <div class=" home-blog-area  <?php echo esc_attr( $extra_class ); ?> <?php echo esc_attr( $custom_css_class ); ?>">
	  <div class="container">
	  <div class="row ">
	  <div class="blog-theme ">	
		<?php if($blog_posts->have_posts()): while($blog_posts->have_posts()) :$blog_posts->the_post();?>
			<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'moto_home_blog',true);	?>

					<div class="col-md-4">
						<article class="single-blog-post blog-column b-mb-30">
						<?php if(has_post_thumbnail()): ?>
							<div class="single-blog-thumb">						
								<a href="<?php echo esc_url( get_the_permalink() ); ?>"><img src="<?php echo esc_url( $image[0] ); ?>" alt="" /></a>		
							</div>
							<?php endif; ?>
							<div class="single-blog-content">
								<h2 class="single-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<div class="single-blog-meta">
									<?php if( $show_author == 'yes' && ! empty( $show_author ) ) { ?>
										<span><?php esc_html_e('By:','moto');?> <?php the_author_posts_link(); ?></span>
									<?php } ?>
									<?php if( $show_date == 'yes' && ! empty( $show_date ) ) { ?>
										<span><?php the_time(esc_html__('j M, Y','moto')); ?></span>
									<?php } ?>
									<?php if( $show_comment == 'yes' && ! empty( $show_comment ) ) { ?>
										<span><?php comments_popup_link(esc_html__('No Comment','moto'), esc_html__('01 Comment','moto'), esc_html__('% Comments','moto'),' ', esc_html__('Comments Off','moto'));?></span>
									<?php } ?>
								</div>
								<p><?php echo wp_trim_words( get_the_content(), $limit_content,' ' );?></p>
								<div class="single-read-more">
									<a href="<?php the_permalink(); ?>"><?php moto_read_more(); ?> <i class="fa fa-angle-right"></i></a>
								</div>
							</div>
						</article>
					</div>
				
				
				<?php
					endwhile;
					endif;
					wp_reset_query();
				?>
	  </div>
	  </div>
	  </div>
  </div>
</div>

			
<?php
	return ob_get_clean();	
}

	add_shortcode('moto_blog_posts','moto_blog_posts_shortcode_func');
}
	