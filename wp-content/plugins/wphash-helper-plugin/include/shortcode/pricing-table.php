<?php 
/*
 * Moto Pricing Table ShortCode
 * Author: Hastech
 * Author URI: http://hastech.company
 * Version: 1.0.0
 * ======================================================
 * 
/**
 * =======================================================
 *    KC Shortcode Map
 * ======================================================= */
 add_action('init', 'moto_pricing_table_sections'); // Call kc_add_map function ///
if(!function_exists('moto_pricing_table_sections')):
	function moto_pricing_table_sections(){
		if(function_exists('kc_add_map')): // if kingComposer is active
		kc_add_map(
		    array(
		        'moto_pricing'  => array( // <-- shortcode tag name
		            'name'        => esc_html__('Pricing Table', 'moto'),
		            'description' => esc_html__('Description Here', 'moto'),
		            'icon'        => 'fa-header',
		            'category'    => 'Moto',
		            'params'      => array(
		        // .............................................
		        // ..... // Content TAB
		        // .............................................
		         	'General' => array(
						array(
							'type' => 'text',
							'name' => 'pricing_title',
							'label' => esc_html__( 'Title:', 'moto' ),
							'description' => esc_html__( 'Enter Your Pricing Title.', 'moto' ),
						),
						array(
							'type' => 'text',
							'name' => 'pricing_price',
							'label' => esc_html__( 'Price:', 'moto' ),
							'description' => esc_html__( 'Enter Your Pricing Price.', 'moto' ),
						),
						array(
							'type' => 'textarea',
							'name' => 'desc',
							'label' => esc_html__( 'Attributes:', 'moto' ),
							'description' => esc_html__( 'Enter Your Pricing Attributes.', 'moto' ),
						),
						array(
							'type' => 'text',
							'name' => 'pricing_button_text',
							'label' => esc_html__( 'Text Button:', 'moto' ),
							'description' => esc_html__( 'Enter Your Button Text.', 'moto' ),
						),
						array(
							'name'			=> 'pricing_link',
							'label'			=> esc_html__( 'Link Button:', 'moto' ),
							'type'			=> 'link',
							'description'	=> esc_html__( 'Set read more text displayed on the button.', 'moto' ),
						),
		                array(
		                    'name'          => 'custom_css_class',
		                    'label'         => esc_html__('CSS Class','moto'),
		                    'description'   => esc_html__('Custom css class for css customisation','moto'),
		                    'type'          => 'text'
		                )
						
		         	), // content
		        // .............................................
		        // ..... // Styling
		        // .............................................
		                    'styling' => array(
		                    	array(
		                    		'name'   => 'custom_css',
		                    		'type'   => 'css',
		                    		'options' => array(
		                    			array(
		                    				'screens' => 'any,1024,999,767,479',
											// Pricing Price
		                    				'Title'   => array(
		                    					array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Color',  'moto'),
		                    					    'selector' => '+ .price-titel h4' 
		                    					),
												array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Hover Title Color',  'moto'),
		                    					    'selector' => '+ .pricing-single:hover .price-titel h4'
		                    					),
		                    					array(
		                    						'property' => 'font-family', 
		                    						'label'    => esc_html__('Font Family', 'moto'),
		                    						'selector' => '+  .price-titel h4'
		                    					),
		                    					array( 
		                    						'property' => 'font-size', 
		                    						'label'    => esc_html__('Font Size','moto'), 
		                    						'selector' => '+ .price-titel h4' 
		                    					),
		                    					array(
		                    						'property' => 'font-weight', 
		                    						'label'    => esc_html__('Font Weight', 'moto'),
		                    						'selector' => '+  .price-titel h4'
		                    					),
		                    					array(
													'property' => 'text-transform', 
													'label'    => esc_html__('Text Transform', 'moto'), 
													'selector' => '+  .price-titel h4'
												),
												array(
		                    						'property' => 'text-align', 
		                    						'label'    => esc_html__('Text Align', 'moto'), 
		                    						'selector' => '+ .price-titel h4'
		                    					),
												array(
													'property'  => 'padding', 
													'label' 	=> esc_html__('Padding', 'moto'), 
													'selector'  => '+ .price-titel h4'
												),
												array(
													'property'  => 'margin', 
													'label' 	=> esc_html__('Margin', 'moto'), 
													'selector'  => '+ .price-titel h4'
												),
		                    				),
											// Pricing Price
											'Price' => array(
												array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Color',  'moto'),
		                    					    'selector' => '+ .pricing-price > span' 
		                    					),
												array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Hover Price Color',  'moto'),
		                    					    'selector' => '+ .pricing-single:hover .pricing-price > span' 
		                    					),
		                    					array(
		                    						'property' => 'font-family', 
		                    						'label'    => esc_html__('Font Family', 'moto'),
		                    						'selector' => '+  .pricing-price > span'
		                    					),
		                    					array( 
		                    						'property' => 'font-size', 
		                    						'label'    => esc_html__('Font Size','moto'), 
		                    						'selector' => '+ .pricing-price > span' 
		                    					),
		                    					array(
		                    						'property' => 'font-weight', 
		                    						'label'    => esc_html__('Font Weight', 'moto'),
		                    						'selector' => '+  .pricing-price > span'
		                    					),
		                    					array(
													'property' => 'text-transform', 
													'label'    => esc_html__('Text Transform', 'moto'), 
													'selector' => '+  .pricing-price > span'
												),
												array(
		                    						'property' => 'text-align', 
		                    						'label'    => esc_html__('Text Align', 'moto'), 
		                    						'selector' => '+ .pricing-price > span'
		                    					),
												array(
		                    						'property' => 'display', 
		                    						'label'    => esc_html__('Text Align', 'moto'), 
		                    						'selector' => '+ .pricing-price > span'
		                    					),
												array(
													'property'  => 'padding', 
													'label' 	=> esc_html__('Padding', 'moto'), 
													'selector'  => '+ .pricing-price > span'
												),
												array(
													'property'  => 'margin', 
													'label' 	=> esc_html__('Margin', 'moto'), 
													'selector'  => '+ .pricing-price > span'
												),
											),
											// Pricing Attributes
											'Attributes' => array(
												array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Color',  'moto'),
		                    					    'selector' => '+ .price-decs ul li' 
		                    					),
												array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Text Hover Color',  'moto'),
		                    					    'selector' => '+ .pricing-single:hover .price-decs ul li' 
		                    					),
		                    					array(
		                    						'property' => 'font-family', 
		                    						'label'    => esc_html__('Font Family', 'moto'),
		                    						'selector' => '+  .price-decs ul li'
		                    					),
		                    					array( 
		                    						'property' => 'font-size', 
		                    						'label'    => esc_html__('Font Size','moto'), 
		                    						'selector' => '+ .price-decs ul li' 
		                    					),
		                    					array(
		                    						'property' => 'font-weight', 
		                    						'label'    => esc_html__('Font Weight', 'moto'),
		                    						'selector' => '+  .price-decs ul li'
		                    					),
		                    					array(
													'property' => 'text-transform', 
													'label'    => esc_html__('Text Transform', 'moto'), 
													'selector' => '+  .price-decs ul li'
												),
												array(
		                    						'property' => 'text-align', 
		                    						'label'    => esc_html__('Text Align', 'moto'), 
		                    						'selector' => '+ .price-decs ul li'
		                    					),
												array(
		                    						'property' => 'line-height', 
		                    						'label'    => esc_html__('Text Align', 'moto'), 
		                    						'selector' => '+ .price-decs ul li'
		                    					),
												array(
													'property'  => 'padding', 
													'label' 	=> esc_html__('Padding', 'moto'), 
													'selector'  => '+ .price-decs ul li'
												),
												array(
													'property'  => 'margin', 
													'label' 	=> esc_html__('Margin', 'moto'), 
													'selector'  => '+ .price-decs ul li'
												),
											),
											// Pricing Button
											'Button' => array(
												array( 
													'property' => 'color', 
													'label'    => esc_html__( 'Color', 'moto' ),
													'selector' => '+ .ordr-btn > a' 
												),
												array( 
													'property' => 'color', 
													'label'    => esc_html__( 'Text hover color', 'moto' ),
													'selector' => '+ .pricing-single:hover .ordr-btn > a' 
												),
												array( 
													'property' => 'background', 
													'label'    => esc_html__( 'Background', 'moto' ),
													'selector' => '+ .ordr-btn > a' 
												),
												array( 
													'property' => 'background', 
													'label'    => esc_html__( 'Background Hover', 'moto' ),
													'selector' => '+ .pricing-single:hover .ordr-btn > a' 
												),
												array(
													'property' => 'font-family', 
													'label'    => esc_html__( 'Font Family', 'moto' ),
													'selector' => '+ .ordr-btn > a'
												),
												array( 
													'property' => 'font-size', 
													'label'    => esc_html__( 'Font Size', 'moto' ),
													'selector' => '+ .ordr-btn > a' 
												),
												array( 
													'property' => 'box-shadow', 
													'label'    => esc_html__( 'Box Shadow', 'moto' ),
													'selector' => '+ .ordr-btn > a' 
												),
												array( 
													'property' => 'line-height', 
													'label'    => esc_html__( 'Line Height', 'moto' ),
													'selector' => '+ .ordr-btn > a' 
												),
												array( 
													'property' => 'display',
													'label'    => esc_html__( 'Display', 'moto' ),
													'selector' => '+ .ordr-btn > a' 
												),
												array(
													'property' => 'font-weight', 
													'label'    => esc_html__( 'Font Weight','moto' ), 
													'selector' => '+ .ordr-btn > a'
												),
												array(
													'property' => 'text-transform', 
													'label'    => esc_html__( 'Text Transform', 'moto' ),
													'selector' => '+ .ordr-btn > a'
												),
												array(
													'property'   => 'padding',
													'label'      => esc_html__('padding','moto'),
													'selector'   => '+ .ordr-btn > a',
												),
												array(
													'property'   => 'margin',
													'label'      => esc_html__('margin','moto'),
													'selector'   => '+ .ordr-btn > a',
												),
											),
											/// Box Css
											'Box' =>array(
		                    					array(
		                    						'property' => 'background', 
		                    						'label'    => esc_html__('Background', 'moto'),
		                    						'selector' => '+  .pricing-single'
		                    					),
		                    					array(
		                    						'property' => 'background', 
		                    						'label'    => esc_html__('Background Hover Color', 'moto'),
		                    						'selector' => '+  .pricing-single:hover'
		                    					),
												array(
		                    						'property' => 'box-shadow', 
		                    						'label'    => esc_html__('Shadow', 'moto'),
		                    						'selector' => '+  .pricing-single'
		                    					),
												array(
		                    						'property' => 'border-radius', 
		                    						'label'    => esc_html__('Radius', 'moto'),
		                    						'selector' => '+  .pricing-single'
		                    					),
												array(
		                    						'property' => 'border', 
		                    						'label'    => esc_html__('Border', 'moto'),
		                    						'selector' => '+  .pricing-single'
		                    					),
												array(
													'property'  => 'padding', 
													'label' 	=> esc_html__('Padding', 'moto'), 
													'selector'  => '+ .pricing-single'
												),
												array(
													'property'  => 'margin', 
													'label' 	=> esc_html__('Margin', 'moto'), 
													'selector'  => '+ .pricing-single'
												),
											),
		                    			)
		                    		) //End of options
		                    	)
		                    ), //End of styling
                            'animate' => array(
								array(
									'name'    => 'animate',
									'type'    => 'animate'
								)
							), //End of animate
		        // .............................................
		        // .............................................
		        // .............................................
		        /////////////////////////////////////////////////////////
		            )// Params
		        )// end shortcode key
		    )// first array
		); // End add map
		endif;
	}
endif;

 
 
 /*
 * =======================================================
 *    Register Pricing Shortcode   
 * =======================================================
 */

 if(!function_exists('moto_pricing_table_shortcode')){
	function moto_pricing_table_shortcode( $atts, $content){
	ob_start();
			$pricing = shortcode_atts(array(
		   'pricing_title'       	  =>'',
		   'pricing_price'      =>'',
		   'desc'      =>'',
		   'pricing_link'      =>'',
		   'pricing_button_text'      =>'',
		   'custom_css_class' =>'',
			),$atts); 
			extract( $pricing );
		//custom class		
		$wrap_class  = apply_filters( 'kc-el-class', $atts );
		if( !empty( $custom_class ) ):
			$wrap_class[] = $custom_class;
		endif;
		$extra_class =  implode( ' ', $wrap_class );
		
		$data_desc='';
		
	?>
	<div class="<?php echo esc_attr( $extra_class ); ?> <?php echo esc_attr( $custom_css_class ); ?>">
	
		<div class="pricing-single white-bg text-center mb-30">
			<div class="price-titel uppercase">
				<h4><?php echo $pricing_title; ?></h4>
			</div>
			<div class="pricing-price">
				<span><?php echo $pricing_price; ?></span>
			</div>
			<div class="price-decs">
					<?php 
					if ( !empty( $desc ) ) {

						 $pros = explode( "\n", $desc );
						 if( count( $pros ) ) {

						  $data_desc .= '<ul class="content-desc">';

						  foreach( $pros as $pro ) {							
							$data_desc .= '<li>'. $pro .' </li>';
						 
						  }

						  $data_desc .= '</ul>';

						 }

						}
						echo $data_desc;
					?>
			</div>
			<div class="ordr-btn uppercase">
				<a href="<?php echo $pricing_link; ?>"><?php echo $pricing_button_text; ?></a>
			</div>
		</div>
		
	</div>
	<?php
	
		return ob_get_clean();
	}
	add_shortcode('moto_pricing' ,'moto_pricing_table_shortcode');
}
 