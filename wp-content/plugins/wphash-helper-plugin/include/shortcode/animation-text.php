<?php
/*
 * Moto Animation Text ShortCode
 * Author: Hastech
 * Author URI: http://hastech.company
 * Version: 1.0.0
 * ======================================================
 */

/**
 * =======================================================
 *    KC Shortcode Map
 * =======================================================
 */

add_action('init', 'moto_animation_slider'); // Call kc_add_map function ///
if(!function_exists('moto_animation_slider')):
	function moto_animation_slider(){
		if(function_exists('kc_add_map')): // if kingComposer is active
		kc_add_map(
		    array(
		        'moto_animation' => array( // <-- shortcode tag name
		            'name' => esc_html__('Animation Slider', 'moto'),
		            'description' => esc_html__('Description Here', 'moto'),
		            'icon' => 'fa-header',
		            'category' => 'Moto',
					'is_container' => true,
		            'params' => array(
		        // .............................................
		        // ..... // Content TAB
		        // .............................................
		         	'General' => array(
		                array(
		                    'name' => 'title',
		                    'label' => esc_html__('Title','moto'),
		                    'type' => 'text',
		                    'value' => 'builder'
		                ),
		                array(
		                    'name' => 'visible_one',
		                    'label' => esc_html__('Visible First Word','moto'),
		                    'type' => 'text',
		                    'value' => 'Beautiful'
		                ),
		                array(
		                    'name' => 'visible',
		                    'label' => esc_html__('Visible Fixed Word','moto'),
		                    'type' => 'group',
		                    'value'     => base64_encode( json_encode(array( ) ) 
							),

						'params' => array(
							array(
								'type'       => 'text',
								'label'      => esc_html__( 'Add New Text', 'moto' ),
								'name'        => 'visible_two',
								'description' => esc_html__( 'Enter text used as visible text', 'moto' ),
								'admin_label' => true,
							)

						)
		                ),
		                 array(
		                    'name' => 'button_text',
		                    'label' => esc_html__('Button Text','moto'),
		                    'type' => 'text',
		                    'value' => 'view portfolio'
		                ),
		                array(
		                    'name' => 'button_link',
		                    'label' => esc_html__('Button Link','moto'),
		                    'type' => 'link',

		                ),
		                array(
		                    'name'    => 'type_class',
		                    'label'   => esc_html__('Type Of Slider','moto'),
		                    'type'    => 'select',
		                    'description' => esc_html__('Select text type','moto'),
		                    'options' => array(
		                    	'loading-bar' => 'Word Under Border',
		                    	'clip'        => 'Word Removing'
		                    )
		                ),
		                array(
		                    'name' => 'custom_css_class',
		                    'label' => esc_html__('CSS Class','moto'),
		                    'description' => esc_html__('Custom css class for css customisation','moto'),
		                    'type' => 'text'
		                ),
		         	), // content
		        // .............................................
		        // ..... // Styling
		        // .............................................
		                    'styling' => array(
		                    	array(
		                    		'name' => 'custom_css',
		                    		'type' => 'css',
		                    		'options' => array(
		                    			array(
		                    				'screens' => 'any,1024,999,767,479',
		                    				'Title'   => array(
		                    					array(
		                    						'property' => 'font-family', 
		                    						'label'    => esc_html__('Title Font Family','moto'), 
		                    						'selector' => '+ .slider-text h1'
		                    					),
		                    					array( 
		                    						'property' => 'font-size', 
		                    						'label'    => esc_html__('Title Font Size','moto'), 
		                    						'selector' => '+ .slider-text h1' 
		                    					),
												array( 
		                    					    'property' => 'letter-spacing', 
		                    					    'label'    => esc_html__('Letter Spacing','moto'),
		                    					    'selector' => '+ .slider-text h1' 
		                    					),
		                    					array(
		                    						'property' => 'font-weight', 
		                    						'label'    => esc_html__('Title Font Weight','moto'), 
		                    						'selector' => '+ .slider-text h1'
		                    					),
		                    					array(
													'property' => 'text-transform', 
													'label'    => esc_html__('Title Text Transform','moto'), 
													'selector' => '+ .slider-text h1'
												),
		                    					array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Title Color','moto'),
		                    					    'selector' => '+ .slider-text h1' 
		                    					),
		                    				),
		                    				///
		                    				'button'      => array(
		                    					array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__( 'Button Text', 'moto'),
		                    					    'selector' => '+ .slider-text a' 
		                    					),
		                    					array( 
		                    					    'property' => 'color', 
		                    					    'label'    => esc_html__('Button Text Hover','moto'), 
		                    					    'selector' => '+ .slider-text a:hover ' 
		                    					),
		                    					array( 
		                    					    'property' => 'background-color', 
		                    					    'label'    => esc_html__('Button Color','moto'), 
		                    					    'selector' => '+ .slider-text a' 
		                    					),
		                    					array( 
		                    					    'property' => 'background-color', 
		                    					    'label'    =>esc_html__('Button Hover Color','moto'), 
		                    					    'selector' => '+ .slider-text a:hover' 
		                    					),
                                                array( 
		                    					    'property' => 'padding', 
		                    					    'label'    => esc_html__('Button Padding','moto'), 
		                    					    'selector' => '+ .slider-text a'
		                    					),
                                                array( 
		                    					    'property' => 'margin', 
		                    					    'label'    => esc_html__('Button Margin','moto'), 
		                    					    'selector' => '+ .slider-text a'
		                    					),
                                                array( 
		                    					    'property' => 'font-size', 
		                    					    'label'    => esc_html__('Font Size','moto'), 
		                    					    'selector' => '+ .slider-text a'
		                    					),
                                                array( 
		                    					    'property' => 'font-weight', 
		                    					    'label'    => esc_html__('Font Weight','moto'), 
		                    					    'selector' => '+ .slider-text a'
		                    					),
                                                array( 
		                    					    'property' => 'font-style', 
		                    					    'label'    => esc_html__('Font Style','moto'), 
		                    					    'selector' => '+ .slider-text a'
		                    					),
                                                array( 
		                    					    'property' => 'font-family', 
		                    					    'label'    => esc_html__('Font Family','moto'), 
		                    					    'selector' => '+ .slider-text a'
		                    					),
                                                array( 
		                    					    'property' => 'text-transform', 
		                    					    'label'    => esc_html__('Text Transform','moto'), 
		                    					    'selector' => '+ .slider-text a'
		                    					),
		                    				),
		                    				///
											'box' => array(
												array(
													'property'   => 'margin',
													'label'      => esc_html__('Section margin','moto'),
													'selector'   => '+ .slider-text',
												)
											),
		                    				///
		                    				'Text align' => array(
		                    					array(
		                    						'property' => 'text-align', 
		                    						'label'    => esc_html__('Text Align','moto'), 
		                    						'selector' => '+ .slider-text'
		                    					),
		                    				),
		                    				
		                    			)
		                    		) //End of options
		                    	)
		                    ), //End of styling
                            'animate' => array(
								array(
									'name'    => 'animate',
									'type'    => 'animate'
								)
							), //End of animate
		        // .............................................
		        // .............................................
		        // .............................................
		        /////////////////////////////////////////////////////////
		            )// Params
		        )// end shortcode key
		    )// first array
		); // End add map
		endif;
	}
endif;



 /**
 * =======================================================
 *    Register Shortcode section title
 * =======================================================
 */
 // [moto_animation title="" description="" custom_css_class=""]
if(!function_exists('moto_animation_shortcode')){
	function moto_animation_shortcode($atts,$content){
	ob_start();
			$moto_animations = shortcode_atts(array(
					'title' 		   => '',
					'visible' 		   => '',
					'visible_one' 	   => '',
					'button_link'      => '',
					'button_text'      => '',
					'type_class'       => '',
					'custom_css' 	   => '',
					'custom_css_class' => '',
			),$atts); 
			extract( $moto_animations );
		//custom class		
		$wrap_class  = apply_filters( 'kc-el-class', $atts );
		if( !empty( $custom_class ) ):
			$wrap_class[] = $custom_class;
		endif;
		$extra_class =  implode( ' ', $wrap_class );

		if ( !empty( $button_link ) ) {
			$link_arr = explode( '|', $button_link );
			if ( !empty( $link_arr[0] ) ) {
				$link_url = $link_arr[0];
			} else {
				$link_url = '#';
			}
		} else {
			$link_url = '#';
		}
	?>
	<div class="<?php echo esc_attr( $extra_class ); ?> <?php echo esc_attr($custom_css_class); ?>">
		<div class="slider-text text-left">
			<h1 class="cd-headline <?php echo esc_attr( $type_class ); ?>">
					<?php if( $title ): ?>
					<span><?php echo esc_html( $title ); ?></span><br>
				    <?php endif; ?>
					<span class="cd-words-wrapper">
						<?php if( $visible_one ): ?>
						<b class="is-visible"><?php echo esc_html( $visible_one ); ?></b>
					   <?php endif; ?>
					   <?php foreach( $visible as $visible_word ): ?>
						
						<b><?php echo $visible_word->visible_two; ?></b>
						<?php endforeach; ?>
				
					</span>
			</h1>
			
		
			<a href="<?php echo esc_url( $link_url ); ?>"><?php echo esc_html( $button_text ); ?></a>
		</div>	
	</div>
	<?php
		return ob_get_clean();
	}
	add_shortcode('moto_animation' ,'moto_animation_shortcode');
}
