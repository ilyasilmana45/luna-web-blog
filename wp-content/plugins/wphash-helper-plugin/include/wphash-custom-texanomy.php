<?php

/*=======================================================
*    Register Custom Taxonomy
* =======================================================*/
if ( ! function_exists('moto_custom_taxonomy') ) {
	function moto_custom_taxonomy() {
		//taxonomies Category for slider
		$labels = array(
			'name'                       => _x( 'Categories', 'Taxonomy General Name', 'moto' ),
			'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'moto' ),
			'menu_name'                  => __( 'Category', 'moto' ),
			'all_items'                  => __( 'All Category', 'moto' ),
			'parent_item'                => __( 'Parent Item', 'moto' ),
			'parent_item_colon'          => __( 'Parent Item:', 'moto' ),
			'new_item_name'              => __( 'New Category Name', 'moto' ),
			'add_new_item'               => __( 'Add New Category', 'moto' ),
			'edit_item'                  => __( 'Edit Category', 'moto' ),
			'update_item'                => __( 'Update Category', 'moto' ),
			'view_item'                  => __( 'View Category', 'moto' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'moto' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'moto' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'moto' ),
			'popular_items'              => __( 'Popular Category', 'moto' ),
			'search_items'               => __( 'Search Category', 'moto' ),
			'not_found'                  => __( 'Not Found', 'moto' ),
			'no_terms'                   => __( 'No Category', 'moto' ),
			'items_list'                 => __( 'Category list', 'moto' ),
			'items_list_navigation'      => __( 'Category list navigation', 'moto' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		);
		register_taxonomy( 'slider_category', array( 'moto_slider' ), $args );
	}
	add_action( 'init', 'moto_custom_taxonomy', 0 );
}